<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.manage.bean.PageModle"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="com.manage.bean.OprInfo,com.trade.bean.*,com.gy.util.*"%>
<%@ include file="include.jsp" %>

<html lang="en"><head>
    <title>线上交易管理平台</title>
	<script type="text/javascript" src="service/channel_js/channel_verify_head.js"></script>
	<script type="text/javascript" src="service/manage_js/account_income_detail.js"></script>

</head>
<body  onload="IFrameResize()" class=" theme-blue" >

<div>
			<div class="header">
				<h1 class="page-title">
					进账明细查询
				</h1>
			</div>
			
			<%--<iframe width="100%" style="border: 0" height="900px" scrolling="no" src="<%= request.getContextPath()%>/manage/tradeInfo.jsp"></iframe>
			
		--%>
		</div>
		<div class="search-well">
                <form class="form-inline" id="account_form" name="account_form" style="margin-top:0px;margin-left: 10px" action="manage/accountQuery" target="_self">
                	<table >
                		<tr>
                		<td><label>入账时间：</label></td>
                			<td><input id="time_start_begin" name="time_start_begin" size="16" type="text" value="" readonly class="form_datetime">
                			- <input id="time_start_end" name="time_start_end" size="16" type="text" value="" readonly class="form_datetime">
                			</td>
                			
                		<td><span><label class="control-label span_auto_width" for="lunch">商户号：</label></span></td><td>
					        <select id="mchtNo" name="mchtNo" class="selectpicker" data-live-search="true" title="全部">
						        
						     </select>
                    	</td>
                    	<td>
            				<label>平台代付单号：</label></td><td>
                    		<input name="dfTransactionId" id="dfTransactionId" class="input-xlarge form-control" placeholder="平台订单号"  type="text">	
          				</td>
                    	</tr>
                    	<tr>
                    	
                		<td><input type="hidden" id="pageNum" name="pageNum" value="1"></td><td colspan="6" align="right">
                    		<button class="btn btn-default" type="button" id="trade_query_btn" name="trade_query_btn"><i class="fa fa-search"></i> 查询</button>
                    	</td>
                    	</tr>
                	</table>
                	
                </form>
            </div>
            
			<div class="main-content">
				<div class="btn-toolbar list-toolbar">
					<button class="btn btn-primary" id="exportBtn">
						<i class="fa fa-plus"></i>导出
					</button>
					<button class="btn btn-default">
						export
					</button>
					<div class="fa">
						&nbsp;&nbsp;&nbsp;<span id="tradeTotalNum">总笔数:0条</span>
						&nbsp;&nbsp;&nbsp;<span id="totalAmount">代付总金额:0元</span>
						&nbsp;&nbsp;&nbsp;<span id="totalFee">手续费总金额:0元</span>
						&nbsp;&nbsp;&nbsp;<span id="accountNum">账户扣除总金额:0元</span>
					</div>
				</div>
				
			</div>
				<div style="width: 99%; overflow-x: scroll;">
					<table class="table" width="1500px" height="auto">
						<thead style="height: 20px">
							<tr style="height: 20px">
								<th style="width: 100px">
									#
								</th>
								<th>
									<span style="width: 120px; display: block;">商户编号</span>
								</th>
								<th>
									<span style="width: 90px; display: block;">订单时间</span>
								</th>
								<th>
									<span style="width: 100px; display: block;">平台订单号</span>
								</th>
								<th>
									<span style="width: 90px; display: block;">交易金额</span>
								</th>
								<th>
									<span style="width: 90px; display: block;">手续费</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">商户进账金额</span>
								</th>
								</tr>
						</thead>

						<tbody id="mcht_tbody">
							
						</tbody>
					</table>
				</div>
				
				<ul class="pager">  
          
        <li id="totalNum">总记录条</li>  
        <li>  </li>  
            <li id="firstPage">  
                <a href="javascript:void(0);" onclick="accountDetailQuery('1')">首页</a>  
            </li>  
            <li >  
                <a id="prePage" href="javascript:void(0);" >上一页</a>  
            </li>  
            <li >  
                <a id="nextPage" href="javascript:void(0);" >下一页</a>  
            </li>  
            <li >  
                <a id="lastPage" href="javascript:void(0);" >尾页</a>  
            </li>  
              
            <li id="currentAndTotal">当前页/</li>  
              
            <li></li>  
            <li><select id="xzPage" name="xzPage" >  
             
            </select></li>  
              
            <li><a href="javascript:void(0);" onclick="accountDetailQuery(document.getElementById('xzPage').value);">go</a></li>  
        </ul>

</body></html>