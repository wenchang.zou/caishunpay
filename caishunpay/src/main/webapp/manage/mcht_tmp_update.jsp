<%@ page language="java" import="java.util.*,com.trade.bean.own.*,com.gy.util.CommonFunction" pageEncoding="UTF-8"%>
<%@page import="com.manage.bean.PageModle"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="com.manage.bean.OprInfo,com.trade.bean.*,com.gy.util.*"%>
<%@ include file="include.jsp"%>
<%
QrcodeMchtInfoTmp mchtInfo = (QrcodeMchtInfoTmp)request.getAttribute("mchtInfo");
%>
<html lang="en">
	<head>
		<title>彩顺网络科技平台</title>
		<script type="text/javascript">
		//初始化toastr.js
		toastr.options = {positionClass:'toast-bottom-center', timeOut: "2000"};
		
    function IFrameResize(){ 
    	//alert(this.document.body.scrollHeight); //弹出当前页面的高度 
    	var obj = parent.document.getElementById("main_iframe"); //取得父页面IFrame对象 
    	//alert(obj.height); //弹出父页面中IFrame中设置的高度 
    	obj.height = this.document.body.scrollHeight; //调整父页面中IFrame的高度为此页面的高度 
    	} 

	<%
	if(mchtInfo == null) {
	    mchtInfo = new QrcodeMchtInfoTmp();
	%>
		alert("加载商户信息异常");
	<%
	}
	%>
    </script>
	</head>
	<body onload="IFrameResize()" class=" theme-blue">

		<div >
			<div class="header">
				<h1 class="page-title">
					商户修改
				</h1>
			</div>

			<div class="row">
				<div class="col-md-8">
					<div id="myTabContent" class="tab-content">
						<div class="tab-pane active in" id="home">

							<form id="mchtForm" name="mchtForm" class="form-horizontal">
							
								<div class="form-group">
									<label for="mchtName" class="col-sm-2 control-label">
										商户编号
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="mchtNo"
											name="mchtNo" placeholder="请输入商户名称"  readonly value="<%=StringUtil.trans2Str(mchtInfo.getMchtNo()) %>">
									</div>
								</div>
								<div class="form-group">
									<label for="mchtName" class="col-sm-2 control-label">
										商户名称
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="mchtName"
											name="mchtName" placeholder="请输入商户名称" value="<%=StringUtil.trans2Str(mchtInfo.getMchtName()) %>">
									</div>
								</div>
								
								<div class="form-group">
									<label for="phone" class="col-sm-2 control-label">
										邮箱
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="email"
											name="email" placeholder="请输入邮箱" value="<%=StringUtil.trans2Str(mchtInfo.getEmail()) %>">
									</div>
								</div>
								<div class="form-group">
									<label for="phone" class="col-sm-2 control-label">
										手机号码
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="phone"
											name="phone" placeholder="请输入手机号码" readonly value="<%=StringUtil.trans2Str(mchtInfo.getPhone()) %>">
									</div>
								</div>
								
								<div class="form-group">
									<label for="identity_no" class="col-sm-2 control-label">
										身份证号
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="identity_no"
											name="identity_no" placeholder="请输入身份证号"  value="<%=StringUtil.trans2Str(mchtInfo.getIdentity_no())%>">
									</div>
								</div>
								
								<div class="form-group">
									<label for="bank_card_no" class="col-sm-2 control-label">
										结算卡号
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="bank_card_no"
											name="bank_card_no" placeholder="请输入结算卡号" value="<%=StringUtil.trans2Str(mchtInfo.getBank_card_no()) %>">
									</div>
								</div>
								
								<div class="form-group">
									<label for="bank_name" class="col-sm-2 control-label">
										结算银行
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="bank_name"
											name="bank_name" placeholder="请输入结算银行" value="<%=StringUtil.trans2Str(mchtInfo.getBank_name())%>">
									</div>
								</div>
								<div class="form-group">
									<label for="card_name" class="col-sm-2 control-label">
										结算人
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="card_name"
											name="card_name" placeholder="请输入结算人"  value="<%=StringUtil.trans2Str(mchtInfo.getCard_name())%>">
									</div>
								</div>
								<div class="form-group">
									<label for="bank_no" class="col-sm-2 control-label">
										联行号
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="bank_no"
											name="bank_no" placeholder="请输入联行号"  value="<%=StringUtil.trans2Str(mchtInfo.getBank_no()) %>">
									</div>
								</div>
								<div class="form-group">
									<label for="lisence_addr" class="col-sm-2 control-label">
										商户地址
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="lisence_addr"
											name="lisence_addr" placeholder="请输入商户地址"  value="<%=StringUtil.trans2Str(mchtInfo.getLisence_addr()) %>">
									</div>
								</div>
								
								<div class="form-group">
									<label for="lisence_addr" class="col-sm-2 control-label">
										微信费率
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="wechat_fee_value"
											name="wechat_fee_value" placeholder="请输入微信费率(%)" value="<%=StringUtil.trans2Str(mchtInfo.getWechat_fee_value()) %>">
									</div>
								</div>
								<div class="form-group">
									<label for="lisence_addr" class="col-sm-2 control-label">
										微信wap费率
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="wechatwap_fee_value" onkeyup="value=value.replace(/[^\d.]/g,'')"
											name="wechatwap_fee_value" placeholder="请输入微信wap费率(%)" value="<%=StringUtil.trans2Str(mchtInfo.getWechatwap_fee_value()) %>">
									</div>
								</div>
								<div class="form-group">
									<label for="lisence_addr" class="col-sm-2 control-label">
										支付宝费率
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="alipay_fee_value"
											name="alipay_fee_value" placeholder="请输入支付宝费率(%)" value="<%=StringUtil.trans2Str(mchtInfo.getAlipay_fee_value()) %>">
									</div>
								</div>
								<div class="form-group">
									<label for="aliwap_fee_value" class="col-sm-2 control-label">
										支付宝wap费率
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="aliwap_fee_value" onkeyup="value=value.replace(/[^\d.]/g,'')"
											name="aliwap_fee_value" placeholder="请输入支付宝wap费率(%)" value="<%=StringUtil.trans2Str(mchtInfo.getAliwap_fee_value()) %>">
									</div>
								</div>
								<div class="form-group">
									<label for="lisence_addr" class="col-sm-2 control-label">
										QQ钱包费率
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="qq_fee_value"
											name="qq_fee_value" placeholder="请输入QQ钱包费率(%)" value="<%=StringUtil.trans2Str(mchtInfo.getQq_fee_value()) %>">
									</div>
								</div>
								<div class="form-group">
									<label for="lisence_addr" class="col-sm-2 control-label">
										快捷支付费率
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="quickpay_fee_value"
											name="quickpay_fee_value" placeholder="请输入快捷支付费率(%)" value="<%=StringUtil.trans2Str(mchtInfo.getQuickpay_fee_value()) %>">
									</div>
								</div>
								<div class="form-group">
									<label for="lisence_addr" class="col-sm-2 control-label">
										网银支付费率
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="netpay_fee_value"
											name="netpay_fee_value" placeholder="请输入网银支付费率(%)"  value="<%=StringUtil.trans2Str(mchtInfo.getNetpay_fee_value()) %>">
									</div>
								</div>
								
								<div class="form-group">
									<label for="single_extra_fee" class="col-sm-2 control-label">
										单笔代付费/分
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="single_extra_fee" 
											name="single_extra_fee" placeholder="请输入单笔代付手续费(分)" value="<%=StringUtil.trans2Str(mchtInfo.getSingle_extra_fee()) %>">
									</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-sm-2" for="lunch">
										公司编号
									</label>
									<div class="col-sm-6">
										<select id="company_id" name="company_id"
										class="selectpicker" data-live-search="true" title="请选公司编号" value="<%=StringUtil.trans2Str(mchtInfo.getCompany_id()) %>">

										</select>
									</div>
									
								</div>
								
								<div class="form-group" >
									<label class="col-sm-2 control-label">图片</label>
									
									<div class="col-sm-10" id="mchtFileDiv" >
										
										<div id="myCarousel" class="carousel slide">
											
										</div> 
									</div>
									<c:forEach items="${requestScope.mchtFiles}" var="item">
											<input name="mchtFilePath" type="hidden" value="${item.mchtFilePath}">
										</c:forEach>
								</div>
								
								<input type="hidden" value="" id="refuse_reason" name="refuse_reason">
							</form>
						</div>

					</div>
				</div>
			</div>

	<div class="btn-toolbar list-toolbar">
      <button class="btn btn-primary" id="saveBtn"><i class="fa fa-save"></i> 保存</button>
      <a  data-toggle="modal" class="btn btn-default" id="backBtn">返回</a>
      <!-- href="#myModal" -->
    </div>
		</div>
		
			<footer>
                <hr>
                <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                <p>© 2017 ykbpay</p>
                <br>
            </footer>
            
<div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Delete Confirmation</h3>
      </div>
      <div class="modal-body">
        
        <p class="error-text"><i class="fa fa-warning modal-icon"></i>确认保存?</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">取消</button>
        <button class="btn btn-danger" data-dismiss="modal">确定</button>
      </div>
    </div>
  </div>
</div>
		<script type="text/javascript" src="service/manage_js/mcht_tmp_update.js"></script>

	</body>
</html>