<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.manage.bean.PageModle"%>
<%@page import="com.gy.system.SysParamUtil"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="com.manage.bean.OprInfo,com.trade.bean.*,com.gy.util.*"%>
<%@ include file="include.jsp" %>

<html lang="en"><head>
    <title>线上交易管理平台</title>
	<script type="text/javascript" src="service/manage_js/account_head.js"></script>
    <script type="text/javascript" src="service/manage_js/account_check.js"></script>
</head>
<body  onload="IFrameResize()" class=" theme-blue" >
	<div>
			<div class="header">
				<h1 class="page-title">
					账户额度审核
				</h1>
			</div>
		</div>
		<div class="search-well">
                <form class="form-inline" id="account_query_form" name="account_query_form" style="margin-top:0px;margin-left: 10px" action="manage/accountFileQuery" target="_self">
                	<table >
                		<tr>
                		<td><label>上传时间：</label></td>
                			<td><input id="time_start_begin" name="time_start_begin" size="10" type="text" value="" readonly class="form_datetime">
                			- <input id="time_start_end" name="time_start_end" size="10" type="text" value="" readonly class="form_datetime">
                			</td>
                		<td>
            				<label>批次号：</label></td><td>
                    		<input name="offerSeq" id="offerSeq" class="input-xlarge form-control" placeholder="批次号"  type="text">
          				</td>
          				
                		<td><input type="hidden" id="pageNum" name="pageNum" value="1"></td><td colspan="3" align="right">
                    		<button class="btn btn-default" type="button" id="trade_query_btn" name="trade_query_btn"><i class="fa fa-search"></i> 查询</button>
                    	</td>
                    	</tr>
                	</table>
                	
                </form>
            </div>
            
			<div class="main-content">
				<div class="btn-toolbar list-toolbar">
					<button class="btn btn-primary" id="exportBtn">
						<i class="fa fa-plus"></i>导出
					</button>
					<div class="btn-group">
					</div>
				</div>
				
			</div>
			
				<div style="width: 99%; overflow-x: scroll;">
					<table class="table" width="1500px" height="auto">
						<thead style="height: 20px">
							<tr style="height: 20px">
								<th style="width: 40px">
									#
								</th>
								<th>
									<span style="width: 90px; display: block;">批次号</span>
								</th>
								<th>
									<span style="width: 160px; display: inline-block; overflow: hidden">账目文件名</span>
								</th>
								<th>
									<span style="width: 100px; display: block;">操作时间</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">操作员</span>
								</th>
								<th>
									<span style="width: 90px; display: block;">状态</span>
								</th>
								<th>
									<span style="width: 90px; display: block;">操作</span>
								</th>
								</tr>
						</thead>

						<tbody id="mcht_tbody">
							
						</tbody>
					</table>
				</div>
				
				<ul class="pager">  
          
        <li id="totalNum">总记录条</li>  
        <li>  </li>  
            <li id="firstPage">  
                <a href="javascript:void(0);" onclick="accountFileQuery('1')">首页</a>  
            </li>  
            <li >  
                <a id="prePage" href="javascript:void(0);" >上一页</a>  
            </li>  
            <li >  
                <a id="nextPage" href="javascript:void(0);" >下一页</a>  
            </li>  
            <li >  
                <a id="lastPage" href="javascript:void(0);" >尾页</a>  
            </li>  
              
            <li id="currentAndTotal">当前页/</li>  
              
            <li></li>  
            <li><select id="xzPage" name="xzPage" >  
             
            </select></li>  
              
            <li><a href="javascript:void(0);" onclick="accountFileQuery(document.getElementById('xzPage').value);">go</a></li>  
        </ul>



</body></html>