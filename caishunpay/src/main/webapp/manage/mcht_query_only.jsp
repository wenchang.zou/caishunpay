<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.manage.bean.PageModle"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="com.manage.bean.OprInfo,com.trade.bean.*,com.gy.util.*"%>
<%@ include file="include.jsp" %>

<html lang="en"><head>
    <title>彩顺网络科技平台</title>
	<script type="text/javascript" src="service/channel_js/channel_verify_head.js"></script>
	<script type="text/javascript" src="service/manage_js/mcht_query_only.js"></script>
</head>
<body  onload="IFrameResize()" class=" theme-blue" >

<div>
			<div class="header">
				<h1 class="page-title">
					商户查询
				</h1>
			</div>
			
			<%--<iframe width="100%" style="border: 0" height="900px" scrolling="no" src="<%= request.getContextPath()%>/manage/tradeInfo.jsp"></iframe>
			
		--%>
		</div>
		<div class="search-well">
                <form class="form-inline" id="mcht_form" name="mcht_form" style="margin-top:0px;margin-left: 10px" action="manage/mchtQuery" target="_self">
                	<table >
                		<tr>
                		<td><span><label class="control-label span_auto_width" for="lunch">公司编号：</label></span></td><td>
					        
						     <select id="company_id" name="company_id"
										class="selectpicker" data-live-search="true" title="请选公司编号">
							</select>
                    	</td>
                		<td><span><label class="control-label span_auto_width" for="lunch">商户号：</label></span></td><td>
					        <select id="mchtNo" name="mchtNo" class="selectpicker" data-live-search="true" title="全部">
						        
						     </select>
                    	</td>
                    	<td>
            				<label>身份证号：</label></td><td>
                    		<input name="identity_no" id="identity_no" class="input-xlarge form-control" placeholder="身份证号"  type="text">	
          				</td>
                		<td><input type="hidden" id="pageNum" name="pageNum" value="1"></td><td align="right">
                    		<button class="btn btn-default" type="button" id="trade_query_btn" name="trade_query_btn"><i class="fa fa-search"></i> 查询</button>
                    	</td>
                    	</tr>
                	</table>
                	
                </form>
            </div>
            
			<div class="main-content">
				<div class="btn-toolbar list-toolbar">
					<button class="btn btn-primary" id="exportBtn">
						<i class="fa fa-plus"></i>商户导出
					</button>
					<button class="btn btn-default">
						export
					</button>
					<div class="btn-group">
					</div>
				</div>
				
			</div>
				<div style="width: 99%; overflow-x: scroll;">
					<table class="table" width="1500px" height="auto">
						<thead style="height: 20px">
							<tr style="height: 20px">
								<th style="width: 100px">
									#
								</th>
								<th>
									<span style="width: 80px; display: block;">商户编号</span>
								</th>
								<th>
									<span style="width: 80px;  display: inline-block; overflow: hidden">商户名称</span>
								</th>
								<th>
									<span style="width: 90px; display: block;">渠道商户号</span>
								</th>
								<th>
									<span style="width: 80px; display: block;">渠道标识</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">公司编号</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">公司名称</span>
								</th>
								<th>
									<span style="width: 90px; display: block;">商户状态</span>
								</th>
								<th>
									<span style="width: 100px; display: block;">手机号</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">身份证号</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">结算卡号</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">结算银行</span>
								</th>
								<th>
									<span style="width: 110px; display: block;">结算法人</span>
								</th>
								<th>
									<span style="width: 130px; display: block;">微信扫码费率</span>
								</th>
								<th>
									<span style="width: 130px; display: block;">微信wap费率</span>
								</th>
								<th>
									<span style="width: 130px; display: block;">支付宝费率</span>
								</th>
								<th>
									<span style="width: 130px; display: block;">支付宝wap费率</span>
								</th>
								<th>
									<span style="width: 130px; display: block;">QQ费率</span>
								</th>
								<th>
									<span style="width: 130px; display: block;">快捷支付费率</span>
								</th>
								<th>
									<span style="width: 130px; display: block;">网银支付费率</span>
								</th>
								<th>
									<span style="width: 130px; display: block;">单笔代付费/分</span>
								</th>
								<th>
									<span style="width: 130px; display: block;">代扣费率(%)</span>
								</th>
								<th>
									<span style="width: 130px; display: block;">代扣保底费用(分)</span>
								</th>
								<th>
									<span style="width: 90px; display: block;">注册时间</span>
								</th>
								</tr>
						</thead>

						<tbody id="mcht_tbody">
							
						</tbody>
					</table>
				</div>
				
				<ul class="pager">  
          
        <li id="totalNum">总记录条</li>  
        <li>  </li>  
            <li id="firstPage">  
                <a href="javascript:void(0);" onclick="mchtQuery('1')">首页</a>  
            </li>  
            <li >  
                <a id="prePage" href="javascript:void(0);" >上一页</a>  
            </li>  
            <li >  
                <a id="nextPage" href="javascript:void(0);" >下一页</a>  
            </li>  
            <li >  
                <a id="lastPage" href="javascript:void(0);" >尾页</a>  
            </li>  
              
            <li id="currentAndTotal">当前页/</li>  
              
            <li></li>  
            <li><select id="xzPage" name="xzPage" >  
             
            </select></li>  
              
            <li><a href="javascript:void(0);" onclick="mchtQuery(document.getElementById('xzPage').value);">go</a></li>  
        </ul>


</body></html>