<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.manage.bean.PageModle"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="com.manage.bean.OprInfo,com.trade.bean.*,com.gy.util.*"%>
<%@ include file="include.jsp" %>

<html lang="en"><head>
    <title>彩顺网络科技平台</title>
	<script type="text/javascript" src="service/channel_js/channel_verify_head.js"></script>
    <script type="text/javascript" src="service/manage_js/daikouCheck.js"></script>
</head>
<body  onload="IFrameResize()" class=" theme-blue" >

<div>
			<div class="header">
				<h1 class="page-title">
					代扣审核
				</h1>
			</div>
			
			<%--<iframe width="100%" style="border: 0" height="900px" scrolling="no" src="<%= request.getContextPath()%>/manage/tradeInfo.jsp"></iframe>
			
		--%>
		</div>
		<div class="search-well">
                <form class="form-inline" id="trade_form" name="trade_form" style="margin-top:0px;margin-left: 10px" action="manage/tradeQuery" target="_self">
                	<input type="hidden" id="trade_state" name="trade_state" value="CHECK">
                	<input type="hidden" id="trade_source" name="trade_source" value="K">
                	
                	<table >
                	<tr>
                		<td><label>接入订单号：</label></td><td>
                    		<input name="tradeSn" id="tradeSn" class="input-xlarge form-control" placeholder="接入订单号" type="text" ></td>
                    	<td>
            				<label>平台订单号：</label></td><td>
                    		<input name="out_trade_no" id="out_trade_no" class="input-xlarge form-control" placeholder="平台订单号"  type="text">	
          				</td>
          				<td>
            				<label>上游订单号：</label></td><td>
                    		<input name="transaction_id" id="transaction_id" class="input-xlarge form-control" placeholder="平台订单号"  type="text">
          				</td>
          			</tr>
                		<tr>
                		<td><label>交易时间：</label></td>
                			<td><input id="time_start_begin" name="time_start_begin" size="10" type="text" value="" readonly class="form_datetime">
                			- <input id="time_start_end" name="time_start_end" size="10" type="text" value="" readonly class="form_datetime">
                			</td>
                		<td><span><label class="control-label span_auto_width" for="lunch">商户号：</label></span></td><td>
					        <select id="merchantId" name="merchantId" class="selectpicker" data-live-search="true" title="全部">
						        
						     </select>
                    	</td>
                    	<td><input type="hidden" id="pageNum" name="pageNum" value="1"></td><td colspan="3" align="right">
                    		<button class="btn btn-default" type="button" id="trade_query_btn" name="trade_query_btn"><i class="fa fa-search"></i> 查询</button>
                    	</td>
                    	</tr>
                	</table>
                	
                </form>
            </div>
            
			<div class="main-content">
				<div class="btn-toolbar list-toolbar">
					<div class="fa">
						&nbsp;&nbsp;&nbsp;<span id="tradeTotalNum">总笔数:0条</span>&nbsp;&nbsp;&nbsp;<span id="totalAmount">总金额:0元</span>
					</div>
				</div>
				
			</div>
				<div style="width: 99%; overflow-x: scroll;">
					<table class="table" width="1500px" height="auto">
						<thead style="height: 20px">
							<tr style="height: 20px">
								<th style="width: 100px">
									#
								</th>
								<th>
									<span style="width: 110px; display: block;">操作</span>
								</th>
								<th>
									<span style="width: 110px; display: block;">卡号</span>
								</th>
								<th>
									<span style="width: 110px; display: block;">持卡人</span>
								</th>
								<th>
									<span style="width: 110px; display: block;">身份证</span>
								</th>
								<th>
									<span style="width: 110px; display: block;">手机号</span>
								</th>
								<th>
									<span style="width: 110px; display: block;">代扣金额(元)</span>
								</th>
								<th>
									<span style="width: 80px; display: block;">申请时间</span>
								</th>
								<th>
									<span style="width: 80px; display: block;">商户编号</span>
								</th>
								<th>
									<span style="width: 80px; display: block;">商户名称</span>
								</th>
								<th>
									<span style="width: 100px; display: block;">渠道商户号</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">接入方订单号</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">平台订单号</span>
								</th>
								<th>
									<span style="width: 110px; display: block;">费率(%)</span>
								</th>
								<th>
									<span style="width: 110px; display: block;">手续费(元)</span>
								</th>
								<th>
									<span style="width: 110px; display: block;">清算金额(元)</span>
								</th>
								<th>
									<span style="width: 110px; display: block;">接口类型</span>
								</th>
								<th>
									<span style="width: 90px; display: block;">应答码</span>
								</th>
								<th>
									<span
										style="width: 120px; display: inline-block; overflow: hidden">应答码描述</span>
								</th>
							</tr>
						</thead>

						<tbody id="trade_tbody">
							
						</tbody>
					</table>
				</div>
				
				<ul class="pager">  
          
        <li id="totalNum">总记录条</li>  
        <li>  </li>  
            <li id="firstPage">  
                <a href="javascript:void(0);" onclick="tradeQuery('1')">首页</a>  
            </li>  
            <li >  
                <a id="prePage" href="javascript:void(0);" >上一页</a>  
            </li>  
            <li >  
                <a id="nextPage" href="javascript:void(0);" >下一页</a>  
            </li>  
            <li >  
                <a id="lastPage" href="javascript:void(0);" >尾页</a>  
            </li>  
              
            <li id="currentAndTotal">当前页/</li>  
              
            <li></li>  
            <li><select id="xzPage" name="xzPage" >  
             
            </select></li>  
              
            <li><a href="javascript:void(0);" onclick="tradeQuery(document.getElementById('xzPage').value);">go</a></li>  
        </ul>

				
				


</body></html>