<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.manage.bean.PageModle"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="com.manage.bean.OprInfo,com.trade.bean.*,com.gy.util.*"%>
<%@ include file="include.jsp" %>

<html lang="en"><head>
    <title>彩顺网络科技平台</title>
	<script type="text/javascript" src="service/channel_js/channel_verify_head.js"></script>
	<script type="text/javascript" src="service/manage_js/tradeQuery4Admin.js"></script>
</head>
<body  onload="IFrameResize()" class=" theme-blue" >

<div>
			<div class="header">
				<h1 class="page-title">
					扫码支付查询
				</h1>
			</div>
			
			<%--<iframe width="100%" style="border: 0" height="900px" scrolling="no" src="<%= request.getContextPath()%>/manage/tradeInfo.jsp"></iframe>
			
		--%>
		</div>
		<div class="search-well">
                <form class="form-inline" id="trade_form" name="trade_form" style="margin-top:0px;margin-left: 10px" action="manage/tradeQuery" target="_self">
                	<table >
                		<tr>
                		<td><label>交易时间：</label></td>
                			<td><input id="time_start_begin" name="time_start_begin" size="12" type="text" value="" readonly class="form_datetime">
                			- <input id="time_start_end" name="time_start_end" size="12" type="text" value="" readonly class="form_datetime">
                			</td>
                		<td><span><label class="control-label span_auto_width" for="lunch">商户号：</label></span></td><td>
					        <select id="merchantId" name="merchantId" class="selectpicker" data-live-search="true" title="全部">
						        
						     </select>
                    	</td>
                    	<td><label>订单状态：</label></td><td>
					            <select name="trade_state" id="trade_state" class="form-control">
					              <option value="">全部</option>
                                    <option value="SUCCESS" selected="selected">交易成功</option>
					              <option value="REFUND">转入退款</option>
					              <option value="NOTPAY">未支付</option>
					              <option value="CLOSED">已关闭</option>
					              <option value="PAYERROR">支付失败</option>
					            </select>
					     </td>
                    	</tr>
                		<tr>
                		<td><label>接入订单号：</label></td><td>
                    		<input name="tradeSn" id="tradeSn" class="input-xlarge form-control" placeholder="接入订单号" type="text" ></td>
                    	<td>
            				<label>平台订单号：</label></td><td>
                    		<input name="out_trade_no" id="out_trade_no" class="input-xlarge form-control" placeholder="平台订单号"  type="text">	
          				</td>
          				<td>
            				<label>上游订单号：</label></td><td>
                    		<input name="transaction_id" id="transaction_id" class="input-xlarge form-control" placeholder="平台订单号"  type="text">
          				</td>
          			</tr>
          			<tr>
          				<td>
            				<label>渠道编号：</label></td><td>
					            <select id="channel_id" name="channel_id"
										class="selectpicker" data-live-search="true" title="请选择渠道编号">

										</select>
          				</td>
          				<td>
            				<label>渠道商户号：</label></td><td>
                    		<input name="mch_id" id="mch_id" class="input-xlarge form-control" placeholder="渠道商户号"  type="text">
          				</td>
          			    <td><label>交易来源：</label></td><td>
					            <select name="trade_source" id="trade_source" class="form-control">
					              <option value="">全部</option>
					              <option value="2">微信扫码</option>
					              <option value="21">微信公众号</option>
								  <option value="22">微信H5</option>
					              <option value="1">支付宝</option>
					              <option value="3">百度支付</option>
					              <option value="4">手机QQ支付</option>
					              <option value="5">京东</option>
					              <option value="71">银联扫码</option>
					            </select></td>
                		<td><input type="hidden" id="pageNum" name="pageNum" value="1"></td><td colspan="1" align="right">
                    		<button class="btn btn-default" type="button" id="trade_query_btn" name="trade_query_btn"><i class="fa fa-search"></i> 查询</button>
                    	</td>
                    	</tr>
                	</table>
                	
                </form>
            </div>
            
			<div class="main-content">
				<div class="btn-toolbar list-toolbar">
					<button class="btn btn-primary" id="exportBtn">
						<i class="fa fa-plus">交易导出</i>
					</button>
					<button class="btn btn-default">
						export
					</button>
					<div class="fa">
						&nbsp;&nbsp;&nbsp;<span id="tradeTotalNum">总笔数:0条</span>&nbsp;&nbsp;&nbsp;<span id="totalAmount">总金额:0元</span>
					</div>
				</div>
				
			</div>
				<div style="width: 99%; overflow-x: scroll;">
					<table class="table" width="1500px" height="auto">
						<thead style="height: 20px">
							<tr style="height: 20px">
								<th style="width: 100px">
									#
								</th>
								<th>
									<span style="width: 80px; display: block;">交易日期</span>
								</th>
								<th>
									<span style="width: 80px; display: block;">交易时间</span>
								</th>
								<th>
									<span style="width: 110px; display: block;">交易金额(元)</span>
								</th>
								<th>
									<span style="width: 80px; display: block;">商户编号</span>
								</th>
								<th>
									<span style="width: 80px; display: block;">商户名称</span>
								</th>
								<th>
									<span style="width: 100px; display: block;">渠道商户号</span>
								</th>
								<th>
									<span style="width: 90px; display: block;">交易状态</span>
								</th>
								<th>
									<span style="width: 80px; display: block;">交易来源</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">接入方订单号</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">平台订单号</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">上游订单号</span>
								</th>
								<th>
									<span style="width: 110px; display: block;">费率(%)</span>
								</th>
								<th>
									<span style="width: 110px; display: block;">手续费(元)</span>
								</th>
								<th>
									<span style="width: 110px; display: block;">清算金额(元)</span>
								</th>
								<%--<th>
									<span style="width: 100px; display: block;">交易渠道</span>
								</th>
								--%><th>
									<span style="width: 90px; display: block;">接口类型</span>
								</th>
								<th>
									<span style="width: 90px; display: block;">应答码</span>
								</th>
								<th>
									<span
										style="width: 120px; display: inline-block; overflow: hidden">应答码描述</span>
								</th>
								<th>
									<span style="width: 100px; display: block;">支付时间</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">操作</span>
								</th>
								<%--
								<th>
									<span style="width: 80px; display: block;">t0应答码</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">t0应答码描述</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">开户行名称</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">收款卡号</span>
								</th>
								<th>
									<span style="width: 90px; display: block;">收款人姓名</span>
								</th>
								<th>
									<span style="width: 110px; display: block;">手续费(分)</span>
								</th>
							--%></tr>
						</thead>

						<tbody id="trade_tbody">
							
						</tbody>
					</table>
				</div>
				
				<ul class="pager">  
          
        <li id="totalNum">总记录条</li>  
        <li>  </li>  
            <li id="firstPage">  
                <a href="javascript:void(0);" onclick="tradeQuery('1')">首页</a>  
            </li>  
            <li >  
                <a id="prePage" href="javascript:void(0);" >上一页</a>  
            </li>  
            <li >  
                <a id="nextPage" href="javascript:void(0);" >下一页</a>  
            </li>  
            <li >  
                <a id="lastPage" href="javascript:void(0);" >尾页</a>  
            </li>  
              
            <li id="currentAndTotal">当前页/</li>  
              
            <li></li>  
            <li><select id="xzPage" name="xzPage" >  
             
            </select></li>  
              
            <li><a href="javascript:void(0);" onclick="tradeQuery(document.getElementById('xzPage').value);">go</a></li>  
        </ul>

				
				
	<script type="text/javascript">

	</script>

</body></html>