<%@ page language="java" import="java.util.*,com.trade.bean.own.MerchantInf,com.gy.util.CommonFunction" pageEncoding="UTF-8"%>
<%@page import="com.manage.bean.PageModle"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="com.manage.bean.OprInfo,com.trade.bean.*,com.gy.util.*"%>
<%@ include file="include.jsp"%>
<%
OprInfo companyInfo = (OprInfo)request.getAttribute("companyInfo");
RateInfo companyRate = (RateInfo)request.getAttribute("companyRate");
%>
<html lang="en">
	<head>
		<title>彩顺网络科技平台</title>
		<script type="text/javascript">
    function IFrameResize(){ 
    	//alert(this.document.body.scrollHeight); //弹出当前页面的高度 
    	var obj = parent.document.getElementById("main_iframe"); //取得父页面IFrame对象 
    	//alert(obj.height); //弹出父页面中IFrame中设置的高度 
    	obj.height = this.document.body.scrollHeight; //调整父页面中IFrame的高度为此页面的高度 
    	} 

	<%
	if(companyInfo == null ) {
	    companyInfo = new OprInfo();
	%>
		alert("加载公司信息异常");
	<%
	}
	if(companyRate == null ) {
	    companyRate = new RateInfo();
	%>
	alert("加载费率信息异常");
	<%
	}
	%>
    </script>
		<script type="text/javascript" src="service/manage_js/company_update.js"></script>
	</head>

	<body onload="IFrameResize()" class=" theme-blue">
	
		<div >
			<div class="header">
				<h1 class="page-title">
					公司修改
				</h1>
			</div>

			<div class="row">
				<div class="col-md-8">
					<div id="myTabContent" class="tab-content">
						<div class="tab-pane active in" id="home">

							<form id="companyForm" name="companyForm" class="form-horizontal">
							
								<div class="form-group">
									<label for="company_id" class="col-sm-2 control-label">
										公司编号
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="company_id" maxlength="10"
											name="company_id" placeholder="请输入6-10位英文字母或数字" readonly value="<%=StringUtil.trans2Str(companyInfo.getCompany_id()) %>">
									</div>
								</div>
								
								<div class="form-group">
									<label for="phone" class="col-sm-2 control-label">
										公司名称
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="company_name" maxlength="32"
											name="company_name" placeholder="公司名称" value="<%=StringUtil.trans2Str(companyInfo.getCompany_name()) %>">
									</div>
								</div>
								
								<div class="form-group">
									<label for="connact_name" class="col-sm-2 control-label">
										联系人姓名
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="connact_name" autocomplete="off" maxlength="32"
											name="connact_name" placeholder="请输入联系人姓名" value="<%=StringUtil.trans2Str(companyInfo.getConnact_name()) %>">
									</div>
								</div>
								<div class="form-group">
									<label for="identify_no" class="col-sm-2 control-label">
										联系人身份证
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="identify_no" autocomplete="off" 
											name="identify_no" placeholder="请输入联系人身份证" value="<%=StringUtil.trans2Str(companyInfo.getIdentify_no()) %>">
									</div>
								</div>
								
								<div class="form-group">
									<label for="opr_mobile" class="col-sm-2 control-label">
										联系人手机号
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="opr_mobile" autocomplete="off" maxlength="15"
											name="opr_mobile" placeholder="请输入联系人手机号" value="<%=StringUtil.trans2Str(companyInfo.getOpr_mobile()) %>">
									</div>
								</div>
								
								<div class="form-group">
									<label for="address" class="col-sm-2 control-label">
										地址
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="address" autocomplete="off" maxlength="128"
											name="address" placeholder="请输入地址" value="<%=StringUtil.trans2Str(companyInfo.getAddress()) %>">
									</div>
								</div>
								
								<div class="form-group">
									<label for="wechat_fee_value" class="col-sm-2 control-label">
										微信费率
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="wechat_fee_value" onkeyup="value=value.replace(/[^\d.]/g,'')"
											name="wechat_fee_value" placeholder="请输入微信费率(%)" value="<%=StringUtil.trans2Str(companyRate.getWechat_fee_value()) %>">
									</div>
								</div>
								<div class="form-group">
									<label for="alipay_fee_value" class="col-sm-2 control-label">
										支付宝费率
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="alipay_fee_value" onkeyup="value=value.replace(/[^\d.]/g,'')"
											name="alipay_fee_value" placeholder="请输入支付宝费率(%)" value="<%=StringUtil.trans2Str(companyRate.getAlipay_fee_value()) %>">
									</div>
								</div>
								<div class="form-group">
									<label for="alipay_fee_value" class="col-sm-2 control-label">
										QQ钱包费率
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="qq_fee_value" onkeyup="value=value.replace(/[^\d.]/g,'')"
											name="qq_fee_value" placeholder="请输入QQ钱包费率(%)" value="<%=StringUtil.trans2Str(companyRate.getQq_fee_value()) %>">
									</div>
								</div>
								<div class="form-group">
									<label for="quickpay_fee_value" class="col-sm-2 control-label">
										快捷支付费率
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="quickpay_fee_value" onkeyup="value=value.replace(/[^\d.]/g,'')"
											name="quickpay_fee_value" placeholder="请输入快捷支付费率(%)" value="<%=StringUtil.trans2Str(companyRate.getQuickpay_fee_value()) %>">
									</div>
								</div>
								<div class="form-group">
									<label for="netpay_fee_value" class="col-sm-2 control-label">
										网银支付费率
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="netpay_fee_value" onkeyup="value=value.replace(/[^\d.]/g,'')"
											name="netpay_fee_value" placeholder="请输入网银支付费率(%)" value="<%=StringUtil.trans2Str(companyRate.getNetpay_fee_value()) %>">
									</div>
								</div>
								<div class="form-group">
									<label for="single_extra_fee" class="col-sm-2 control-label">
										单笔代付费/分
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="single_extra_fee" onkeyup="value=value.replace(/[^\d.]/g,'')"
											name="single_extra_fee" placeholder="请输入单笔代付手续费(分)" value="<%=StringUtil.trans2Str(companyRate.getSingle_extra_fee()) %>">
									</div>
								</div>
							</form>
						</div>

					</div>
				</div>
			</div>

	<div class="btn-toolbar list-toolbar">
      <button class="btn btn-primary" id="saveMchtBtn"><i class="fa fa-save"></i> 保存</button>
      <a  data-toggle="modal" class="btn btn-danger" id="backBtn">返回</a>
      
      <!-- href="#myModal" -->
    </div>
		</div>
		
			<footer>
                <hr>
                <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                <p>© 2017 ykbpay</p>
                <br>
            </footer>
            
<div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Delete Confirmation</h3>
      </div>
      <div class="modal-body">
        
        <p class="error-text"><i class="fa fa-warning modal-icon"></i>确认保存?</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">取消</button>
        <button class="btn btn-danger" data-dismiss="modal">确定</button>
      </div>
    </div>
  </div>
</div>


	</body>
	
</html>