<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.manage.bean.PageModle"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="com.manage.bean.OprInfo,com.trade.bean.*,com.gy.util.*"%>
<%@ include file="../manage/include.jsp"%>

<html lang="en">
	<head>
		<title>彩顺网络科技平台</title>
		<script type="text/javascript" src="service/channel_js/channel_verify_head.js"></script>
		<script type="text/javascript" src="service/channel_js/channel_verify.js"></script>
	</head>
	<body onload="IFrameResize()" class="theme-blue">

		<div >
			<div class="header">
				<h1 class="page-title">
					通道进件审核
				</h1>
			</div>

			<div class="row">
				<div class="col-md-8">
					<div id="myTabContent" class="tab-content">
						<div class="tab-pane active in" id="home">

							<form id="channelMchtForm" name="channelMchtForm" class="form-horizontal" action="manage/channelMchtVerify" method="post" enctype ="multipart/form-data">
								<div class="form-group">
									<label for="userid" class="col-sm-2 control-label">
										通道机构号
									</label>
									<div class="col-sm-6">
										<select id="userid" name="userid"
										class="selectpicker" data-live-search="true" title="请选择通道机构号">
											<option value='714974' selected="selected">恒丰银行机构号-714974</option>
										</select>
										
									</div>
								</div>
								<div class="form-group">
									<label for="account" class="col-sm-2 control-label">
										通道商户编号
									</label>
									<div class="col-sm-6">
										<select id="account" name="account"
										class="selectpicker" data-live-search="true" title="请选择通道商户编号">
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<label for="real_name" class="col-sm-2 control-label">
										开户人名
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="real_name"
											name="real_name" placeholder="请输入开户人名" value="左松">
									</div>
								</div>
								
								<div class="form-group">
									<label for="cmer" class="col-sm-2 control-label">
										商户名称
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="cmer" maxlength="20"
											name="cmer" placeholder="请输入商户名称10个汉字以内" value="">
									</div>
								</div>
								<div class="form-group">
									<label for="cmer_sort" class="col-sm-2 control-label">
										商户简称
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="cmer_sort"
											name="cmer_sort" placeholder="请输入商户简称10个汉字以内" value="">
									</div>
								</div>
								
								<div class="form-group">
									<label for="channel_code" class="col-sm-2 control-label">
										支付通道
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="channel_code"
											name="channel_code" placeholder="请输入支付通道" readonly="readonly" value="WXPAY">
									</div>
								</div>
								
								<div class="form-group">
									<label for="location" class="col-sm-2 control-label">
										开户城市
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="location"
											name="location" placeholder="请输入开户城市" value="">
									</div>
								</div>
								
								<div class="form-group">
									<label for="card_no" class="col-sm-2 control-label">
										卡号
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="card_no"
											name="card_no" placeholder="请输入卡号" value="6226220618766389">
									</div>
								</div>
								
								<div class="form-group">
									<label for="cert_no" class="col-sm-2 control-label">
										身份证号
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="cert_no"
											name="cert_no" placeholder="请输入身份证号" value="500382198408202155">
									</div>
								</div>
								
								<div class="form-group">
									<label for="mobile" class="col-sm-2 control-label">
										开户时绑定手机号
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="mobile"
											name="mobile" placeholder="请输入手机号" value="15907690877">
									</div>
								</div>
								
								
								<div class="form-group">
									<label for="phone" class="col-sm-2 control-label">
										联系电话
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="phone"
											name="phone" placeholder="请输入联系电话" value="15907690877">
									</div>
								</div>
								
								
								<div class="form-group">
									<label for="regionCode" class="col-sm-2 control-label">
										区编码
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="regionCode"
											name="regionCode" placeholder="请输入区编码" value="310113">
									</div>
								</div>
								
								
								<div class="form-group">
									<label for="address" class="col-sm-2 control-label">
										详细地址
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="address"
											name="address" placeholder="请输入详细地址" value="">
									</div>
								</div>
								
								
								<div class="form-group">
									<label for="cert_correct" class="col-sm-2 control-label">
										身份证正面
									</label>
									<div class="col-sm-6">
										<input id="cert_correct" name="cert_correct" type="file">
									</div>
								</div>
								
								<div class="form-group">
									<label for="cert_opposite" class="col-sm-2 control-label">
										身份证背面
									</label>
									<div class="col-sm-6">
										<input id="cert_opposite" name="cert_opposite" type="file">
									</div>
								</div>
								
								<div class="form-group">
									<label for="cert_meet" class="col-sm-2 control-label">
										手持身份证
									</label>
									<div class="col-sm-6">
										<input id="cert_meet" name="cert_meet" type="file">
									</div>
								</div>
								
								<div class="form-group">
									<label for="card_correct" class="col-sm-2 control-label">
										银行卡正面
									</label>
									<div class="col-sm-6">
										<input id="card_correct" name="card_correct" type="file">
									</div>
								</div>
								
								
								<div class="form-group">
									<label for="card_opposite" class="col-sm-2 control-label">
										银行卡背面
									</label>
									<div class="col-sm-6">
										<input id="card_opposite" name="card_opposite" type="file" >
									</div>
								</div>
								<div class="form-group">
									<label for="bl_img" class="col-sm-2 control-label">
										营业执照
									</label>
									<div class="col-sm-6">
										<input id="bl_img" name="bl_img" type="file">
									</div>
								</div>
								<div class="form-group">
									<label for="door_img" class="col-sm-2 control-label">
										门头照
									</label>
									<div class="col-sm-6">
										<input id="door_img" name="door_img" type="file">
									</div>
								</div>
								<div class="form-group">
									<label for="cashier_img" class="col-sm-2 control-label">
										收银台照
									</label>
									<div class="col-sm-6">
										<input id="cashier_img" name="cashier_img" type="file">
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
	<div class="btn-toolbar list-toolbar">
      <button class="btn btn-primary" id="verifyBtn"><i class="fa fa-save"></i> 验卡</button>
      <a  data-toggle="modal" class="btn btn-danger" id="resetBtn">重置</a>
      <!-- href="#myModal" -->
    </div>
		</div>
			<footer>
                <hr>
                <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                <p>© 2017 ykbpay</p>
                <br>
            </footer>
<div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Delete Confirmation</h3>
      </div>
      <div class="modal-body">
        
        <p class="error-text"><i class="fa fa-warning modal-icon"></i>确认保存?</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">取消</button>
        <button class="btn btn-danger" data-dismiss="modal">确定</button>
      </div>
    </div>
  </div>
</div>


	</body>
</html>