<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.manage.bean.PageModle"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="com.manage.bean.OprInfo,com.trade.bean.*,com.gy.util.*"%>
<%@ include file="../manage/include.jsp"%>

<html lang="en">
	<head>
		<title>彩顺网络科技平台</title>

		<script type="text/javascript" src="service/channel_js/channel_verify_head.js"></script>
		<script type="text/javascript" src="service/channel_js/channel_register.js"></script>
	</head>
	<body onload="IFrameResize()" class=" theme-blue">

		<div >
			<div class="header">
				<h1 class="page-title">
					通道商户注册
				</h1>
			</div>

			<div class="row">
				<div class="col-md-8">
					<div id="myTabContent" class="tab-content">
						<div class="tab-pane active in" id="home">

							<form id="channelMchtForm" name="channelMchtForm" class="form-horizontal">
								<div class="form-group">
									<label for="agtId" class="col-sm-2 control-label">
										通道机构号
									</label>
									<div class="col-sm-6">
										<select id="agtId" name="agtId"
										class="selectpicker" data-live-search="true" title="请选择通道机构号">
											<option value='714974' selected="selected">恒丰银行机构号-714974</option>
										</select>
										
									</div>
								</div>
								<div class="form-group">
									<label for="channel_mcht_no" class="col-sm-2 control-label">
										通道商户编号
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="channel_mcht_no" maxlength="11" onkeyup="value=value.replace(/[^\d]/g,'')"
											name="channel_mcht_no" placeholder="请输入通道商户编号(手机号)">
									</div>
								</div>
								
								<div class="form-group">
									<label for="secret_key" class="col-sm-2 control-label">
										密码
									</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="pass_word" maxlength="10" onkeyup="value=value.replace(/[^\da-zA-Z]/g,'')"
											name="pass_word" placeholder="请输入密码(6-10位英文或数字)">
									</div>
								</div>
								
							</form>
						</div>

					</div>
				</div>
			</div>

	<div class="btn-toolbar list-toolbar">
      <button class="btn btn-primary" id="saveMchtBtn"><i class="fa fa-save"></i> 保存</button>
      <a  data-toggle="modal" class="btn btn-danger" id="resetBtn">重置</a>
      
      <!-- href="#myModal" -->
    </div>
		</div>
		
			<footer>
                <hr>
                <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                <p>© 2017 ykbpay</p>
                <br>
            </footer>
            
<div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Delete Confirmation</h3>
      </div>
      <div class="modal-body">
        
        <p class="error-text"><i class="fa fa-warning modal-icon"></i>确认保存?</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">取消</button>
        <button class="btn btn-danger" data-dismiss="modal">确定</button>
      </div>
    </div>
  </div>
</div>
	</body>
</html>