<%@ page language="java" import="java.util.*,com.trade.bean.own.*,com.gy.util.CommonFunction" pageEncoding="UTF-8"%>
<%@page import="com.manage.bean.PageModle"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="com.manage.bean.OprInfo,com.trade.bean.*,com.gy.util.*"%>
<%@ include file="../manage/include.jsp"%>
<%
QrcodeMchtInfoTmp mchtInfo = (QrcodeMchtInfoTmp)request.getAttribute("mchtInfo");
%>
<html lang="en">
	<head>
		<title>彩顺网络科技平台</title>
		<script type="text/javascript" src="service/channel_js/channel_verify_head.js"></script>
		<script type="text/javascript" src="service/channel_js/jump_mcht_add.js"></script>
	</head>
	<body onload="IFrameResize()" class=" theme-blue">

		<div >
			<div class="header">
				<h1 class="page-title">
					跳码商户新增
				</h1>
			</div>

			<div class="row">
				<div class="col-md-8">
					<div id="myTabContent" class="tab-content">
						<div class="tab-pane active in" id="home">

							<form id="mchtForm" name="mchtForm" class="form-horizontal">
							
							<div class="form-group">
									<label class="control-label col-sm-2" for="lunch">
										跳码组
									</label>
									<div class="col-sm-6">
										<select id="jump_group" name="jump_group"
										class="selectpicker" data-live-search="true" title="选择跳码组">
											
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-sm-2" for="lunch">
										渠道编号
									</label>
									<div class="col-sm-6">
										<select id="channel_id" name="channel_id"
										class="selectpicker" data-live-search="true" title="请选择渠道编号">

										</select>
									</div>
									
								</div>
								
								<div class="form-group">
									<label class="control-label col-sm-2" for="lunch">
										渠道商户
									</label>
									<div class="col-sm-6">
										<select id="channel_mcht_no" name="channel_mcht_no"
										class="selectpicker" data-live-search="true" title="请选择渠道商户">

										</select>
									</div>
									
								</div>
								
								<div class="form-group">
									<label for="tradeSource1" class="col-sm-2 control-label">
										交易来源
									</label>
									<div class="col-sm-6">
										<label class="checkbox-inline">
											<input type="checkbox" id="tradeSource1" name="trade_source" value="1" > 支付宝
										</label>
										<label class="checkbox-inline">
											<input type="checkbox"  name="trade_source" value="12" > 支付宝H5支付
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" id="tradeSource2" name="trade_source" value="2" > 微信扫码支付
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" id="tradeSource21" name="trade_source" value="21"> 微信公众号支付
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" id="tradeSource22" name="trade_source" value="22"> 微信H5支付
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" id="tradeSource4" name="trade_source" value="4" > 手机QQ支付
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" id="tradeSource6" name="trade_source" value="6"> 网银支付
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" id="tradeSource7" name="trade_source" value="7"> 快捷支付
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" id="tradeSource71"
												   name="trade_source" value="71"> 银联扫码
										</label>
									</div>
								</div>
								<input id="group_name" name="group_name" type="hidden">
							</form>
						</div>

					</div>
				</div>
			</div>

	<div class="btn-toolbar list-toolbar">
      <button class="btn btn-primary" id="saveBtn"><i class="fa fa-save"></i> 保存</button>
      <a  data-toggle="modal" class="btn btn-default" id="backBtn">返回</a>
      <!-- href="#myModal" -->
    </div>
		</div>
		
			<footer>
                <hr>
                <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                <p>© 2017 ykbpay</p>
                <br>
            </footer>
            
<div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Delete Confirmation</h3>
      </div>
      <div class="modal-body">
        
        <p class="error-text"><i class="fa fa-warning modal-icon"></i>确认保存?</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">取消</button>
        <button class="btn btn-danger" data-dismiss="modal">确定</button>
      </div>
    </div>
  </div>
</div>


	</body>
</html>