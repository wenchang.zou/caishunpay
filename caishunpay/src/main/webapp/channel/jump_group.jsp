<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.manage.bean.PageModle"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="com.manage.bean.OprInfo,com.trade.bean.*,com.gy.util.*"%>
<%@ include file="../manage/include.jsp" %>

<html lang="en"><head>
    <title>彩顺网络科技平台</title>
	<script type="text/javascript" src="service/channel_js/channel_verify_head.js"></script>
	<script type="text/javascript" src="service/channel_js/jump_group.js"></script>
</head>
<body  onload="IFrameResize()" class=" theme-blue" >
<div>
			<div class="header">
				<h1 class="page-title">
					跳码组维护
				</h1>
			</div>
		</div>
		<div class="search-well">
                <form class="form-inline" id="mcht_form" name="mcht_form" style="margin-top:0px;margin-left: 10px" action="" target="_self">
                	<table >
                		<tr>
                			<td>
            				<label>跳码组：</label></td><td>
					            <select id="jump_group" name="jump_group"
										class="selectpicker" data-live-search="true" title="请选择跳码组">
										</select>
          					</td>
          					<td><label>交易类型：</label></td><td>
					            <select name="trade_source" id="trade_source" class="form-control">
					              <option value="">全部</option>
					              <option value="2">微信扫码</option>
					              <option value="21">微信公众号</option>
					              <option value="1">支付宝</option>
					              <option value="4">手机QQ支付</option>
					              <option value="6">网银支付</option>
					              <option value="7">快捷支付</option>
					            </select></td>
                			<td>
                			<td>
            				<label>渠道编号：</label></td><td>
					            <select id="channel_id" name="channel_id"
										class="selectpicker" data-live-search="true" title="请选择渠道编号">
										</select>
          				</td>
                		</tr>
                		<tr>
          				<td>
            				<label>渠道商户号：</label></td><td>
                    		<input name="channel_mcht_no" id="channel_mcht_no" class="input-xlarge form-control" placeholder="渠道商户号"  type="text">
          				</td>
                		<td><input type="hidden" id="pageNum" name="pageNum" value="1"></td><td colspan="5" align="right">
                    		<button class="btn btn-default" type="button" id="trade_query_btn" name="trade_query_btn"><i class="fa fa-search"></i> 查询</button>
                    	</td>
                    	</tr>
                	</table>
                	
                </form>
            </div>
            
			<div class="main-content">
				<div class="btn-toolbar list-toolbar">
					<button class="btn btn-primary" id="exportBtn">
						<i class="fa fa-plus"></i>跳码组导出
					</button>
					<a class="btn btn-default" href="<%=path %>/channel/jump_group_add.jsp">
						跳码组新增
					</a>
					<a class="btn btn-default" href="<%=path %>/channel/jump_mcht_add.jsp">
						跳码商户新增
					</a>
					<div class="btn-group">
					</div>
				</div>
				
			</div>
				<div style="width: 99%; overflow-x: scroll;">
					<table class="table" width="1500px" height="auto">
						<thead style="height: 20px">
							<tr style="height: 20px">
								<th style="width: 100px">
									#
								</th>
								
								<th>
									<span style="width: 90px; display: block;">跳码组编号</span>
								</th>
								<th>
									<span style="width: 90px;  display: inline-block; overflow: hidden">跳码组名称</span>
								</th>
								<th>
									<span style="width: 90px; display: block;">交易类型</span>
								</th>
								<th>
									<span style="width: 90px; display: block;">渠道编号</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">渠道名称</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">通道商户编号</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">通道商户名称</span>
								</th>
								<th>
									<span style="width: 100px; display: block;">跳码权重</span>
								</th>
								<th>
									<span style="width: 120px; display: block;">操作</span>
								</th>
								</tr>
						</thead>

						<tbody id="mcht_tbody">
							
						</tbody>
					</table>
				</div>
				
				<ul class="pager">  
          
        <li id="totalNum">总记录条</li>  
        <li>  </li>  
            <li id="firstPage">  
                <a href="javascript:void(0);" onclick="jumpGroupQuery('1')">首页</a>  
            </li>  
            <li >  
                <a id="prePage" href="javascript:void(0);" >上一页</a>  
            </li>  
            <li >  
                <a id="nextPage" href="javascript:void(0);" >下一页</a>  
            </li>  
            <li >  
                <a id="lastPage" href="javascript:void(0);" >尾页</a>  
            </li>  
              
            <li id="currentAndTotal">当前页/</li>  
              
            <li></li>  
            <li><select id="xzPage" name="xzPage" >  
             
            </select></li>  
              
            <li><a href="javascript:void(0);" onclick="jumpGroupQuery(document.getElementById('xzPage').value);">go</a></li>  
        </ul>

				
				


</body></html>