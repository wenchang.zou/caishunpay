package com.gy.util;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MapUtil {

	public static Map<String, String> sort(Map<String, String> map) {
		Map<String, String> mapParm = new TreeMap<String, String>(new Comparator<String>() {
			public int compare(String obj1, String obj2) {
				// 降序排序
				return obj1.compareTo(obj2);
			}
		});
		mapParm = map;
		return mapParm;
	}

	/**
	 * 去掉map中 value 为空的数据
	 * 
	 * @param paramMap
	 * @return
	 */
	public static Map<String, String> removeMapEmptyValue(Map<String, String> paramMap) {
		Set<String> set = paramMap.keySet();
		Iterator<String> it = set.iterator();
		while (it.hasNext()) {
			String str = it.next();
			if (paramMap.get(str) == null || paramMap.get(str) == "") {
				paramMap.remove(str);
				set = paramMap.keySet();
				it = set.iterator();
			}
		}
		return paramMap;
	}

	public static void main(String[] args) {
		Map<String, String> map = new HashMap<>();
		map.put("b", "ccccc");
		map.put("d", "aaaaa");
		map.put("c", "bbbbb");
		map.put("a", "ddddd");

		Set<String> keySet = map.keySet();
		Iterator<String> iter = keySet.iterator();
		while (iter.hasNext()) {
			String key = iter.next();
			System.out.println(key + ":" + map.get(key));
		}
	}

}
