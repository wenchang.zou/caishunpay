/*
 * Decompiled with CFR 0_124.
 * 
 * Could not load the following classes:
 *  org.apache.http.HttpEntity
 *  org.apache.http.client.methods.HttpPost
 *  org.apache.http.entity.StringEntity
 *  org.apache.http.params.HttpParams
 *  org.apache.log4j.Logger
 */
package com.gy.util;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.gy.system.Environment;
import com.trade.util.MD5Util;

import antlr.DocBookCodeGenerator;

public final class HttpUtility {
    private static Logger log = Logger.getLogger(HttpUtility.class);

    private HttpUtility() {
    }

    public static PostMethod getPostMethod(Environment env, String xmlRequest){
        PostMethod post = new PostMethod(env.getBaseUrl());
        post.setRequestBody(xmlRequest);
        String contentType = "";
        if (env.getContentType() != null) {
            contentType += env.getContentType();
            if (env.getCharset() != null) {
                contentType += "; charset=" + env.getCharset();
            }
        }
        post.setRequestHeader("Content-Type", contentType);
        if(env.getHeaderParam()!=null){
            for (Map.Entry<String,String> entry : env.getHeaderParam().entrySet())
            post.setRequestHeader(entry.getKey(),entry.getValue());
        }
        log.info("向 "+env.getBaseUrl() +"发送报文:" + xmlRequest);
        return post;
    }

    public static GetMethod getGetMethod(String Url) {
        return new GetMethod(Url);
    }

    public static String postData(Environment env, String xmlRequest) {
        String response = null;
        HttpClient httpclient = new HttpClient();
        PostMethod post = getPostMethod(env,xmlRequest);
        try {
            httpclient.executeMethod(post);
            response = post.getResponseBodyAsString();
        } catch (IOException e) {
            post.releaseConnection();
        }
        log.info("\u8fd4\u56de\u62a5\u6587:" + response);
        return response;
    }

    public static String getData(String url, Map<String, String> params) {
        String response = null;
        HttpClient httpclient = new HttpClient();
        if (params != null) {
            url = url + "?" + MD5Util.map2HttpParam(params);
        }
        GetMethod get = getGetMethod(url);
        try {
            httpclient.executeMethod(get);
            response = get.getResponseBodyAsString();
        } catch (IOException e) {
            get.releaseConnection();
        }
        log.info("\u8fd4\u56de\u62a5\u6587:" + response);
        return response;
    }

    public static Map<String, String> httpParam2map(String paramStr) {
        Map<String, String> map = new HashMap<>();
        for (String s : paramStr.split("&")) {
            String[] as = s.split("=");
            if (as.length == 1) {
                map.put(as[0], "");
            } else {
                try {
                    map.put(as[0], URLDecoder.decode(as[1], "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error("when decode url : " + paramStr, e);
                }
            }
        }
        return map;
    }
    
    /** 
     * 获取post请求响应 
     * @param url 
     * @param params 
     * @return 
     */  
    public static String urlPostMethod(String url,String params) {  
        HttpClient httpClient = new HttpClient();  
        PostMethod method = new PostMethod(url);  
        try {  
            if(params != null && !params.trim().equals("")) {  
                RequestEntity requestEntity = new StringRequestEntity(params,"text/xml","UTF-8");  
                method.setRequestEntity(requestEntity);  
            }  
            method.releaseConnection();  
            httpClient.executeMethod(method);  
            String responses= method.getResponseBodyAsString();  
            return responses;  
        } catch (HttpException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        return null;  
    } 
    public static String urlPostforJson(String url,String params) {  
        HttpPost httpPost = new HttpPost(url);
        CloseableHttpClient client = HttpClients.createDefault();
        String respContent = null;
        
        StringEntity entity = new StringEntity(params,"utf-8");//解决中文乱码问题    
        entity.setContentEncoding("UTF-8");    
        entity.setContentType("application/json");    
        httpPost.setEntity(entity);  
		try {
			HttpResponse resp = client.execute(httpPost);
			if(resp.getStatusLine().getStatusCode() == 200) {
	            HttpEntity he = resp.getEntity();
	            respContent = EntityUtils.toString(he,"UTF-8");
	        }
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        return respContent;  
    } 
    
    public static String doPost(String url, String json) {
   	 String body = null;
        HttpEntity entity1 = null;
        CloseableHttpClient client = HttpClients.createDefault();
		try{
			HttpPost post = new HttpPost(url);
			StringEntity entity = new StringEntity(json, "UTF-8");
			post.setEntity(entity);
			post.setHeader("Content-Type", "application/json;charset=utf-8");
			//logger.info("post:" + post);

	        org.apache.http.HttpResponse httpresponse = client.execute(post);
	        //POST http://106.37.197.53:38080/paygateway HTTP/1.1
	        entity1 = httpresponse.getEntity();
	        body = EntityUtils.toString(entity1, "UTF-8");
	        System.out.println(body);
		}catch (Exception ex) {
			ex.printStackTrace();
			return "exception";
		 } finally {
	            try {
	                EntityUtils.consume(entity1);
	            } catch (IOException e) {
	            }
	           // httpClient.getConnectionManager().shutdown();
	        }
		return body;
	}
    
    public static void main(String[] args) {
		getGetMethod("http://www.baidu.com");
	}
}
