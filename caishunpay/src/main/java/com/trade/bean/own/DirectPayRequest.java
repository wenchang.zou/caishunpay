package com.trade.bean.own;

/**
 * 北京易兴—智讯网关直接接入请求实体类
 * 
 * @author pengzaifeng
 * @version 1.0
 * @since 2018.5.30
 */
public class DirectPayRequest {

	private String merCode;// 给合作商户分配的唯一标识
	
	private float orderAmount;// 订单金额，单位分
	
	private String returnAddress;// 支付完成跳转页面
	
	private String orderNo;// 商户订单号
	
	private String backAddress;// 商户回调通知
	
	private String dateTime;//发起时间
	
	private String payType;// 支付方式：25 b2b网银， 26 b2c网银，  27 快捷支付
	
	private String bankCardType;// 支付卡类型： 1借记卡  2信用卡 （必填，直连方式1、2、5、25此字段填写0，其他方式根据选择1或2）
	
	private String bankCode;// 银行编码 如ICBC （非必填，直连方式为25，26，直连到银行此字段必填，其他方式非必填）
	
	private String sign;// 签名
	
	private String tradeSn;//交易流水号

	public String getMerCode() {
		return merCode;
	}

	public void setMerCode(String merCode) {
		this.merCode = merCode;
	}

	public float getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(float orderAmount) {
		this.orderAmount = orderAmount;
	}

	public String getReturnAddress() {
		return returnAddress;
	}

	public void setReturnAddress(String returnAddress) {
		this.returnAddress = returnAddress;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getBackAddress() {
		return backAddress;
	}

	public void setBackAddress(String backAddress) {
		this.backAddress = backAddress;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getBankCardType() {
		return bankCardType;
	}

	public void setBankCardType(String bankCardType) {
		this.bankCardType = bankCardType;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getTradeSn() {
		return tradeSn;
	}

	public void setTradeSn(String tradeSn) {
		this.tradeSn = tradeSn;
	}



}
