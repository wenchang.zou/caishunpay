
package com.trade.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.gy.util.StringUtil;
import com.trade.service.QuickpayService;
import com.trade.service.ThirdPartyPayDetailService;
import com.trade.service.impl.NotifyProcessingService;
import com.trade.service.quickimpl.QuickpayServiceImpl;
import com.trade.util.JsonUtil;
import com.trade.util.MD5Util;
import com.trade.util.StreamUtil;

/***
 * 支付结果通知接口
 */
@Controller
@RequestMapping(value = {"/notify"})
public class NotifyController {
    private static Logger log = Logger.getLogger(NotifyController.class);

    @Autowired
    private NotifyProcessingService service;
    
    @Autowired
    private ThirdPartyPayDetailService thirdPartyPayDetailService; 
    
    @ResponseBody
    @RequestMapping(value = {"/notifyHaibeiNetshiyunay.do"})
    public String notifyHaibeiNetpay(HttpServletRequest request) {
        String response;
        String requestValue;
        try {
            requestValue = StreamUtil.getString((InputStream) request.getInputStream(), (String) "UTF-8");
            if (requestValue == null)
                return "fail";
            log.info("海贝网银交易结果通知" + requestValue);
            requestValue = URLDecoder.decode(requestValue, "UTF-8");
            Map resultMap = MD5Util.httpParam2Map((String) requestValue);
            this.service.acceptThirdPartyNotify("haibei", resultMap);
            return "OK";
        } catch (Exception e) {
            response = "fail";
            e.printStackTrace();
        }
        return response;
    }

    @ResponseBody
    @RequestMapping(value = {"/notifyHaibei.do"})
    public String notifyHaibei(HttpServletRequest request) {
        String response;
        String requestValue;
        try {
            requestValue = StreamUtil.getString((InputStream) request.getInputStream(), (String) "UTF-8");
            if (requestValue == null)
                return "fail";
            log.info("海贝三方交易结果通知" + requestValue);
            requestValue = URLDecoder.decode(requestValue, "UTF-8");
            Map resultMap = MD5Util.httpParam2Map((String) requestValue);
            this.service.acceptBankCardNotify("haibei", resultMap);
            return "OK";
        } catch (Exception e) {
            response = "fail";
            e.printStackTrace();
        }
        return response;
    }

    @ResponseBody
    @RequestMapping(value = {"/notifyHengfutong.do"})
    public String notifyHengfutong(HttpServletRequest request) {
        String response;
        try {
            request.setCharacterEncoding("utf-8");
            Map<String, String> resultMap = new HashMap<>();
            Enumeration e = request.getParameterNames();
            while (e.hasMoreElements()) {
                String paramName = (String) e.nextElement();
                String paramValue = request.getParameter(paramName);
                resultMap.put(paramName, paramValue);
            }
            this.service.acceptThirdPartyNotify("hengfutong", resultMap);
            return "ISRESPONSION";
        } catch (Exception e) {
            response = "fail";
            e.printStackTrace();
            log.error("恒付通交易结果回传时出错:", e);
        }
        log.info("恒付通交易结果回传:" + response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = {"/notifyWanzhongyunfu.do"})
    public String notifyWanzhongyunfu(HttpServletRequest request, @RequestBody String json) {
        String response;
        try {
            request.setCharacterEncoding("utf-8");
            log.info("get wzyf result string:" + json);
            Map resultMap = JsonUtil.gsonParseJson(json);
            this.service.acceptThirdPartyNotify("wanzhongyunfu", resultMap);
            return "success";
        } catch (Exception e) {
            response = "fail";
            e.printStackTrace();
            log.error("万众云付交易结果回传时出错:", e);
        }
        log.info("万众云付交易结果回传:" + response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = {"/notifyZhihuifu.do"})
    public String notifyZhihuifu(HttpServletRequest request, @RequestParam Map<String, String> params) {
        String response;
        try {
            this.service.acceptThirdPartyNotify("zhihuifu", params);
            return "success";
        } catch (Exception e) {
            response = "fail";
            e.printStackTrace();
            log.error("智慧付回传时出错:", e);
        }
        log.info("万众云付交易结果回传:" + response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = {"/notifyYinshengbaoQP.do"})
    public String notifyYinshengbaoQP(HttpServletRequest request, @RequestParam Map<String, String> params) {
        String response;
        try {
            log.info("get ysb result string:" + JsonUtil.buildJson(params));
            params.put("__TYPE", "quickpay");
            this.service.acceptThirdPartyNotify("yinshengbao", params);
            return "success";
        } catch (Exception e) {
            response = "fail";
            e.printStackTrace();
            log.error("银生宝QP回传时出错:", e);
        }
        log.info("银生宝QP结果回传:" + response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = {"/notifyYinshengbaoNP.do"})
    public String notifyYinshengbaoNP(HttpServletRequest request, @RequestParam Map<String, String> params) {
        String response;
        try {
            log.info("get ysb result string:" + JsonUtil.buildJson(params));
            params.put("__TYPE", "netpay");
            this.service.acceptThirdPartyNotify("yinshengbao", params);
            return "success";
        } catch (Exception e) {
            response = "fail";
            e.printStackTrace();
            log.error("银生宝NP回传时出错:", e);
        }
        log.info("银生宝NP交易结果回传:" + response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = {"/notifyHuanxunNP.do"})
    public String notifyHuanxunNP(HttpServletRequest request, @RequestParam Map<String, String> params) {
        String response;
        try {
            log.info("get huanxun result string:" + JsonUtil.buildJson(params));
            params.put("__TYPE", "netpay");
            this.service.acceptThirdPartyNotify("huanxun", params);
            return "success";
        } catch (Exception e) {
            response = "fail";
            e.printStackTrace();
            log.error("银生宝NP回传时出错:", e);
        }
        log.info("银生宝NP交易结果回传:" + response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = {"/notifyZhiwukeji.do"})
    public String notifyZhiwukeji(HttpServletRequest request, @RequestBody String json) {
        String response;
        try {
            request.setCharacterEncoding("utf-8");
            log.info("get zhiwukeji result string:" + json);
            Map resultMap = JsonUtil.gsonParseJson(json);
            this.service.acceptThirdPartyNotify("zhiwukeji", resultMap);
            return "success";
        } catch (Exception e) {
            response = "fail";
            e.printStackTrace();
            log.error("志武科技回传时出错:", e);
        }
        log.info("志武科技交易结果回传:" + response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = {"/notifyYinshengbao.do"})
    public String notifyYinshengbao(HttpServletRequest request, @RequestBody String json) {
        String response;
        try {
            request.setCharacterEncoding("utf-8");
            log.info("get yinshengbao result string:" + json);
            Map resultMap = JsonUtil.gsonParseJson(json);
            this.service.acceptThirdPartyNotify("yinshengbao", resultMap);
            return "success";
        } catch (Exception e) {
            response = "fail";
            e.printStackTrace();
            log.error("yinshengbao回传时出错:", e);
        }
        log.info("yinshengbao交易结果回传:" + response);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = {"/notifyShiyuntong.do"})
    public String notifyShiyuntong(HttpServletRequest request, @RequestBody String body) {
        String response;
        try {
            request.setCharacterEncoding("utf-8");
            log.info("get shiyuntong result string:" + body);
            body = URLDecoder.decode(body, "UTF-8");
            String sign = body.split("\\|")[0];
            String json = body.split("\\|")[1].replace("=", "");
            Map root = JsonUtil.gsonParseJson(json);
            Map data = (Map) JsonUtil.gsonParseJson(json).remove("data");
            String orderId = data.get("orderId").toString();
            Map<String, String> resultMap = new HashMap<>();
            resultMap.put("rawJson", json);
            resultMap.put("sign", sign);
            resultMap.put("orderId", orderId);
//            resultMap.put("orderAmount", data.get("orderAmount").toString());
            resultMap.put("code", root.get("code").toString());
            resultMap.put("message", root.get("message").toString());
            this.service.acceptThirdPartyNotify("shiyuntong", resultMap);
            return "success";
        } catch (Exception e) {
            response = "fail";
            e.printStackTrace();
            log.error("回传时出错:", e);
        }
        log.info("交易结果回传:" + response);
        return response;
    }
    
    @ResponseBody
    @RequestMapping(value = {"/notifyYiPay.do"})
    public String notifyYiPay(HttpServletRequest request, @RequestBody String body) {
        String response;
        try {
            request.setCharacterEncoding("utf-8");
            log.info("get YiPay result string:" + body);
            body = URLDecoder.decode(body, "UTF-8");
            @SuppressWarnings("unchecked")
			Map<String,String> maps = (Map<String,String>)JSON.parse(body);
            this.service.acceptThirdPartyNotify("yizhifu", maps);
            return "success";
        } catch (Exception e) {
            response = "fail";
            e.printStackTrace();
            log.error("回传时出错:", e);
        }
        log.info("交易结果回传:" + response);
        return response;
    }
    
    @RequestMapping(value = {"/notifyYinShengPay.do"})
    public void notifyYinShengPay(HttpServletRequest request, @RequestBody String body,HttpServletResponse response) {
    	response.setContentType("text/html;charset=utf-8");
        try {
            request.setCharacterEncoding("utf-8");
            log.info("get YinShengPay result string:" + body);
            body = URLDecoder.decode(body, "UTF-8");
            Map<String,String> maps = StringUtil.parseReturn(body);
            this.service.acceptThirdPartyNotify("yinshengPay", maps);
            response.getWriter().write("success");
        } catch (Exception e) {
            try {
				response.getWriter().write("fail");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            e.printStackTrace();
            log.error("回传时出错:", e);
        }
        log.info("交易结果回传:" + response);
    }
    
    @ResponseBody
    @RequestMapping(value = {"/notifyCaiShun.do"})
    public String notifyCaiShun(HttpServletRequest request, @RequestBody String body) {
        String response;
        try {
            request.setCharacterEncoding("utf-8");
            log.info("get caishun result string:" + body);
            body = URLDecoder.decode(body, "UTF-8");
            @SuppressWarnings("unchecked")
			Map<String,Object> resultMap = (Map<String,Object>)JSON.parse(body);
            Map<String,String> map=new HashMap<String,String>();
            for (Map.Entry<String, Object> entry : resultMap.entrySet()) {
            	map.put(entry.getKey(), String.valueOf(entry.getValue()));
    		}
            this.service.acceptThirdPartyNotify("caishun", map);//接受第三方通知
            return "success";
        } catch (Exception e) {
            response = "fail";
            e.printStackTrace();
            log.error("回传时出错:", e);
        }
        log.info ("交易结果回传:" + response);
        return response;
    }
    
    @ResponseBody
    @RequestMapping(value = {"/notifyHanyin.do"})
    public String notifyHanYin(HttpServletRequest request) {
        String response;
        try {
            request.setCharacterEncoding("utf-8");
            String hpMerCode=request.getParameter("hpMerCode");
            String orderNo=request.getParameter("orderNo");
            String transDate=request.getParameter("transDate");
            String transStatus=request.getParameter("transStatus");
            String transAmount=request.getParameter("transAmount");
            String actualAmount=request.getParameter("actualAmount");
            String transSeq=request.getParameter("transSeq");
            String statusCode=request.getParameter("statusCode");
            String statusMsg=request.getParameter("statusMsg");
            String signature=request.getParameter("signature");
            Map<String,String> map=new HashMap<String,String>();
            map.put("hpMerCode", hpMerCode);
            map.put("orderNo", orderNo);
            map.put("transDate", transDate);
            map.put("transStatus", transStatus);
            map.put("transAmount", transAmount);
            map.put("actualAmount", actualAmount);
            map.put("transSeq", transSeq);
            map.put("statusCode", statusCode);
            map.put("statusMsg", statusMsg);
            map.put("signature", signature);
//            this.service.acceptThirdPartyNotify("hanyin", map);//接受第三方通知
            QuickpayService quickpayServiceImpl = QuickpayServiceImpl.switchService("hanyin");
            String result=quickpayServiceImpl.saveResultNotify(map);
            return result;
        } catch (Exception e) {
            response = "fail";
            e.printStackTrace();
            log.error("回传时出错:", e);
        }
        log.info ("交易结果回传:" + response);
        return response;
    }
    
    @RequestMapping(value = {"/notifyXiying.do"})
    public void notifyXiying(HttpServletRequest request,HttpServletResponse response) {
    	response.setContentType("text/html;charset=utf-8");
        try {
            request.setCharacterEncoding("utf-8");
            String version=request.getParameter("version");
            String agentId=request.getParameter("agentId");
            String agentOrderId=request.getParameter("agentOrderId");
            String jnetOrderId=request.getParameter("jnetOrderId");
            String payAmt=request.getParameter("payAmt");
            String payResult=request.getParameter("payResult");
            String payMessage=request.getParameter("payMessage");
            String sign=request.getParameter("sign");
            Map<String,String> map=new HashMap<String,String>();
            map.put("version", version);
            map.put("agentId", agentId);
            map.put("agentOrderId", agentOrderId);
            map.put("jnetOrderId", jnetOrderId);
            map.put("payAmt", payAmt);
            map.put("payResult", payResult);
            map.put("payMessage", payMessage);
            map.put("sign", sign);
            this.service.acceptThirdPartyNotify("xiying", map);//接受第三方通知
            response.getWriter().write("OK");
        } catch (Exception e) {
            try {
				response.getWriter().write("FAIL");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            e.printStackTrace();
            log.error("回传时出错:", e);
        }
        log.info ("交易结果回传:" + response);
    }

    @RequestMapping(value = {"/notifyYiXing.do"})
    public void notifyYiXing(HttpServletRequest request,HttpServletResponse response) { 
        	String responseMsg = "";
        	try {
    	    	int contentLength = request.getContentLength();
    	        // 读取内容
    	        byte responseBody[] = new byte[contentLength];
    	        for (int i = 0; i < contentLength;) {
    	            int readlen = request.getInputStream().read(responseBody, i, contentLength - i);
    	            if (readlen == -1) {
    	                break;
    	            }
    	            i += readlen;
    	        }
    	        // 处理返回的内容
    	        responseMsg = new String(responseBody,"UTF-8");
    	        System.out.println("异步通知返回内容：" + responseMsg);
    	        // 结果集
    	        Map<String,String> map = (Map<String,String>)JSON.parse(responseMsg);
    	        
            this.service.acceptThirdPartyNotify("yixing", map);//接受第三方通知
			response.getWriter().write("SUCCESS");
        } catch (Exception e) {
            try {
				response.getWriter().write("FAIL");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            e.printStackTrace();
            log.error("回传时出错:", e);
        }
        log.info ("交易结果回传:" + response);
    }
    
    
}
