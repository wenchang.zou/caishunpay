package com.trade.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gy.util.CommonFunction;
import com.gy.util.StringUtil;
import com.gy.util.UUIDGenerator;
import com.trade.bean.own.DirectPayRequest;
import com.trade.bean.own.MerchantInf;
import com.trade.bean.own.PayChannelInf;
import com.trade.bean.response.DirectPayResponse;
import com.trade.enums.MchtStatusEnum;
import com.trade.enums.ResponseEnum;
import com.trade.enums.TradeSource;
import com.trade.service.BankCardPayService;
import com.trade.service.MerchantInfService;
import com.trade.util.MD5Util;

@Controller
@RequestMapping("/directPay")
public class DirectPayController {
	
	private static Logger log = Logger.getLogger(DirectPayController.class);
	
	@Autowired
    private MerchantInfService merchantInfServiceImpl;
	
	
	/**
	 * 银行直连接口
	 * @param payRequest
	 * @param request
	 * @return
	 */
	@RequestMapping("/doCreateOrder")
	public DirectPayResponse doCreateOrder(DirectPayRequest payRequest,HttpServletRequest request) {
		DirectPayResponse response=new DirectPayResponse();
		String checkValue = this.checkPayParam(payRequest);
        if (!"00".equals(checkValue)) {
            response.setResultCode(ResponseEnum.FAIL_PARAM.getCode());
            response.setMessage(checkValue);
            return response;
        }
        MerchantInf qrcodeMcht = this.merchantInfServiceImpl.getMchtInfo(payRequest.getMerCode());
        if (qrcodeMcht == null) {
            response.setResultCode(ResponseEnum.FAIL_MCHT_NOT_EXIST.getCode());
            response.setMessage(ResponseEnum.FAIL_MCHT_NOT_EXIST.getMemo());
            return response;
        }
        if (MchtStatusEnum.FREEZE.getCode().equals(qrcodeMcht.getStatus())) {
            response.setResultCode(ResponseEnum.FAIL_MCHT_FREEZE.getCode());
            response.setMessage(ResponseEnum.FAIL_MCHT_FREEZE.getMemo());
            return response;
        }
        String sign = payRequest.getSign();
        payRequest.setSign("");
        String checkSign = MD5Util.getMd5SignByMap(MD5Util.mappingParameterMap(request.getParameterMap()), qrcodeMcht.getSecretKey(),"UTF-8");
        if (!checkSign.equals(sign)) {
            response.setResultCode(ResponseEnum.FAIL_SIGN.getCode());
            response.setMessage(ResponseEnum.FAIL_SIGN.getMemo());
            return response;
        }
        if (!CommonFunction.isTradeSourceOpen(qrcodeMcht.getTrade_source_list(), TradeSource.NETPAY)) {
            response.setResultCode(ResponseEnum.PAY_TYPE_UNSUPPORT.getCode());
            response.setMessage(ResponseEnum.PAY_TYPE_UNSUPPORT.getMemo());
            return response;
        }
        if (StringUtil.isEmpty(qrcodeMcht.getChannel_id()) || StringUtil.isEmpty(qrcodeMcht.getChannelMchtNo())) {
            response.setResultCode(ResponseEnum.UNAUTHOR_ERROR.getCode());
            response.setMessage(ResponseEnum.UNAUTHOR_ERROR.getMemo());
            return response;
        }
        PayChannelInf qrChannelInf = null;
        qrChannelInf = "1".equals(qrcodeMcht.getJump_flag()) ? this.merchantInfServiceImpl.getChannelInfBalance(qrcodeMcht.getJump_group(), TradeSource.NETPAY.getCode()) : this.merchantInfServiceImpl.getChannelInf(qrcodeMcht.getChannel_id(), qrcodeMcht.getChannelMchtNo());
        if (qrChannelInf == null) {
            response.setResultCode(ResponseEnum.ERROR_CHANNEL.getCode());
            response.setMessage(ResponseEnum.ERROR_CHANNEL.getMemo());
            return response;
        }
        
//        payRequest.setTradeSn(UUIDGenerator.getUUID());
		return null;
		
	}
	
	private String checkPayParam(DirectPayRequest payRequest) {
		return "00";
		
	}

}
