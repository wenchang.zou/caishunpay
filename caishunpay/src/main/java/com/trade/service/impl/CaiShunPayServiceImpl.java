package com.trade.service.impl;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gy.system.Environment;
import com.gy.system.SysParamUtil;
import com.gy.util.HttpUtility;
import com.gy.util.JsonUtils;
import com.gy.util.MD5Encrypt;
import com.gy.util.MapUtil;
import com.gy.util.SetUtil;
import com.gy.util.StringUtil;
import com.gy.util.UUIDGenerator;
import com.trade.annotations.PayChannelImplement;
import com.trade.bean.ThirdPartyPayDetail;
import com.trade.bean.own.MerchantInf;
import com.trade.bean.own.PayChannelInf;
import com.trade.bean.own.PayRequest;
import com.trade.dao.ThirdPartyPayDetailDao;
import com.trade.dao.impl.MerchantInfDaoImpl;
import com.trade.enums.TradeSource;
import com.trade.enums.TradeStateEnum;
import com.trade.service.ThirdPartyPayService;
import com.trade.util.Base64;
import com.trade.util.JsonUtil;
import com.trade.util.caishun.SignUtil;

@PayChannelImplement(channelId = "caishun")
public class CaiShunPayServiceImpl implements ThirdPartyPayService {
    @Autowired
    private ThirdPartyPayDetailDao thirdPartyPayDetailDao;
    
    @Autowired
    private MerchantInfDaoImpl merchantInfDaoImpl;

    private static Logger log = Logger.getLogger(CaiShunPayServiceImpl.class);


    private String toAmountString(Double amount) {
        return String.format("%.2f", amount);
    }

    @Override
    public ThirdPartyPayDetail doOrderCreate(PayRequest payRequest, MerchantInf qrcodeMcht, PayChannelInf qrChannelInf) {
            return doNetPayOrderCreate(payRequest, qrcodeMcht, qrChannelInf);
    }

    private ThirdPartyPayDetail doNetPayOrderCreate(PayRequest payRequest, MerchantInf qrcodeMcht, PayChannelInf qrChannelInf) {
        String tradeNo = UUIDGenerator.getOrderIdByUUId((int) 20);
        String orderAmount = toAmountString(payRequest.getOrderAmount() / 100.0);
        Map<String, String> params = new LinkedHashMap<>();
        params.put("tradeType", "cs.pay.submit");//交易类型
        params.put("version", "1.3");//版本
        params.put("channel", "unionpayQR");//支付类型
        params.put("mchId",payRequest.getMerchantId());//代理商户号
        params.put("subMchId", qrcodeMcht.getChannelMchtNo());//商户号
        params.put("body", payRequest.getGoodsName());//商品描述
        params.put("outTradeNo", tradeNo);//商户订单号
        params.put("amount", orderAmount);//交易金额
        params.put("currency", "CNY");//货币类型
        params.put("subject", payRequest.getGoodsName());//商品标题
        params.put("notifyUrl", SysParamUtil.getParam("CAISHUN_NOTIFY_URL"));
        try {
    		//拼接
    		String toSign = SignUtil.createLinkString(params)+"&key="+qrChannelInf.getSecret_key();
    		System.out.println("签名串："+toSign);
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] signed = md5.digest(toSign.getBytes("UTF-8"));
            // 3.生成签名:
            String sign = Hex.encodeHexString(signed).toUpperCase();
    		params.put("sign", sign);
		} catch (Exception e) {
			e.printStackTrace();
		}//签名
        String reqStr= JsonUtils.toJson(params);
        String jsonResult = HttpUtility.urlPostforJson(Environment.CAISHUN_NETPAY_URL.getBaseUrl(), reqStr);//进行post请求
        Map returnMap = (Map) JsonUtil.parseJson(jsonResult);
        ThirdPartyPayDetail detail = ThirdPartyPayDetail.ThirdPartyPayDetailBuilder.getBuilder()
                .isSuccess(true)
                .withLocalTradeNumber(tradeNo)
                .withPayRequest(payRequest)
                .withPayChannelInf(qrChannelInf)
                .withTradeSource(payRequest.getTradeSource())
                .withTradeState(TradeStateEnum.NOTPAY)
                .withNotifyUrl(SysParamUtil.getParam("CAISHUN_NOTIFY_URL"))
                .withCodeUrl((String)returnMap.get("codeUrl"))
                .build();
        return detail;
    }

    public ThirdPartyPayDetail doOrderQuery(ThirdPartyPayDetail thirdPartyPayDetail, PayChannelInf qrChannelInf) {
    	if (thirdPartyPayDetail != null && StringUtil.isNotEmpty((String[]) new String[]{thirdPartyPayDetail.getCode_url()}) && !TradeStateEnum.SUCCESS.getCode().equals(thirdPartyPayDetail.getTrade_state())) {
//            Environment env = Environment.YI_PAY_QUERY_H5;
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("tradeType", "cs.trade.single.query");
            params.put("version", "1.3");
            params.put("mchId", thirdPartyPayDetail.getMerchantId());
            params.put("subMchId", qrChannelInf.getChannel_mcht_no());
            params.put("outTradeNo", thirdPartyPayDetail.getOut_trade_no());
            params.put("queryType", "1");
            try {
        		//拼接
        		String toSign = SignUtil.createLinkString(params)+"&key="+qrChannelInf.getSecret_key();
        		System.out.println("签名串："+toSign);
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                byte[] signed = md5.digest(toSign.getBytes("UTF-8"));
                // 3.生成签名:
                String sign = Hex.encodeHexString(signed).toUpperCase();
        		params.put("sign", sign);
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}//签名
            String reqStr= JsonUtils.toJson(params);
            String jsonResult = HttpUtility.urlPostforJson(Environment.CAISHUN_NETPAY_URL.getBaseUrl(), reqStr);//进行post请求
            Map returnMap = (Map) JsonUtil.parseJson((String) jsonResult);
            if (returnMap != null) {
                String resultCode = StringUtil.trans2Str(returnMap.get("resultCode"));
                String returnMsg = StringUtil.trans2Str(returnMap.get("returnMsg"));
                String returnCode = StringUtil.trans2Str(returnMap.get("returnCode"));
                if ("0".equals(resultCode) && "0".equals(returnCode)) {
                    String status = (String) returnMap.get("status");
                    String payType = (String) returnMap.get("channel");
                    String orderNo = (String) returnMap.get("outTradeNo");
                    String backSign = (String) returnMap.remove("sign");
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("resultCode", resultCode);
                    map.put("returnMsg", returnMsg);
                    map.put("returnCode", returnCode);
                    map.put("errCode", (String) returnMap.get("errCode"));
                    map.put("errCodeDes", (String) returnMap.get("errCodeDes"));
                    map.put("channel", (String) returnMap.get("channel"));
                    map.put("payChannelTypeCode", (String) returnMap.get("payChannelTypeCode"));
                    map.put("terminalType", (String) returnMap.get("terminalType"));
                    map.put("cashierName", (String) returnMap.get("cashierName"));
                    map.put("outTradeNo", (String) returnMap.get("outTradeNo"));
                    map.put("outChannelNo", (String) returnMap.get("outChannelNo"));
                    map.put("body", (String) returnMap.get("body"));
                    map.put("currency", (String) returnMap.get("currency"));
                    map.put("amount", String.valueOf(returnMap.get("amount")) );
                    map.put("transTime", (String) returnMap.get("transTime"));
                    map.put("postscript", (String) returnMap.get("postscript"));
                    map.put("subject", (String) returnMap.get("subject"));
                    map.put("status", (String) returnMap.get("status"));
                    map.put("outRefundNo", (String) returnMap.get("outRefundNo"));
                    map.put("channelRefundNo", (String) returnMap.get("channelRefundNo"));
                    map.put("outRefundTime", (String) returnMap.get("outRefundTime"));
                    map.put("refundStatus", (String) returnMap.get("refundStatus"));
                    map.put("payChannelType", (String) returnMap.get("payChannelType"));
                    map=(HashMap<String, String>) MapUtil.removeMapEmptyValue(map);
                    String toSign = SignUtil.createLinkString(map);//+"&key="+qrChannelInf.getSecret_key();
            		System.out.println("签名串："+toSign);
            		String checkSign = SignUtil.genSign(qrChannelInf.getSecret_key(), toSign);
            		
                    if (!checkSign.equals(backSign)) {
                        return thirdPartyPayDetail;
                    }
                    thirdPartyPayDetail.setTrade_state(status);
                    thirdPartyPayDetail.setErr_msg(returnMsg);
                    thirdPartyPayDetail.setPay_result(resultCode);
                    thirdPartyPayDetail.setTrade_type(payType);
                    thirdPartyPayDetail.setOut_transaction_id(orderNo);
                    thirdPartyPayDetail.setTime_end(com.gy.util.DateUtil.getCurrentTime());
                }
            }
        }
        return thirdPartyPayDetail;
    }

    @Override
    public Set<TradeSource> supportedThirdPartyTradeSource() {
        return SetUtil.toSet(TradeSource.UNIPAY_QRCODE);
    }

    /**
	 * 当交易状态不是成功时,会进入此方法回调
	 * */
	    private void processResultMap(ThirdPartyPayDetail payDetail, Map returnMap) {
	        if (returnMap != null) {
	            String resultCode = StringUtil.trans2Str(returnMap.get("returnCode"));
	            String resultMsg = StringUtil.trans2Str(returnMap.get("returnMsg"));//返回的错误信息
	            String orderNo = StringUtil.trans2Str(returnMap.get("outTradeNo"));
	            if (!payDetail.getOut_trade_no().equals(orderNo)) {
	                log.error("paydetail not match, will not update.");
	                return;
	            }
	            String orderStatus = returnMap.get("status").toString();
	            TradeStateEnum tradeStateEnum = getTradeStateByResultId(orderStatus);//查询返回码的信息
	            if (orderStatus != null) {
	                payDetail.setResp_code(resultCode);
	                payDetail.setErr_msg(resultMsg);
	                payDetail.setTrade_state(tradeStateEnum.getCode());
	            } else {
	                log.error("parameter is illegal, skip");
	            }
	        }
	    }
	
	
	@Override
	public ThirdPartyPayDetail acceptThirdPartyPayNotify(Map<String, String> returnMap) {

        ThirdPartyPayDetail payDetail = this.thirdPartyPayDetailDao.getById(returnMap.get("outTradeNo"));
        try {
            PayChannelInf qrChannelInf = this.merchantInfDaoImpl.getChannelInf(payDetail.getChannel_id(), payDetail.getMch_id());
            HashMap<String, String> map = new HashMap<String, String>();
            String status=(String) returnMap.get("status");
            String sign=(String) returnMap.get("sign");
            map.put("resultCode", (String) returnMap.get("resultCode"));
            map.put("returnMsg", (String) returnMap.get("returnMsg"));
            map.put("returnCode", (String) returnMap.get("returnCode"));
            map.put("errCode", (String) returnMap.get("errCode"));
            map.put("errCodeDes", (String) returnMap.get("errCodeDes"));
            map.put("channel", (String) returnMap.get("channel"));
            map.put("payChannelTypeCode", (String) returnMap.get("payChannelTypeCode"));
            map.put("terminalType", (String) returnMap.get("terminalType"));
            map.put("cashierName", (String) returnMap.get("cashierName"));
            map.put("outTradeNo", (String) returnMap.get("outTradeNo"));
            map.put("outChannelNo", (String) returnMap.get("outChannelNo"));
            map.put("body", (String) returnMap.get("body"));
            map.put("currency", (String) returnMap.get("currency"));
            map.put("amount", String.valueOf(returnMap.get("amount")) );
            map.put("transTime", (String) returnMap.get("transTime"));
            map.put("postscript", (String) returnMap.get("postscript"));
            map.put("subject", (String) returnMap.get("subject"));
            map.put("status", (String) returnMap.get("status"));
            map.put("outRefundNo", (String) returnMap.get("outRefundNo"));
            map.put("channelRefundNo", (String) returnMap.get("channelRefundNo"));
            map.put("outRefundTime", (String) returnMap.get("outRefundTime"));
            map.put("refundStatus", (String) returnMap.get("refundStatus"));
            map.put("payChannelType", (String) returnMap.get("payChannelType"));
            map=(HashMap<String, String>) MapUtil.removeMapEmptyValue(map);
            String toSign = SignUtil.createLinkString(map);//+"&key="+qrChannelInf.getSecret_key();
    		System.out.println("签名串："+toSign);
    		String checkSign = SignUtil.genSign(qrChannelInf.getSecret_key(), toSign);
    		
    		System.out.println("我们生成的签名："+sign);
    		System.out.println("传过来的签名："+checkSign);
    		
            if (!sign.equals(checkSign)) {
                return payDetail;
            }
            if (!TradeStateEnum.SUCCESS.getCode().equals(payDetail.getTrade_state())) {
                processResultMap(payDetail, returnMap);//这里进入processResultMap方法
            }
            
            if(status.equals("05")) {
                String outRefundNo = returnMap.get("outRefundNo");
                String channelRefundNo = returnMap.get("channelRefundNo");
                String outRefundTime = returnMap.get("outRefundTime");
                String refundStatus = returnMap.get("refundStatus");
                log.error("状态为退款：" + outRefundNo + ", " + channelRefundNo+ ", " + outRefundTime+ ", " + refundStatus);
            }
            
            System.out.println(payDetail);
            
            return payDetail;
        } catch (Exception e) {
            log.error("when processing notify", e);
            e.printStackTrace();
            return payDetail;
        }
	}
    
    private TradeStateEnum getTradeStateByResultId(String orderStatus) {
        switch (orderStatus) {
            case "01":
                return TradeStateEnum.NOTPAY;//未支付 
            case "02":
                return TradeStateEnum.PREPAID;//已支付
            case "03":
                return TradeStateEnum.PRESUCCESS;//交易成功
            case "04":
                return TradeStateEnum.CLOSED;//已关闭
            case "05":
                return TradeStateEnum.REFUND;//转入退款
            case "10":
                return TradeStateEnum.OVERTIME;//订单超时
            default:
                return TradeStateEnum.PAYERROR;//支付失败
        }
    }
}
