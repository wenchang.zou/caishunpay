package com.trade.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.gy.system.SysParamUtil;
import com.gy.util.DateUtil;
import com.gy.util.SetUtil;
import com.gy.util.StringUtil;
import com.gy.util.UUIDGenerator;
import com.trade.annotations.PayChannelImplement;
import com.trade.bean.ThirdPartyPayDetail;
import com.trade.bean.own.MerchantInf;
import com.trade.bean.own.PayChannelInf;
import com.trade.bean.own.PayRequest;
import com.trade.dao.ThirdPartyPayDetailDao;
import com.trade.enums.TradeSource;
import com.trade.enums.TradeStateEnum;
import com.trade.service.ThirdPartyPayService;
import com.trade.util.JsonUtil;
import com.trade.util.yinshengpay.ApipaySubmit;

@PayChannelImplement(channelId = "yinshengPay")
public class YinShengPayServiceImpl implements ThirdPartyPayService {
    @Autowired
    private ThirdPartyPayDetailDao thirdPartyPayDetailDao;

    private static final String YINSHENGPAY_NETPAY_URL = "https://qrcode.ysepay.com/gateway.do";

    private static Logger log = Logger.getLogger(YinShengPayServiceImpl.class);


    private String toAmountString(Double amount) {
        return String.format("%.2f", amount);
    }

    @Override
    public ThirdPartyPayDetail doOrderCreate(PayRequest payRequest, MerchantInf qrcodeMcht, PayChannelInf qrChannelInf) {
            return doNetPayOrderCreate(payRequest, qrcodeMcht, qrChannelInf);
    }

    private ThirdPartyPayDetail doNetPayOrderCreate(PayRequest payRequest, MerchantInf qrcodeMcht, PayChannelInf qrChannelInf) {
        
    	System.out.println("商户名："+qrcodeMcht.getMchtName());
    	
    	String tradeNo = UUIDGenerator.getOrderIdByUUId((int) 20);
        Map<String, String> param = new HashMap<String, String>();
        param.put("method", "ysepay.online.qrcodepay");//接口名称
        param.put("partner_id", qrChannelInf.getChannel_mcht_no());//商户号
        param.put("timestamp", DateUtil.dateToString(new Date()));//发送请求的时间
        param.put("charset", "UTF-8");//商户网站使用的编码格式
        param.put("sign_type", "RSA");//签名类型
        param.put("notify_url", SysParamUtil.getParam("YINSHENGPAY_NOTIFY_URL"));//"xiyingbuy.com");////
        param.put("version","3.0");//接口版本
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("seller_name",qrcodeMcht.getMchtName());//收款方银盛支付客户名
        map.put("total_amount",toAmountString(payRequest.getOrderAmount() / 100.0));//金额
        map.put("timeout_express", "96h");//设置未付款交易的超时时间
        map.put("subject",payRequest.getGoodsName());//标题
        map.put("business_code", "01000010");//业务代码
        map.put("bank_type","1902000");//二维码行别 微信-1902000 支付宝-1903000   QQ扫码-1904000 银联扫码-9001002
        map.put("out_trade_no", tradeNo);//订单号
        map.put("return_url", payRequest.getCallback_url());//同步通知地址
        map.put("seller_id", qrChannelInf.getChannel_mcht_no());//收款方银盛支付用户号
        map.put("currency", "CNY");//支持币种
        JSONObject json = new JSONObject(map);
		param.put("biz_content",json.toString());//
		System.out.println("请求数据："+param);
		String jsonResult =ApipaySubmit.backgroundURL(YINSHENGPAY_NETPAY_URL,param, qrcodeMcht.getSecretKey());
		Map maps = (Map)JSON.parse(jsonResult);
		String s=maps.get("ysepay_online_qrcodepay_response").toString();
		Map<String, String> params = JSONObject.parseObject(s, new TypeReference<Map<String, String>>(){});
        ThirdPartyPayDetail detail = ThirdPartyPayDetail.ThirdPartyPayDetailBuilder.getBuilder()
                .isSuccess(true)
                .withLocalTradeNumber(tradeNo)
                .withPayRequest(payRequest)
                .withPayChannelInf(qrChannelInf)
                .withTradeSource(payRequest.getTradeSource())
                .withTradeState(TradeStateEnum.NOTPAY)
                .withNotifyUrl(SysParamUtil.getParam("YINSHENGPAY_NOTIFY_URL"))
                .withCodeUrl((String)params.get("qr_code_url"))
                .build();
        return detail;
    }

    public ThirdPartyPayDetail doOrderQuery(ThirdPartyPayDetail thirdPartyPayDetail, PayChannelInf qrChannelInf) {
        return thirdPartyPayDetail;
    }

    @Override
    public Set<TradeSource> supportedThirdPartyTradeSource() {
        return SetUtil.toSet(TradeSource.WEPAY, TradeSource.WE_WAP_PAY);
    }

    public ThirdPartyPayDetail acceptThirdPartyPayNotify(Map<String, String> resultMap) {
    	String orderNo = resultMap.get("out_trade_no");
        ThirdPartyPayDetail payDetail = this.thirdPartyPayDetailDao.getById(orderNo);
        try {           
            if (!ApipaySubmit.verifySign(resultMap)) {
                log.error("notifyYinShengPay sign verify error!!! ");
                return payDetail;
            }
            if (!TradeStateEnum.SUCCESS.getCode().equals(payDetail.getTrade_state())) {
                processResultMap(payDetail, resultMap);
            }
            return payDetail;
        } catch (Exception e) {
            log.error("when processing notify", e);
            e.printStackTrace();
            return payDetail;
        }
    }
    
    private void processResultMap(ThirdPartyPayDetail payDetail, Map returnMap) {
        if (returnMap != null) {
            String trade_status = StringUtil.trans2Str(returnMap.get("trade_status"));
            //String resultMsg = StringUtil.trans2Str(returnMap.get("resultMsg"));
            String orderNo = StringUtil.trans2Str(returnMap.get("out_trade_no"));
            if (!payDetail.getOut_trade_no().equals(orderNo)) {
                log.error("paydetail not match, will not update.");
                return;
            }
            if("TRADE_SUCCESS".equals(trade_status)) {
            	payDetail.setResp_code("000000");
                payDetail.setErr_msg(trade_status);
                payDetail.setTrade_state("SUCCESS");
            }else {
            	payDetail.setResp_code("111111");
                payDetail.setErr_msg(trade_status);
                payDetail.setTrade_state("Fail");
            }
            
        }
    }
}
