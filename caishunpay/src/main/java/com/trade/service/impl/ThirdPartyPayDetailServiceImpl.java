package com.trade.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trade.bean.ThirdPartyPayDetail;
import com.trade.dao.ThirdPartyPayDetailDao;
import com.trade.service.ThirdPartyPayDetailService;

@Service
public class ThirdPartyPayDetailServiceImpl implements ThirdPartyPayDetailService {

	@Autowired
	private ThirdPartyPayDetailDao thirdPartyPayDetailDao;
	
	public ThirdPartyPayDetail getByOuTTradeNO(String orderNO) {
		return thirdPartyPayDetailDao.getById(orderNO);
	}

}
