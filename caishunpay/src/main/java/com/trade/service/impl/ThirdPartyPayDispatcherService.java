/*
 * Decompiled with CFR 0_124.
 * 
 * Could not load the following classes:
 *  org.apache.log4j.Logger
 *  org.springframework.beans.factory.annotation.Autowired
 *  org.springframework.stereotype.Service
 */
package com.trade.service.impl;

import com.account.service.TradeMchtAccountService;
import com.gy.util.ContextUtil;
import com.trade.bean.ThirdPartyPayDetail;
import com.trade.bean.own.MerchantInf;
import com.trade.bean.own.PayChannelInf;
import com.trade.bean.own.PayRequest;
import com.trade.bean.response.Response;
import com.trade.bean.response.ThirdPartyPayResponse;
import com.trade.controller.H5PayController;
import com.trade.dao.MerchantInfDao;
import com.trade.dao.ThirdPartyPayDetailDao;
import com.trade.enums.*;
import com.trade.service.BankCardPayService;
import com.trade.service.MerchantInfService;
import com.trade.service.ThirdPartyPayService;
import com.trade.service.WappayService;
import com.trade.util.FeeCountUtil;
import com.trade.util.ReflectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ThirdPartyPayDispatcherService {
    private static Logger log = Logger.getLogger(ThirdPartyPayDispatcherService.class);
    @Autowired
    private ThirdPartyPayDetailDao thirdPartyPayDetailDao;
    @Autowired
    private MerchantInfDao merchantInfDaoImpl;
    @Autowired
    protected TradeMchtAccountService tradeMchtAccountService;
    @Autowired
    private MerchantInfService merchantInfService;

    @Autowired
    private List<ThirdPartyPayService> thirdPartyPayServices;

    @Autowired
    private List<WappayService> wappayServices;

    private static Map<String, ThirdPartyPayService> thirdPartyServiceMap = new HashMap<>();
    private static Map<String, WappayService> wapPayServiceMap = new HashMap<>();

    @PostConstruct
    public void onInitialize() {
        for (ThirdPartyPayService service : thirdPartyPayServices) {
            ReflectionUtils.putServiceTo(service, thirdPartyServiceMap);
        }
        for (WappayService service : wappayServices) {
            ReflectionUtils.putServiceTo(service, wapPayServiceMap);
        }
    }

    public Response doOrderCreate(PayRequest payRequest) {
        MerchantInf qrcodeMcht = this.merchantInfService.getMchtInfo(payRequest.getMerchantId());
        if (qrcodeMcht == null)
            return Response.with(ResponseEnum.FAIL_MCHT_NOT_EXIST);
        if (MchtStatusEnum.FREEZE.getCode().equals(qrcodeMcht.getStatus()))
            return Response.with(ResponseEnum.FAIL_MCHT_FREEZE);
        TradeSource tradeSource = payRequest.getTradeSource();
        PayChannelInf payChannelInf = merchantInfService.getChannelInf(qrcodeMcht, tradeSource);
        if (payChannelInf == null) {
            return Response.with(ResponseEnum.UNAUTHOR_ERROR);
        }
        if (ChannelStatusEnum.FREEZE.getCode().equals(payChannelInf.getStatus())) {
            return Response.with(ResponseEnum.ERROR_CHANNEL);
        }
        ThirdPartyPayService service = switchService(payChannelInf.getChannel_id());
        System.out.println(payChannelInf.getChannel_id());
        if (!service.supportedThirdPartyTradeSource().contains(payRequest.getTradeSource())) {
        	System.out.println(service.supportedThirdPartyTradeSource());
        	System.out.println(payRequest.getTradeSource());
        	System.out.println(service.supportedThirdPartyTradeSource().contains(payRequest.getTradeSource()));
            return Response.with(ResponseEnum.ERROR_UNSUPPORT);
        }
        if (this.thirdPartyPayDetailDao.getByTradesn(payRequest.getTradeSn(), payRequest.getMerchantId()) != null) {
        	System.out.println(payRequest.getTradeSn());
        	System.out.println(payRequest.getMerchantId());
            return Response.with(ResponseEnum.FAIL_ORDER_NO_REPEAT);
        }
        ThirdPartyPayDetail thirdPartyPayDetail = service.doOrderCreate(payRequest, qrcodeMcht, payChannelInf);
        FeeCountUtil.countMchtFee(thirdPartyPayDetail, qrcodeMcht);
        thirdPartyPayDetailDao.save(thirdPartyPayDetail);
        if (thirdPartyPayDetail.isSuccess()) {
            ThirdPartyPayResponse response = ThirdPartyPayResponse
                    .success(thirdPartyPayDetail.getCode_url(), thirdPartyPayDetail.getMerchantId());
            if(qrcodeMcht.getChannel_id().trim().equals("xiying") || qrcodeMcht.getChannel_id().trim().equals("yinshengPay")) {
            	response.setImg_url(thirdPartyPayDetail.getCode_url());
            }else {
            	response.setImg_url(H5PayController.makeQrCodeUrl(thirdPartyPayDetail.getCode_url()));
            }
            
            return response;
        } else {
            return ThirdPartyPayResponse.fail(ResponseEnum.CHANNEL_ERROR);
        }
    }

    public ThirdPartyPayDetail doOrderQuery(String outTransactionId) {
        ThirdPartyPayDetail detail = this.thirdPartyPayDetailDao.getById(outTransactionId);
        return this.doOrderQuery(detail.getTradeSn(), detail.getMerchantId());
    }

    public ThirdPartyPayDetail doOrderQuery(String tradeSn, String gyMchtId) {
        ThirdPartyPayDetail thirdPartyPayDetail = this.thirdPartyPayDetailDao.getByTradesn(tradeSn, gyMchtId);
        if (thirdPartyPayDetail == null) {
            return null;
        }
        if (!TradeStateEnum.SUCCESS.getCode().equals(thirdPartyPayDetail.getTrade_state())) {
            ThirdPartyPayService thirdPartyPayService = switchService(thirdPartyPayDetail.getChannel_id());
            PayChannelInf qrChannelInf = this.merchantInfDaoImpl.getChannelInf(thirdPartyPayDetail.getChannel_id(), thirdPartyPayDetail.getMch_id());
            thirdPartyPayDetail = thirdPartyPayService.doOrderQuery(thirdPartyPayDetail, qrChannelInf);
            this.thirdPartyPayDetailDao.update(thirdPartyPayDetail);
        }
        return thirdPartyPayDetail;
    }

    public ThirdPartyPayService switchService(String channelId) {
        return thirdPartyServiceMap.get(channelId);
    }

    public WappayService switchWapService(String channelId) {
        return wapPayServiceMap.get(channelId);
    }
}
