package com.trade.service.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gy.system.Environment;
import com.gy.system.SysParamUtil;
import com.gy.util.MD5Encrypt;
import com.gy.util.SetUtil;
import com.gy.util.StringUtil;
import com.gy.util.UUIDGenerator;
import com.trade.annotations.PayChannelImplement;
import com.trade.bean.ThirdPartyPayDetail;
import com.trade.bean.own.MerchantInf;
import com.trade.bean.own.PayChannelInf;
import com.trade.bean.own.PayRequest;
import com.trade.controller.RedirectController;
import com.trade.dao.MerchantInfDao;
import com.trade.dao.ThirdPartyPayDetailDao;
import com.trade.enums.TradeSource;
import com.trade.enums.TradeStateEnum;
import com.trade.exception.RequestLimitedException;
import com.trade.service.ThirdPartyPayService;
import com.trade.util.Base64;
import com.trade.util.MD5Util;

@PayChannelImplement(channelId = "yizhifu")
public class YiZhiFuServiceImpl implements ThirdPartyPayService{
    private static Logger log = Logger.getLogger(YiZhiFuServiceImpl.class);
    
    @Autowired
    private ThirdPartyPayDetailDao thirdPartyPayDetailDao;
    @Autowired
    private MerchantInfDao merchantInfDaoImpl;

    private String toAmountString(Double amount) {
        return String.format("%.2f", amount);
    }
    private static Map<TradeSource, String> serviceNameMap = new HashMap<>();

    static {
        serviceNameMap.put(TradeSource.WEPAY, "1");
        serviceNameMap.put(TradeSource.ALIPAY, "2");
        serviceNameMap.put(TradeSource.QQPAY, "5");
        serviceNameMap.put(TradeSource.JDPAY, "36");
        serviceNameMap.put(TradeSource.UNIPAY_QRCODE, "37");
    }
    
    
    
    @Override
    public ThirdPartyPayDetail doOrderCreate(PayRequest payRequest, MerchantInf qrcodeMcht, PayChannelInf qrChannelInf) {
        String tradeNo = UUIDGenerator.getOrderIdByUUId((int) 20);
        
        if (StringUtil.isEmpty(payRequest.getOrderTime())) {
        	throw new RequestLimitedException("交易时间有误");
        }
        if (payRequest.getValidityNum()>120 || payRequest.getValidityNum()<1) {
        	throw new RequestLimitedException("有效时间有误");
        }
        if (payRequest.getOrderAmount() < 10) {
            throw new RequestLimitedException("此通道订单金额不得少于1元");
        }
        if (payRequest.getOrderAmount() > 500000) {
            throw new RequestLimitedException("此通道订单金额不得高于5000元");
        }
        String orderAmount = toAmountString(payRequest.getOrderAmount() / 100.0);
        Map<String, String> params = new LinkedHashMap<>();
        params.put("orderNo", tradeNo);//订单编号
        params.put("orderAmount", orderAmount);//订单金额
        params.put("callbackUrl", SysParamUtil.getParam("YIZHIFU_NOTIFY_URL"));//通知地址
        
        System.out.println("呃呃呃："+serviceNameMap.get(payRequest.getTradeSource()));
        
        params.put("payType", "1");//交易类型
        params.put("productDesc", payRequest.getGoodsName());//商品描述
        params.put("merCode", payRequest.getMerchantId());//商户编号
        params.put("dateTime", payRequest.getOrderTime());//交易时间
        params.put("validityNum",Integer.toString(payRequest.getValidityNum()));//有效时间
        
        System.out.println("看值："+qrChannelInf.getSecret_key());
        
        params.put("sign", MD5Util.md5SignNokey(params, qrChannelInf.getSecret_key(), "UTF-8").toUpperCase());//签名

        String redirectUrl = RedirectController.makeJump(params, "post", Environment.YIZHIFU_FRONTEND_URL.getBaseUrl(), Environment.DEFAULT_JUMP_URL.getBaseUrl());
        return ThirdPartyPayDetail.ThirdPartyPayDetailBuilder.getBuilder()
                .isSuccess(true)
                .withLocalTradeNumber(tradeNo)
                .withPayRequest(payRequest)
                .withPayChannelInf(qrChannelInf)
                .withTradeSource(payRequest.getTradeSource())
                .withTradeState(TradeStateEnum.NOTPAY)
                .withNotifyUrl(SysParamUtil.getParam("YIZHIFU_NOTIFY_URL"))
                .withCodeUrl(redirectUrl)
                .build();
    }

	@Override
	public ThirdPartyPayDetail doOrderQuery(ThirdPartyPayDetail thirdPartyPayDetail, PayChannelInf qrChannelInf) {
		// TODO Auto-generated method stub
		return thirdPartyPayDetail;
	}

	
    private TradeStateEnum getTradeStateByResultId(String orderStatus) {
        switch (orderStatus) {
            case "000000":
                return TradeStateEnum.SUCCESS;//交易成功
            default:
                return TradeStateEnum.PAYERROR;//支付失败
        }
    }
	
	
    private void processResultMap(ThirdPartyPayDetail payDetail, Map returnMap) {
        if (returnMap != null) {
            String resultCode = StringUtil.trans2Str(returnMap.get("code"));
            String resultMsg = StringUtil.trans2Str(returnMap.get("message"));
            String orderNo = StringUtil.trans2Str(returnMap.get("orderId"));
            if (!payDetail.getOut_trade_no().equals(orderNo)) {
                log.error("paydetail not match, will not update.");
                return;
            }
            String orderStatus = returnMap.get("code").toString();
            TradeStateEnum tradeStateEnum = getTradeStateByResultId(orderStatus);//查询返回码的信息
            if (orderStatus != null) {
                payDetail.setResp_code(resultCode);
                payDetail.setErr_msg(resultMsg);
                payDetail.setTrade_state(tradeStateEnum.getCode());
            } else {
                log.error("parameter is illegal, skip");
            }
        }
    }
	
	
	@Override
	public ThirdPartyPayDetail acceptThirdPartyPayNotify(Map<String, String> resultMap) {
        ThirdPartyPayDetail payDetail = this.thirdPartyPayDetailDao.getById(resultMap.get("orderId"));
        try {
            PayChannelInf qrChannelInf = this.merchantInfDaoImpl.getChannelInf(payDetail.getChannel_id(), payDetail.getMch_id());
            String json = resultMap.get("rawJson");
            String sign = resultMap.get("sign");
            String b64 = Base64.encode(json.getBytes());
            String b64md5 = MD5Encrypt.getMessageDigest(b64).toLowerCase();
            String kb64md5 = qrChannelInf.getSecret_key() + b64md5;
            String verifySign = MD5Encrypt.getMessageDigest(kb64md5).toUpperCase();
            if (!sign.equals(verifySign)) {
                log.error("sign verify error, " + json + ", " + verifySign + ", " + sign);
                return payDetail;
            }
            if (!TradeStateEnum.SUCCESS.getCode().equals(payDetail.getTrade_state())) {
                processResultMap(payDetail, resultMap);//这里进入processResultMap方法
            }
            return payDetail;
        } catch (Exception e) {
            log.error("when processing notify", e);
            e.printStackTrace();
            return payDetail;
        }
	}

	@Override
	public Set<TradeSource> supportedThirdPartyTradeSource() {
        return SetUtil.toSet(
                TradeSource.ALI_WAP_PAY, TradeSource.ALIPAY,TradeSource.ALI_PC_PAY
        );
	}

}
