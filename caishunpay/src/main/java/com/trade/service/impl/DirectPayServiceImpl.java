package com.trade.service.impl;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;

import com.gy.system.Environment;
import com.gy.system.SysParamUtil;
import com.gy.util.HttpUtility;
import com.trade.bean.BankCardPay;
import com.trade.bean.own.DirectPayRequest;
import com.trade.bean.own.MerchantInf;
import com.trade.bean.response.DirectPayResponse;
import com.trade.dao.BankCardPayDao;
import com.trade.enums.ResponseEnum;
import com.trade.service.DirectPayService;
import com.trade.util.DateUtil;
import com.trade.util.MD5Util;

public class DirectPayServiceImpl implements DirectPayService {
	
	@Autowired
    private BankCardPayDao bankCardPayDao;

	@Override
	public DirectPayResponse doOrderCreate(DirectPayRequest reqParam, MerchantInf qrcodeMcht) {
		DirectPayResponse response=new DirectPayResponse();
		BankCardPay bankCardPay = bankCardPayDao.getByOrderNo(reqParam.getOrderNo(), reqParam.getMerCode());
		if(bankCardPay != null) {
			response.setResultCode(ResponseEnum.FAIL_ORDER_NO_REPEAT.getCode());
            response.setMessage(ResponseEnum.FAIL_ORDER_NO_REPEAT.getMemo());
            return response;
		}
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("merCode", reqParam.getMerCode());
		params.put("orderNo", reqParam.getOrderNo());
		params.put("orderAmount", String.valueOf(reqParam.getOrderAmount()));
		params.put("returnAddress", reqParam.getReturnAddress());
		params.put("backAddress", SysParamUtil.getParam((String) "HAIBEI_NETPAY_NOTIFY_URL"));
		params.put("dateTime", DateUtil.dateToString(new Date(), DateUtil.DATE_YYYYMMDDHHmmss));
		params.put("payType", reqParam.getPayType());
		params.put("bankCardType", reqParam.getBankCardType());
		params.put("bankCode", reqParam.getBankCode());
		params.put("sign", MD5Util.getMd5SignNoKeyByMap(params, (String) qrcodeMcht.getSecretKey(), (String[]) new String[]{"utf-8"}));
		String keyValue = MD5Util.map2HttpParam(params);
		String jsonResult = HttpUtility.postData((Environment) Environment.SHENBIANHUI_PAY_URL, (String) keyValue);
		return null;
	}


}
