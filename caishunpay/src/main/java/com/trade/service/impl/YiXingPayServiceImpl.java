package com.trade.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.google.gson.Gson;
import com.gy.system.Environment;
import com.gy.system.SysParamUtil;
import com.gy.util.DateUtil;
import com.gy.util.Dom4jUtil;
import com.gy.util.HttpUtility;
import com.gy.util.HttpUtils;
import com.gy.util.MD5;
import com.gy.util.MD5Encrypt;
import com.gy.util.SetUtil;
import com.gy.util.StringUtil;
import com.gy.util.UUIDGenerator;
import com.trade.annotations.PayChannelImplement;
import com.trade.bean.ThirdPartyPayDetail;
import com.trade.bean.own.MerchantInf;
import com.trade.bean.own.PayChannelInf;
import com.trade.bean.own.PayRequest;
import com.trade.dao.MerchantInfDao;
import com.trade.dao.ThirdPartyPayDetailDao;
import com.trade.enums.TradeSource;
import com.trade.enums.TradeStateEnum;
import com.trade.service.ThirdPartyPayService;
import com.trade.util.Base64;
import com.trade.util.JsonUtil;
import com.trade.util.MD5Util;
import com.trade.util.yinshengpay.ApipaySubmit;

@PayChannelImplement(channelId = "yixing")
public class YiXingPayServiceImpl implements ThirdPartyPayService {
    @Autowired
    private ThirdPartyPayDetailDao thirdPartyPayDetailDao;
    @Autowired
    private MerchantInfDao merchantInfDaoImpl;
    
    private static final String YIXINGPAY_NETPAY_URL = "http://rpi.snsshop.net/unifiedorder";

    private static Logger log = Logger.getLogger(YiXingPayServiceImpl.class);


    private String toAmountString(Integer integer) {
        return String.format("%.2f", integer);
    }

    @Override
    public ThirdPartyPayDetail doOrderCreate(PayRequest payRequest, MerchantInf qrcodeMcht, PayChannelInf qrChannelInf) {
            return doNetPayOrderCreate(payRequest, qrcodeMcht, qrChannelInf);
    }

    private ThirdPartyPayDetail doNetPayOrderCreate(PayRequest payRequest, MerchantInf qrcodeMcht, PayChannelInf qrChannelInf) {
        
    	System.out.println("商户名："+qrcodeMcht.getMchtName());
    	
    	String tradeNo = UUIDGenerator.getOrderIdByUUId((int) 20);
		Map<String, Object> map = new TreeMap<>();
		Map<String, Object> map2 = new TreeMap<>();
		map.put("mch_id", qrChannelInf.getChannel_mcht_no());
		map.put("out_order_no", String.valueOf(System.currentTimeMillis()));
		map.put("pay_platform", "ALIPAY");
		map.put("payment_fee", payRequest.getOrderAmount()*100);
		map.put("pay_type", "MWEB");
		map.put("cur_type", "CNY");
		map.put("body", payRequest.getGoodsName());
		map.put("notify_url", SysParamUtil.getParam("YIXINGPAY_NOTIFY_URL"));
		map.put("bill_create_ip", payRequest.getClientIp());
		
		JSONObject dataobj = new JSONObject(map);
		
		String biz_content = dataobj.toString();
		System.out.println("biz_content------------->" + biz_content);
		map2.put("biz_content", biz_content);
		map2.put("signature", buildSign(map,qrChannelInf));
		map2.put("sign_type", "MD5");
    	
		System.out.println("请求数据："+map2);
		String jsonResult = HttpUtils.httpSend(YIXINGPAY_NETPAY_URL, map2);
		Map maps = (Map)JSON.parse(jsonResult);
		
		System.out.println("返回MAP:"+maps);
		
		String s=maps.get("biz_content").toString();
		Map<String, String> params = JSONObject.parseObject(s, new TypeReference<Map<String, String>>(){});
        ThirdPartyPayDetail detail = ThirdPartyPayDetail.ThirdPartyPayDetailBuilder.getBuilder()
                .isSuccess(true)
                .withLocalTradeNumber(params.get("out_order_no"))
                .withPayRequest(payRequest)
                .withPayChannelInf(qrChannelInf)
                .withTradeSource(payRequest.getTradeSource())
                .withTradeState(TradeStateEnum.NOTPAY)
                .withNotifyUrl(SysParamUtil.getParam("YIXINGPAY_NOTIFY_URL"))
                .withCodeUrl((String)params.get("mweb_url"))
                .build();
        return detail;
    }
 
    public ThirdPartyPayDetail doOrderQuery(ThirdPartyPayDetail thirdPartyPayDetail, PayChannelInf qrChannelInf) {
    	Environment env = Environment.YIXING_QUERY_URL;
    	YiXingPayServiceImpl yixing = new YiXingPayServiceImpl();
		String keyValue = HttpUtils.httpSend(env.YIXING_QUERY_URL.getBaseUrl(),
				yixing.buildOrderquery(thirdPartyPayDetail.getOut_transaction_id(),qrChannelInf));
            Map<String, String> returnMap = Dom4jUtil.parseXml2Map(keyValue);
            String backSign = returnMap.get("signature").toUpperCase();
            String biz_content = returnMap.get("biz_content").toUpperCase();
            
    	    Gson gson = new Gson();
    	    Map<String, String> map = new HashMap<String, String>();
    	    map = gson.fromJson(biz_content, map.getClass());
            
            
            String checkSign = MD5Util.generateMd5(
                    "UTF-8",
                    qrChannelInf.getSecret_key(),
                    map.get("body"),
                    map.get("cashier_id"),
                    map.get("create_time"),
                    map.get("cur_type"),
                    map.get("mch_id"),
                    map.get("mch_name"),
                    map.get("order_no"),
                    map.get("order_status"),
                    map.get("out_order_no"),
                    map.get("pay_platform"),
                    map.get("pay_type"),
                    map.get("payment_fee"),
                    map.get("refund_fee"),
                    map.get("remark"),
                    map.get("shop_id"),
                    map.get("transaction_id")
            );
            if (checkSign != null && checkSign.toUpperCase().equals(backSign.toUpperCase())) {
                savepayScanStatus(returnMap, qrChannelInf, thirdPartyPayDetail);
            } else {
                log.warn("checkSign != backSign, ignore !");
            }
        return thirdPartyPayDetail;
    }

    
	/**
	 * 查询生成签名字段
	 * 
	 * @param map
	 * @param qrChannelInf 
	 * @return
	 */
	private String buildSignQuery(TreeMap<String, Object> map, PayChannelInf qrChannelInf) {
		JSONObject dataobj = new JSONObject(map);
		String strPre = "biz_content=" + dataobj.toString() + "&key=" + qrChannelInf.getSecret_key();
		System.out.println("md5前字段---------->：" + strPre);
		String str = MD5.MD5Encode(strPre).toUpperCase();
		System.out.println("md5后字段---------->：" + str);
		return str;
	}
    
    
	/**
	 * 下单生成签名字段
	 * 
	 * @param map
	 * @param qrcodeMcht 
	 * @return
	 */
	private String buildSign(Map<String, Object> map, PayChannelInf qrChannelInf) {
        JSONObject dataobj = new JSONObject(map);
		String strPre = "biz_content=" + dataobj.toString() + "&key=" + qrChannelInf.getSecret_key();
		System.out.println("md5前字段---------->：" + strPre);
		String str = MD5.MD5Encode(strPre).toUpperCase();
		System.out.println("md5后字段---------->：" + str);
		return str;
	}
    
	/**
	 * 构造查询参数
	 * @param qrChannelInf 
	 * 
	 * @param out_order_no 
	 * @return
	 */
	private TreeMap<String, Object> buildOrderquery(String out_transaction_id, PayChannelInf qrChannelInf) {
		TreeMap<String, Object> map = new TreeMap<>();
		TreeMap<String, Object> map2 = new TreeMap<>();
		map.put("mch_id", qrChannelInf.getChannel_mcht_no());
		map.put("order_no", out_transaction_id);
		JSONObject dataobj = new JSONObject(map);
		String biz_content = dataobj.toString();
		System.out.println("biz_content------------->" + biz_content);
		map2.put("biz_content", biz_content);
		map2.put("signature", buildSignQuery(map,qrChannelInf));
		map2.put("sign_type", "MD5");
		System.out.println("post 请求的 map参数：" + map2.toString());
		return map2;
	}
    
    
    private void savepayScanStatus(Map<String, String> returnMap, PayChannelInf qrChannelInf, ThirdPartyPayDetail thirdPartyPayDetail) {
        if (returnMap != null) {
            String ret_code = StringUtil.trans2Str(returnMap.get("ret_code"));
            String orderStatus = returnMap.get("status");
            if (!ret_code.equals("0")) {
                String errInfo = StringUtil.trans2Str(returnMap.get("ret_msg"));
                thirdPartyPayDetail.setTrade_state(TradeStateEnum.PAYERROR.getCode());
                thirdPartyPayDetail.setErr_msg(errInfo);
                this.thirdPartyPayDetailDao.update(thirdPartyPayDetail);
            } else {
                thirdPartyPayDetail.setTrade_state(TradeStateEnum.SUCCESS.getCode());
                thirdPartyPayDetail.setTime_end(DateUtil.getCurrTime());
                this.thirdPartyPayDetailDao.update(thirdPartyPayDetail);
            }
        }
    }
    
    @Override
    public Set<TradeSource> supportedThirdPartyTradeSource() {
        return SetUtil.toSet(TradeSource.ALI_WAP_PAY);
    }

    public ThirdPartyPayDetail acceptThirdPartyPayNotify(Map<String, String> resultMap) {

            String json = JSON.toJSONString(resultMap.get("biz_content"));
            System.out.println(json);
            Map<String, Object> map = JSONObject.parseObject(json, new TypeReference<Map<String, Object>>(){});
            System.out.println(map.get("out_order_no"));
            ThirdPartyPayDetail payDetail = this.thirdPartyPayDetailDao.getById((String) map.get("out_order_no"));

            PayChannelInf qrChannelInf = this.merchantInfDaoImpl.getChannelInf(payDetail.getChannel_id(), payDetail.getMch_id());
            try {
            String sign = resultMap.get("signature");
            String verifySign = buildSign(map,qrChannelInf);
            if (!sign.equals(verifySign)) {
                log.error("sign verify error, " + json + ", " + verifySign + ", " + sign);
                return payDetail;
            }
            if (!TradeStateEnum.SUCCESS.getCode().equals(payDetail.getTrade_state())) {
                processResultMap(payDetail, resultMap,map);
            }
            return payDetail;
        } catch (Exception e) {
            log.error("when processing notify", e);
            e.printStackTrace();
            return payDetail;
        }
    }
    
    private void processResultMap(ThirdPartyPayDetail payDetail, Map returnMap, Map<String, Object> map) {
        if (returnMap != null) {
            String trade_status = StringUtil.trans2Str(returnMap.get("ret_code"));
            String orderNo = StringUtil.trans2Str(map.get("out_order_no"));
            
            System.out.println("给的商户订单号:"+orderNo);
            System.out.println("原有的商户订单号:"+payDetail.getOut_trade_no());
            
            if (!payDetail.getOut_trade_no().equals(orderNo)) {
                log.error("paydetail not match, will not update.");
                return;
            }
            if("0".equals(trade_status)) {
            	payDetail.setResp_code("000000");
                payDetail.setErr_msg(trade_status);
                payDetail.setTrade_state("SUCCESS");
            }else {
            	payDetail.setResp_code("111111");
                payDetail.setErr_msg(trade_status);
                payDetail.setTrade_state("Fail");
            }
            payDetail.setOut_transaction_id((String)map.get("order_no"));
            
        }
    }
}
