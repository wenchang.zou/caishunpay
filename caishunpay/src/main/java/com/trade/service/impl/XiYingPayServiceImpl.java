package com.trade.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gy.system.Environment;
import com.gy.system.SysParamUtil;
import com.gy.util.SetUtil;
import com.gy.util.StringUtil;
import com.gy.util.UUIDGenerator;
import com.gy.util.XiYingUtil;
import com.trade.annotations.PayChannelImplement;
import com.trade.bean.ThirdPartyPayDetail;
import com.trade.bean.own.MerchantInf;
import com.trade.bean.own.PayChannelInf;
import com.trade.bean.own.PayRequest;
import com.trade.dao.ThirdPartyPayDetailDao;
import com.trade.dao.impl.MerchantInfDaoImpl;
import com.trade.enums.TradeSource;
import com.trade.enums.TradeStateEnum;
import com.trade.service.ThirdPartyPayService;
import com.trade.util.JsonUtil;

@PayChannelImplement(channelId = "xiying")
public class XiYingPayServiceImpl implements ThirdPartyPayService {
	@Autowired
	private ThirdPartyPayDetailDao thirdPartyPayDetailDao;

	@Autowired
	private MerchantInfDaoImpl merchantInfDaoImpl;

	private static Logger log = Logger.getLogger(XiYingPayServiceImpl.class);

	private String toAmountString(Double amount) {
		return String.format("%.2f", amount);
	}

	@Override
	public ThirdPartyPayDetail doOrderCreate(PayRequest payRequest, MerchantInf qrcodeMcht,
			PayChannelInf qrChannelInf) {
		return doNetPayOrderCreate(payRequest, qrcodeMcht, qrChannelInf);
	}

	private ThirdPartyPayDetail doNetPayOrderCreate(PayRequest payRequest, MerchantInf qrcodeMcht,
			PayChannelInf qrChannelInf) {
		String tradeNo = UUIDGenerator.getOrderIdByUUId((int) 20);
		String orderAmount = toAmountString(payRequest.getOrderAmount() / 100.0);
		String version = "1.0";
		String payType = "30";
		// 提交单据时间
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String orderTime = dateFormat.format(now);
		// 签名串
		String sign = version + "|" + qrcodeMcht.getChannelMchtNo() + "|" + tradeNo + "|" + payType + "|" + orderAmount
				+ "|" + orderTime + "|" + payRequest.getClientIp() + "|" + SysParamUtil.getParam("XIYING_NOTIFY_URL")
				+ "|" + qrChannelInf.getSecret_key();
		// md5加密
		String result = "";
		try {
			result = XiYingUtil.encryption(sign);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String param = "version=" + version + "&&agentId=" + qrcodeMcht.getChannelMchtNo() + "&&agentOrderId=" + tradeNo
				+ "&&payType=" + payType + "&&payAmt=" + orderAmount + "&&orderTime=" + orderTime + "&&payIp="
				+ payRequest.getClientIp() + "&&notifyUrl=" + SysParamUtil.getParam("XIYING_NOTIFY_URL") + "&&sign=" + result; // 把字符串转换为URL请求地址
		// 发送 POST 请求
		String retCode = XiYingUtil.sendPost(Environment.XIYING_PAY_URL.getBaseUrl(), param);
		String resultString = XiYingUtil.ResultParse(retCode);
		ThirdPartyPayDetail detail = ThirdPartyPayDetail.ThirdPartyPayDetailBuilder.getBuilder().isSuccess(true)
				.withLocalTradeNumber(tradeNo).withPayRequest(payRequest).withPayChannelInf(qrChannelInf)
				.withTradeSource(payRequest.getTradeSource()).withTradeState(TradeStateEnum.NOTPAY)
				.withNotifyUrl(SysParamUtil.getParam("XIYING_NOTIFY_URL")).withCodeUrl(resultString).build();
		return detail;
	}

	public ThirdPartyPayDetail doOrderQuery(ThirdPartyPayDetail thirdPartyPayDetail, PayChannelInf qrChannelInf) {
		if (thirdPartyPayDetail != null
				&& StringUtil.isNotEmpty((String[]) new String[] { thirdPartyPayDetail.getCode_url() })
				&& !TradeStateEnum.SUCCESS.getCode().equals(thirdPartyPayDetail.getTrade_state())) {
			// 签名串
			String sign = thirdPartyPayDetail.getVersion() + "|" + qrChannelInf.getChannel_mcht_no() + "|"
					+ thirdPartyPayDetail.getOut_trade_no() + "|" + qrChannelInf.getSecret_key();
			// md5加密
			String result = "";
			try {
				result = XiYingUtil.encryption(sign);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String param = "version=" + thirdPartyPayDetail.getVersion() + "&agentId="
					+ qrChannelInf.getChannel_mcht_no() + "&agentOrderId=" + thirdPartyPayDetail.getOut_trade_no()
					+ "&sign=" + result; // 把字符串转换为URL请求地址参数
			// 发送 POST 请求
			String jsonResult = XiYingUtil.sendPost(Environment.XIYING_PAY_QUERYURL.getBaseUrl(), param);
			Map returnMap = (Map) JsonUtil.parseJson((String) jsonResult);
			if (returnMap != null) {
				String version = StringUtil.trans2Str(returnMap.get("version"));
				String agentId = StringUtil.trans2Str(returnMap.get("agentId"));
				String agentOrderId = StringUtil.trans2Str(returnMap.get("agentOrderId"));
				String jnetOrderId = StringUtil.trans2Str(returnMap.get("jnetOrderId"));
				String payResult = StringUtil.trans2Str(returnMap.get("payResult"));
				String payMessage = StringUtil.trans2Str(returnMap.get("payMessage"));
				String returnSign = StringUtil.trans2Str(returnMap.get("sign"));
				String payAmt = StringUtil.trans2Str(returnMap.get("payAmt"));
				if ("SUCCESS".equals(payResult)) {
					String toSign = version + "|" + agentId + "|" + agentOrderId + "|" + jnetOrderId + "|" + payAmt
							+ "|" + payResult + qrChannelInf.getSecret_key();
					// md5加密
					String resultSign = "";
					try {
						resultSign = XiYingUtil.encryption(toSign);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (!returnSign.equals(resultSign)) {
						return thirdPartyPayDetail;
					}
					thirdPartyPayDetail.setTrade_state(payResult);
					thirdPartyPayDetail.setErr_msg(payMessage);
					thirdPartyPayDetail.setPay_result(payResult);
					thirdPartyPayDetail.setTrade_type("支付宝H5");
					thirdPartyPayDetail.setOut_transaction_id(jnetOrderId);
					thirdPartyPayDetail.setTime_end(com.gy.util.DateUtil.getCurrentTime());
				}
			}
		}
		return thirdPartyPayDetail;
	}

	@Override
	public Set<TradeSource> supportedThirdPartyTradeSource() {
		return SetUtil.toSet(TradeSource.ALI_WAP_PAY);
	}

	/**
	 * 当交易状态不是成功时,会进入此方法回调
	 */
	private void processResultMap(ThirdPartyPayDetail payDetail, Map returnMap) {
		if (returnMap != null) {
			String version = (String) returnMap.get("version");
			// String agentId=(String) returnMap.get("agentId");
			String agentOrderId = (String) returnMap.get("agentOrderId");
			String jnetOrderId = (String) returnMap.get("jnetOrderId");
			// String payAmt=(String) returnMap.get("payAmt");
			String payResult = (String) returnMap.get("payResult");
			String payMessage = (String) returnMap.get("payMessage");
			// String sign=(String) returnMap.get("sign");
			if (!payDetail.getOut_trade_no().equals(agentOrderId)) {
				log.error("paydetail not match, will not update.");
				return;
			}

			if (payResult != null) {
				payDetail.setVersion(version);
				payDetail.setOut_transaction_id(jnetOrderId);
				payDetail.setResp_code(payResult);
				payDetail.setErr_msg(payMessage);
				payDetail.setTrade_state(payResult);
			} else {
				log.error("parameter is illegal, skip");
			}
		}
	}

	@Override
	public ThirdPartyPayDetail acceptThirdPartyPayNotify(Map<String, String> returnMap) {

		ThirdPartyPayDetail payDetail = this.thirdPartyPayDetailDao.getById(returnMap.get("agentOrderId"));
		try {
			PayChannelInf qrChannelInf = this.merchantInfDaoImpl.getChannelInf(payDetail.getChannel_id(),
					payDetail.getMch_id());
			HashMap<String, String> map = new HashMap<String, String>();
			String version = (String) returnMap.get("version");
			String agentId = (String) returnMap.get("agentId");
			String agentOrderId = (String) returnMap.get("agentOrderId");
			String jnetOrderId = (String) returnMap.get("jnetOrderId");
			String payAmt = (String) returnMap.get("payAmt");
			String payResult = (String) returnMap.get("payResult");
			// String payMessage=(String) returnMap.get("payMessage");
			String sign = (String) returnMap.get("sign");
			String checkString = version + "|" + agentId + "|" + agentOrderId + "|" + jnetOrderId + "|" + payAmt + "|"
					+ payResult + "|" + qrChannelInf.getSecret_key();
			// md5加密
			String checkSign = "";
			try {
				checkSign = XiYingUtil.encryption(checkString);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (!sign.equals(checkSign)) {
				return payDetail;
			}
			if (!TradeStateEnum.SUCCESS.getCode().equals(payDetail.getTrade_state())) {
				processResultMap(payDetail, returnMap);// 这里进入processResultMap方法
			}
			return payDetail;
		} catch (Exception e) {
			log.error("when processing notify", e);
			e.printStackTrace();
			return payDetail;
		}
	}
}
