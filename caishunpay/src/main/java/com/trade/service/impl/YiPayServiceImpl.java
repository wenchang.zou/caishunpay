package com.trade.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.common.Constants;
import com.gy.system.Environment;
import com.gy.system.SysParamUtil;
import com.gy.util.HttpUtility;
import com.gy.util.MD5Encrypt;
import com.gy.util.MapUtil;
import com.gy.util.SetUtil;
import com.gy.util.StringUtil;
import com.gy.util.UUIDGenerator;
import com.trade.annotations.PayChannelImplement;
import com.trade.bean.ThirdPartyPayDetail;
import com.trade.bean.own.MerchantInf;
import com.trade.bean.own.PayChannelInf;
import com.trade.bean.own.PayRequest;
import com.trade.dao.MerchantInfDao;
import com.trade.dao.ThirdPartyPayDetailDao;
import com.trade.enums.TradeSource;
import com.trade.enums.TradeStateEnum;
import com.trade.service.ThirdPartyPayService;
import com.trade.util.Base64;
import com.trade.util.DateUtil;
import com.trade.util.JsonUtil;
import com.trade.util.MD5Util;
/**
 * 溢支付对接接口
 * @author Administrator
 * @version 1.0.0
 * @since 2018.6.3
 *
 */
@PayChannelImplement(channelId = "yizhifu_h5")
public class YiPayServiceImpl implements ThirdPartyPayService {
    private static Logger log = Logger.getLogger(YiPayServiceImpl.class);
    @Autowired
    private ThirdPartyPayDetailDao thirdPartyPayDetailDao;
    @Autowired
    private MerchantInfDao merchantInfDaoImpl;
    private static Map<TradeSource, String> serviceNameMap = new HashMap<>();

    static {
        serviceNameMap.put(TradeSource.WEPAY, "11");
        serviceNameMap.put(TradeSource.ALI_WAP_PAY, "22");
        serviceNameMap.put(TradeSource.ALI_PC_PAY, "23");
        serviceNameMap.put(TradeSource.ALIPAY, "24");
        serviceNameMap.put(TradeSource.QQPAY, "33");
    }

    private String toAmountString(Double amount) {
        return String.format("%.2f", amount);
    }

    /**
     * 创建订单接口
     * @param payRequest
     * @param qrcodeMcht
     * @param qrChannelInf
     * @return ThirdPartyPayDetail
     */
    public ThirdPartyPayDetail doOrderCreate(PayRequest payRequest, MerchantInf qrcodeMcht, PayChannelInf qrChannelInf) {
    	String tradeNo = "yp"+UUIDGenerator.getOrderIdByUUId((int) 14);
        String payType = PayTypeEnum.wechat.toString();
        if (TradeSource.ALIPAY.equals(payRequest.getTradeSource()) || TradeSource.ALI_WAP_PAY.equals(payRequest.getTradeSource())) {
            payType = PayTypeEnum.ali.toString();
        } else if (TradeSource.WEPAY.equals(payRequest.getTradeSource()) || TradeSource.WE_WAP_PAY.equals(payRequest.getTradeSource()))  {
            payType = PayTypeEnum.wechat.toString();
        } 
        Map<String, String> params = new HashMap<String, String>();
        params.put("orderNo", tradeNo);
        params.put("orderAmount", String.valueOf(payRequest.getOrderAmount()));
        params.put("callbackUrl", payRequest.getCallback_url());
        params.put("payType", payType);
        params.put("productDesc", payRequest.getGoodsName());
        params.put("merCode", qrChannelInf.getChannel_mcht_no());
        params.put("dateTime", DateUtil.dateToString(new Date(), DateUtil.DATE_YYYYMMDDHHmmss));
        params.put("cancelUrl", "http://www.baidu.com");
        params.put("showUrl", SysParamUtil.getParam((String) "YIPAY_NOTIFY_URL"));
        params.put("sign", MD5Util.getSign(params, qrcodeMcht.getSecretKey()));
        String pString=JSON.toJSONString(params);
        String jsonResult = HttpUtility.urlPostforJson(Environment.YI_PAY_URL_H5.getBaseUrl(), pString);
        Map returnMap = (Map) JsonUtil.parseJson(jsonResult);
        String resultStatus = StringUtil.trans2Str(returnMap.get("resultStatus"));
        String resultMsg = StringUtil.trans2Str(returnMap.get("resultMsg"));
        String resultCode = StringUtil.trans2Str(returnMap.get("resultCode"));
        String codeUrl = StringUtil.trans2Str(returnMap.get("payUrl"));
        Boolean success = Constants.SUCCESS_DATA.equals(resultCode) && Constants.SUCCESS.equals(resultStatus);
        TradeStateEnum tradeStateEnum;
        if (success) {
            tradeStateEnum = TradeStateEnum.NOTPAY;
        } else {
            tradeStateEnum = TradeStateEnum.PAYERROR;
        }
        ThirdPartyPayDetail detail = ThirdPartyPayDetail.ThirdPartyPayDetailBuilder.getBuilder()
                .isSuccess(success)
                .withLocalTradeNumber(tradeNo)
                .withPayRequest(payRequest)
                .withPayChannelInf(qrChannelInf)
                .withTradeSource(payRequest.getTradeSource())
                .withTradeState(tradeStateEnum)
                .withNotifyUrl(SysParamUtil.getParam("YIPAY_NOTIFY_URL"))
                .withCodeUrl(codeUrl)
                .build();
        detail.setResp_code(resultCode);
        detail.setResult_code(resultStatus);
        detail.setErr_msg("fail");
        return detail;
    }

    public ThirdPartyPayDetail doOrderQuery(ThirdPartyPayDetail thirdPartyPayDetail, PayChannelInf qrChannelInf) {
    	if (thirdPartyPayDetail != null && StringUtil.isNotEmpty((String[]) new String[]{thirdPartyPayDetail.getCode_url()}) && !TradeStateEnum.SUCCESS.getCode().equals(thirdPartyPayDetail.getTrade_state())) {
            Environment env = Environment.YI_PAY_QUERY_H5;
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("merCode", qrChannelInf.getChannel_mcht_no());
            params.put("orderNo", thirdPartyPayDetail.getOut_trade_no());
            params.put("dateTime", DateUtil.dateToString(new Date(), DateUtil.DATE_YYYYMMDDHHmmss));
            params=(HashMap<String, String>) MapUtil.sort(params);
            params.put("sign", MD5Util.getSign(params, qrChannelInf.getSecret_key()));
            String keyValue = JSON.toJSONString(params);
            String jsonResult = HttpUtility.urlPostMethod(env.getBaseUrl(), keyValue);
            Map returnMap = (Map) JsonUtil.parseJson((String) jsonResult);
            if (returnMap != null) {
                String resultCode = StringUtil.trans2Str(returnMap.get("resultCode"));
                String resultMsg = StringUtil.trans2Str(returnMap.get("resultMsg"));
                String resultStatus = StringUtil.trans2Str(returnMap.get("resultStatus"));
                if (Constants.SUCCESS_DATA.equals(resultCode) && Constants.SUCCESS.equals(resultStatus)) {
                    String orderStatus = (String) returnMap.get("orderStatus");
                    String payType = (String) returnMap.get("payType");
                    String payDate = (String) returnMap.get("payDate");
                    String merCode = (String) returnMap.get("merCode");
                    String orderNo = (String) returnMap.get("orderNo");
                    String backSign = (String) returnMap.remove("sign");
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("merCode", merCode);
                    map.put("orderNo", orderNo);
                    map.put("dateTime", payDate);
                    map=(HashMap<String, String>) MapUtil.sort(map);
                    String checkSign = MD5Util.getSign(map, qrChannelInf.getSecret_key());
                    if (!checkSign.equals(backSign)) {
                        return thirdPartyPayDetail;
                    }
                    if ("YWC".equals(orderStatus)) {
                        thirdPartyPayDetail.setTrade_state(TradeStateEnum.SUCCESS.getCode());
                    } else if ("DZF".equals(orderStatus)) {
                        thirdPartyPayDetail.setTrade_state(TradeStateEnum.NOTPAY.getCode());
                    }
                    thirdPartyPayDetail.setErr_msg(resultMsg);
                    thirdPartyPayDetail.setPay_result(resultCode);
                    thirdPartyPayDetail.setTrade_type(payType);
                    thirdPartyPayDetail.setOut_transaction_id((String) returnMap.get("orderNo"));
                    thirdPartyPayDetail.setTime_end(com.gy.util.DateUtil.getCurrentTime());
                }
            }
        }
        return thirdPartyPayDetail;
    }

    private void processResultMap(ThirdPartyPayDetail payDetail, Map returnMap) {
        if (returnMap != null) {
            String resultCode = StringUtil.trans2Str(returnMap.get("resultCode"));
            String resultMsg = StringUtil.trans2Str(returnMap.get("resultMsg"));
            String orderNo = StringUtil.trans2Str(returnMap.get("orderNo"));
            if (!payDetail.getOut_trade_no().equals(orderNo)) {
                log.error("paydetail not match, will not update.");
                return;
            }
            String orderStatus = returnMap.get("resultStatus").toString();
            TradeStateEnum tradeStateEnum = getTradeStateByResultId(orderStatus);
            if (orderStatus != null) {
                payDetail.setResp_code(resultCode);
                payDetail.setErr_msg(resultMsg);
                payDetail.setTrade_state(tradeStateEnum.getCode());
            } else {
                log.error("parameter is illegal, skip");
            }
        }
    }

    @Override
    public Set<TradeSource> supportedThirdPartyTradeSource() {
        return SetUtil.toSet(
                TradeSource.ALI_WAP_PAY, TradeSource.ALIPAY,TradeSource.ALI_PC_PAY,TradeSource.WEPAY,TradeSource.WE_WAP_PAY
        );
    }

    public ThirdPartyPayDetail acceptThirdPartyPayNotify(Map<String, String> resultMap) {
    	String orderNo = resultMap.get("orderNo");
        ThirdPartyPayDetail payDetail = this.thirdPartyPayDetailDao.getById(orderNo);
        try {
            PayChannelInf qrChannelInf = this.merchantInfDaoImpl.getChannelInf(payDetail.getChannel_id(), payDetail.getMch_id());
            String merCode = resultMap.get("merCode");
            String orderAmount = String.valueOf(resultMap.get("orderAmount"));
            String payDate = resultMap.get("payDate");
            String payCompletionDate = resultMap.get("payCompletionDate");//支付完成时间
            String resultCode = resultMap.get("resultCode");
            String resultMsg = resultMap.get("resultMsg");
            String resultStatus = resultMap.get("resultStatus");
            String resultTime = resultMap.get("resultTime");
            String sign = resultMap.get("sign");
            Map<String,String> map=new HashMap<String,String>();
            map.put("merCode", merCode);
            map.put("orderNo", orderNo);
            map.put("orderAmount", orderAmount);
            map.put("dateTime", payDate);
            map.put("payCompletionDate", payCompletionDate);
            map.put("resultMsg", resultMsg);
            map.put("resultStatus", resultStatus);
            map.put("resultTime", resultTime);
            map.put("resultCode", resultCode);
            map=MapUtil.sort(map);
            String verifySign = MD5Util.getSign(map, qrChannelInf.getSecret_key());
            if (!sign.equals(verifySign)) {
                log.error("notifyYiPay sign verify error!!! ");
                return payDetail;
            }
            if (!TradeStateEnum.SUCCESS.getCode().equals(payDetail.getTrade_state())) {
                processResultMap(payDetail, resultMap);
            }
            return payDetail;
        } catch (Exception e) {
            log.error("when processing notify", e);
            e.printStackTrace();
            return payDetail;
        }
    }

    private TradeStateEnum getTradeStateByResultId(String orderStatus) {
        switch (orderStatus) {
            case "000000":
                return TradeStateEnum.SUCCESS;
            default:
                return TradeStateEnum.PAYERROR;
        }
    }
    
    private enum PayTypeEnum {
        wechat("32", "微信扫码支付"),
        ali("33", "支付宝扫码支付"),
        qqpay("8", "QQ支付"),
        netpay("11", "网银支付");

        private String value;
        private String desc;

        PayTypeEnum(String value, String desc) {
            this.value = value;
            this.desc = (String) desc;
        }

        public String toString() {
            return this.value;
        }
    }
}
