package com.trade.service;

import com.trade.bean.own.DirectPayRequest;
import com.trade.bean.own.MerchantInf;
import com.trade.bean.response.DirectPayResponse;

public interface DirectPayService {
	DirectPayResponse doOrderCreate(DirectPayRequest reqParam, MerchantInf qrcodeMcht);

}
