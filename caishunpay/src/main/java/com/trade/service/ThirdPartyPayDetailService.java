package com.trade.service;

import com.trade.bean.ThirdPartyPayDetail;

public interface ThirdPartyPayDetailService {
	
	public ThirdPartyPayDetail getByOuTTradeNO(String orderNO); 

}
