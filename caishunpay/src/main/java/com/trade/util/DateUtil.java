package com.trade.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	
	public static final String DATE_YYYYMMDDHHmmss="yyyy-MM-dd HH:mm:SS";
	
	public static final String DATE_YYYYMMDD="yyyy-MM-dd";
	
	public static String dateToString(Date date,String format) {
		String result="";
		SimpleDateFormat sdf=new SimpleDateFormat(format);
		result=sdf.format(date);
		result=result.replaceAll("-", "").replaceAll(":", "").replaceAll(" ", "");
		return result;
	}
	
	public static void main(String[] args) {
		System.out.println(DateUtil.dateToString(new Date(), DateUtil.DATE_YYYYMMDDHHmmss));
	}

}
