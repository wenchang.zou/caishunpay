/*
 * @(#)HttpServiceException.java        1.0 2015��9��11��
 *
 * Copyright (c) 2007-2014 Shanghai Handpay IT, Co., Ltd.
 * No. 80 Xinchang Rd, Huangpu District, Shanghai, China
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * Shanghai Handpay IT Co., Ltd. ("Confidential Information").  
 * You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you 
 * entered into with Handpay.
 */
package com.trade.util.hanyin.quickpay;

/**
 * @Description http�����쳣��
 * @version 1.0
 * @author yhe
 * @since 2015��9��11��
 * @history 
 * ʱ�� �汾 ���� �޸�����
 */
public class HttpServiceException extends BaseException {

	/** serialVersionUID */
	private static final long serialVersionUID = 3329595588421120758L;

	public HttpServiceException() {
	}

	public HttpServiceException(String errorCode, String errorMsg) {
		super(errorCode, errorMsg);
	}

	public HttpServiceException(String errorCode, Throwable caused) {
		super(errorCode, caused);
	}

	public HttpServiceException(String errorCode, String errorMsg, Throwable caused) {
		super(errorCode, errorMsg, caused);
	}
}
