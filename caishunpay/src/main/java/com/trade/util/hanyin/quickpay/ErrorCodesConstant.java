/*
 * @(#)ErrorCodesConstant.java        1.0 2015��8��20��
 *
 * Copyright (c) 2007-2014 Shanghai Handpay IT, Co., Ltd.
 * No. 80 Xinchang Rd, Huangpu District, Shanghai, China
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * Shanghai Handpay IT Co., Ltd. ("Confidential Information").  
 * You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you 
 * entered into with Handpay.
 */
package com.trade.util.hanyin.quickpay;

/**
 * @Description �쳣������볣���� BaseException��errorCode���ø�class�ĳ�������
 * @version 1.0
 * @author yhe
 * @since 2015��8��20��
 * @history ʱ�� �汾 ���� �޸�����
 */
public class ErrorCodesConstant {

	/** ϵͳ���� */
	public static final String SYS_ERROR = "9999";

	/** �û�Ȩ�޲������쳣 */
	public static final String E_AUTH = "AUTH";
	
	/** �ʽ𷽹���������쳣 */
	public static final String E_CAPITAL = "CAPITAL";

	/** ���м��˻�ϵͳ�������쳣 */
	public static final String E_SVS = "SVS";

	/** ��֧��ƽ̨�������쳣 */
	public static final String E_PAYMENT = "PAYMENT";

	/** �ɶ��˲������쳣 */
	public static final String E_RNCNL = "RNCNL";

	/** �ɽ���������쳣 */
	public static final String E_SETTLE = "SETTLE";

	/** �̻���Ϣ����������쳣 */
	public static final String E_MERC = "MERC";

	/** ������Ϣ����������쳣 */
	public static final String E_INSTI = "INSTI";

	/** ������Ϣ����������쳣 */
	public static final String E_PARAMS = "PARAMS";
	
	/** �����ݷ��ʲ�������쳣 */
	public static final String E_DB = "D";

	/** ��������쳣 */
	public static final String E_DB_SAVE = E_DB + "001";

	/** ���ݸ����쳣 */
	public static final String E_DB_UPDATE = E_DB + "002";

	/** ����ɾ���쳣 */
	public static final String E_DB_DEL = E_DB + "003";

	/** ���ݲ�ѯ�쳣 */
	public static final String E_DB_FIND = E_DB + "004";

	/** ��ѯ�м��˻���Ϣ�쳣 */
	public static final String E_SVS_ERROR = E_SVS + "001";

	/** �м��˻���Ϣ������ */
	public static final String E_SVS_03 = E_SVS + "003";

	/** ���������������쳣 */
	public static final String E_CONNECTOR = "CONNECTOR";

	/** ��֧�ֵĽӿ����� */
	public static final String E_CONNECTOR_NOT_SUPPORT_INTERFACE = E_CONNECTOR + "001";

	/** connector�ӿ�ȱ�ٱ�Ҫ���� */
	public static final String E_CONNECTOR_LACK_OF_PARAM = E_CONNECTOR + "002";

	/** ����������ʧ�� */
	public static final String E_CONNECTOR_CREATE_REQ_ERROR = E_CONNECTOR + "003";

	/** �̻�����ʧ�� */
	public static final String E_CONNECTOR_MERCHANT_CONNECT_FAIL = E_CONNECTOR + "004";

	/** ����̻�Ӧ����ʧ�� */
	public static final String E_CONNECTOR_GET_RESPONSE_FAIL = E_CONNECTOR + "005";

	/** �յ������Ӧ���� */
	public static final String E_CONNECTOR_RESPONSE_ERROR = E_CONNECTOR + "006";

	/** �̻��𸴲�������������������벻�� */
	public static final String E_CONNECTOR_RESPCODE_NOT_TRANSCODE = E_CONNECTOR + "007";

	/** ���ļ��ܴ��� */
	public static final String E_CONNECTOR_ENCODE = E_CONNECTOR + "008";

	/** ���Ľ��ܴ��� */
	public static final String E_CONNECTOR_DECODE = E_CONNECTOR + "009";

	/** ǩ�����ݴ��� */
	public static final String E_CONNECTOR_INVALID_SIGN = E_CONNECTOR + "010";
	
	/** ��֤ǩ������ */
	public static final String E_CONNECTOR_VELIDATE_INVALID_SIGN = E_CONNECTOR + "011";
	
	/** ѹ�������ļ����� */
	public static final String E_CONNECTOR_ENCODE_BATCH_DFFILE = E_CONNECTOR + "012";
	
	/** ���������ļ����� */
	public static final String E_CONNECTOR_ENCODE_PARSE_DFFILE = E_CONNECTOR + "013";
	
	/** HTTPͨѶ�쳣 */
	public static final String E_CONNECTOR_HTTP_ERROR = E_CONNECTOR + "020";
	
	/** HTTP�����쳣 */
	public static final String E_CONNECTOR_HTTP_REQUEST_ERROR = E_CONNECTOR + "021";
	
	/** HTTP��Ӧ�쳣 */
	public static final String E_CONNECTOR_HTTP_RESPONSE_ERROR = E_CONNECTOR + "022";
	
	/** ��Ч��֧������ */
	public static final String E_PAYMENT_INVALID_BACKEND = E_PAYMENT + "001";

	/** ��Ч�Ķ������� */
	public static final String E_RNCNL_INVALID_BACKEND = E_RNCNL + "001";
	
	/** �����쳣 */
	public static final String E_RNCNL_ERROR = E_RNCNL + "002";
	
	/** �����쳣 */
	public static final String E_SETTLE_ERROR = E_SETTLE + "001";
	
	/** û���ҵ������ļ� */
	public static final String E_RNCNL_NOT_FOUND = E_RNCNL + "001";
	
	/** ���ض����ļ�ʧ�� */
	public static final String E_RNCNL_DOWN_ERROR = E_RNCNL + "002";
	
	/** ��ѹ�����ļ�ʧ�� */
	public static final String E_RNCNL_UNGZ_ERROR = E_RNCNL + "003";
	
	/** http�쳣 */

	/** �û�Ȩ�޲������쳣 */
	public static final String E_AUTH_USER_EXIST = E_AUTH + "01";
	public static final String R_NE_AUTH_USER_NOT_EXIST = E_AUTH + "02";
	public static final String E_AUTH_USER_INS_MGR_EXIST = E_AUTH + "03";
	public static final String R_NE_AUTH_USER_INIT = E_AUTH + "04";
	public static final String E_AUTH_ROLE_EXIST = E_AUTH + "05";
	public static final String E_AUTH_ROLE_NO_PERMISSION = E_AUTH + "06";
	public static final String E_AUTH_USER_INS_MGR_EXIST_MSG = "��������Ա�Ѿ����ڣ�";
	public static final String E_AUTH_USER_CAP_MGR_EXIST_MSG = "�ó��ʷ��û��Ѿ����ڣ�";
	public static final String E_AUTH_USER_MER_MGR_EXIST_MSG = "�̻�����Ա�Ѿ����ڣ�";
	public static final String E_AUTH_USER_EXIST_MSG = "�û����Ѿ����ڣ�";
	public static final String E_AUTH_ROLE_EXIST_MSG = "��ɫ���Ѿ����ڣ�";
	public static final String E_AUTH_ROLE_NO_PERMISSION_MSG = "��Ȩ�����ò�����";
	public static final String E_AUTH_USER_NOT_EXIST_MSG = "�û������ڣ�";
	public static final String R_NE_AUTH_USER_INIT_MSG = "�û�״̬��ʼ�����ܶ��ᣡ";
	
	/** �ʽ𷽹����쳣 */
	public static final String E_CAPITAL_EXIST = E_CAPITAL + "01";
	public static final String E_CAPITAL_EXIST_MSG =  "�ʽ������Ѿ����ڣ�";
	public static final String E_CAPITAL_NOT_EXIST = E_CAPITAL + "02";
	public static final String E_CAPITAL_NOT_EXIST_MSG =  "�ʽ���Ϣ�����ڣ�";
}
