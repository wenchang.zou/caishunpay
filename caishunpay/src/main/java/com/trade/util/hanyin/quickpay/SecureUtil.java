package com.trade.util.hanyin.quickpay;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

/**
 * ��ȫ����
 * @author jhjiang
 *
 */
public class SecureUtil {
	
	private static final Logger LOG = Logger.getLogger(SecureUtil.class);

	public static final String KEY_ALGORITHM_DES = "DES";			// DES-��Կ�㷨
	
	public static final String KEY_ALGORITHM_3DES = "DESede";		// 3DES-��Կ�㷨
	
	public static final String KEY_ALGORITHM_RSA = "RSA";			// RSA-��Կ�㷨
	
	
	/**
	 * MD5����-����
	 * @param dataByteAry	����������-byte����
	 * @return
	 */
	public static byte[] enByMD5(byte[] dataByteAry){
		return DigestUtils.md5(dataByteAry);
	}
	
	/**
	 * DES����-����
	 * @param dataAry	�����������ֽ�����	
	 * @param keyAry	��Կ�ֽ�����-8���ֽ�
	 * @param algorithm �㷨
	 * @return
	 * @throws Exception
	 */
	public static byte[] enByDES(byte[] dataAry, byte[] keyAry, String algorithm) {
		try {
			DESKeySpec keySpec = new DESKeySpec(keyAry);									// 1.����keyʵ�����淶
			SecretKeyFactory factory = SecretKeyFactory.getInstance(KEY_ALGORITHM_DES);		// 2.ʵ������Կ���� - DES�㷨
			SecretKey key = factory.generateSecret(keySpec);								// 3.���ݹ淶����ԭ��Կ
			Cipher c = Cipher.getInstance(algorithm);										// 4.ʵ����
			c.init(Cipher.ENCRYPT_MODE, key);												// 5.��ʼ��������Ϊ����ģʽ
			return c.doFinal(dataAry);														// 6.ִ�в���
		} catch (InvalidKeySpecException e) {
			LOG.error("DES���ܷ����쳣����Ч����Կ�淶", e);
		} catch (InvalidKeyException e) {
			LOG.error("DES���ܷ����쳣����Ч����Կ", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("DES���ܷ����쳣��û��[" + algorithm + "]�㷨", e);
		} catch (NoSuchPaddingException e) {
			LOG.error("DES���ܷ����쳣����Ч����䷽��", e);
		} catch (IllegalBlockSizeException e) {
			LOG.error("DES���ܷ����쳣���Ƿ��Ŀ鳤��", e);
		} catch (BadPaddingException e) {
			LOG.error("DES���ܷ����쳣���������䷽��", e);
		} 
		return null;
	}
	
	
	/**
	 * DES����-����
	 * @param dataAry	�����������ֽ�����	
	 * @param keyAry	��Կ�ֽ�����
	 * @param algorithm �㷨
	 * @return
	 * @throws Exception
	 */
	public static byte[] deByDES(byte[] dataAry, byte[] keyAry, String algorithm) {
		try {
			DESKeySpec keySpec = new DESKeySpec(keyAry);									// 1.����keyʵ�����淶
			SecretKeyFactory factory = SecretKeyFactory.getInstance(KEY_ALGORITHM_DES);		// 2.ʵ������Կ���� - DES�㷨
			SecretKey key = factory.generateSecret(keySpec);								// 3.���ݹ淶����ԭ��Կ
			Cipher c = Cipher.getInstance(algorithm);										// 4.ʵ����
			c.init(Cipher.DECRYPT_MODE, key);												// 5.��ʼ��������Ϊ����ģʽ
			return c.doFinal(dataAry);														// 6.ִ�в���
		} catch (InvalidKeySpecException e) {
			LOG.error("DES���ܷ����쳣����Ч����Կ�淶", e);
		} catch (InvalidKeyException e) {
			LOG.error("DES���ܷ����쳣����Ч����Կ", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("DES���ܷ����쳣��û��[" + algorithm + "]�㷨", e);
		} catch (NoSuchPaddingException e) {
			LOG.error("DES���ܷ����쳣����Ч����䷽��", e);
		} catch (IllegalBlockSizeException e) {
			LOG.error("DES���ܷ����쳣���Ƿ��Ŀ鳤��", e);
		} catch (BadPaddingException e) {
			LOG.error("DES���ܷ����쳣���������䷽��", e);
		} 
		return null;
	}
	
	/**
	 * 3DES����-����
	 * @param dataAry	�����������ֽ�����	
	 * @param keyAry	��Կ�ֽ�����
	 * @param algorithm �㷨
	 * @return
	 * @throws Exception
	 */
	public static byte[] enBy3DES(byte[] dataAry, byte[] keyAry, String algorithm) {
		try {
			DESedeKeySpec keySpec = new DESedeKeySpec(keyAry);								// 1.����keyʵ�����淶
			SecretKeyFactory factory = SecretKeyFactory.getInstance(KEY_ALGORITHM_3DES);	// 2.ʵ������Կ���� - DESede�㷨
			SecretKey key = factory.generateSecret(keySpec);								// 3.���ݹ淶����ԭ��Կ
			Cipher c = Cipher.getInstance(algorithm);										// 4.ʵ����
			c.init(Cipher.ENCRYPT_MODE, key);												// 5.��ʼ��������Ϊ����ģʽ
			return c.doFinal(dataAry);														// 6.ִ�в���
		} catch (InvalidKeySpecException e) {
			LOG.error("3DES���ܷ����쳣����Ч����Կ�淶", e);
		} catch (InvalidKeyException e) {
			LOG.error("3DES���ܷ����쳣����Ч����Կ", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("3DES���ܷ����쳣��û��[" + algorithm + "]�㷨", e);
		} catch (NoSuchPaddingException e) {
			LOG.error("3DES���ܷ����쳣����Ч����䷽��", e);
		} catch (IllegalBlockSizeException e) {
			LOG.error("3DES���ܷ����쳣���Ƿ��Ŀ鳤��", e);
		} catch (BadPaddingException e) {
			LOG.error("3DES���ܷ����쳣���������䷽��", e);
		} 
		return null;
	}

	/**
	 * 3DES����-����
	 * @param dataAry	�����������ֽ�����	
	 * @param keyAry	��Կ�ֽ�����
	 * @param algorithm �㷨
	 * @return
	 * @throws Exception
	 */
	public static byte[] deBy3DES(byte[] dataAry, byte[] keyAry, String algorithm) {
		try {
			DESedeKeySpec keySpec = new DESedeKeySpec(keyAry);								// 1.����keyʵ�����淶
			SecretKeyFactory factory = SecretKeyFactory.getInstance(KEY_ALGORITHM_3DES);	// 2.ʵ������Կ���� - DESede�㷨
			SecretKey key = factory.generateSecret(keySpec);								// 3.���ݹ淶����ԭ��Կ
			Cipher c = Cipher.getInstance(algorithm);										// 4.ʵ����
			c.init(Cipher.DECRYPT_MODE, key);												// 5.��ʼ��������Ϊ����ģʽ
			return c.doFinal(dataAry);														// 6.ִ�в���
		} catch (InvalidKeySpecException e) {
			LOG.error("3DES���ܷ����쳣����Ч����Կ�淶", e);
		} catch (InvalidKeyException e) {
			LOG.error("3DES���ܷ����쳣����Ч����Կ", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("3DES���ܷ����쳣��û��[" + algorithm + "]�㷨", e);
		} catch (NoSuchPaddingException e) {
			LOG.error("3DES���ܷ����쳣����Ч����䷽��", e);
		} catch (IllegalBlockSizeException e) {
			LOG.error("3DES���ܷ����쳣���Ƿ��Ŀ鳤��", e);
		} catch (BadPaddingException e) {
			LOG.error("3DES���ܷ����쳣���������䷽��", e);
		} 
		return null;
	}
	
	/**
	 * RSA����-����
	 * @param dataAry	����������
	 * @param keyAry	��Կ����
	 * @param algorithm	�㷨
	 * @param keyType	��������-{false����Կ���ܣ�true: ˽Կ����}
	 * @return
	 */
	public static byte[] enByRSA(byte[] dataAry, byte[] keyAry, String algorithm, boolean keyType){
		try {
			EncodedKeySpec keySpec = keyType ? new PKCS8EncodedKeySpec(keyAry) : new X509EncodedKeySpec(keyAry);
			KeyFactory factory = KeyFactory.getInstance(KEY_ALGORITHM_RSA);
			Key key = keyType ? factory.generatePrivate(keySpec) : factory.generatePublic(keySpec);
			Cipher c = Cipher.getInstance(algorithm);
			c.init(Cipher.ENCRYPT_MODE, key);
			return c.doFinal(dataAry);
		} catch (InvalidKeySpecException e) {
			LOG.error("RSA���ܷ����쳣����Ч����Կ�淶", e);
		} catch (InvalidKeyException e) {
			LOG.error("RSA���ܷ����쳣����Ч����Կ", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("RSA���ܷ����쳣��û��[" + algorithm + "]�㷨", e);
		} catch (NoSuchPaddingException e) {
			LOG.error("RSA���ܷ����쳣����Ч����䷽��", e);
		} catch (IllegalBlockSizeException e) {
			LOG.error("RSA���ܷ����쳣���Ƿ��Ŀ鳤��", e);
		} catch (BadPaddingException e) {
			LOG.error("RSA���ܷ����쳣���������䷽��", e);
		} 
		return null;
	}
	
	/**
	 * RSA����-����
	 * @param dataAry	����������
	 * @param keyAry	��Կ����
	 * @param algorithm	�㷨
	 * @param keyType	��������-{false����Կ���ܣ�true: ˽Կ����}
	 * @return
	 */
	public static byte[] deByRSA(byte[] dataAry, byte[] keyAry, String algorithm, boolean keyType){
		try {
			EncodedKeySpec keySpec = keyType ? new PKCS8EncodedKeySpec(keyAry) : new X509EncodedKeySpec(keyAry);
			KeyFactory factory = KeyFactory.getInstance(KEY_ALGORITHM_RSA);
			Key key = keyType ? factory.generatePrivate(keySpec) : factory.generatePublic(keySpec);
			Cipher c = Cipher.getInstance(algorithm);
			c.init(Cipher.DECRYPT_MODE, key);
			return c.doFinal(dataAry);
		} catch (InvalidKeySpecException e) {
			LOG.error("RSA���ܷ����쳣����Ч����Կ�淶", e);
		} catch (InvalidKeyException e) {
			LOG.error("RSA���ܷ����쳣����Ч����Կ", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("RSA���ܷ����쳣��û��[" + algorithm + "]�㷨", e);
		} catch (NoSuchPaddingException e) {
			LOG.error("RSA���ܷ����쳣����Ч����䷽��", e);
		} catch (IllegalBlockSizeException e) {
			LOG.error("RSA���ܷ����쳣���Ƿ��Ŀ鳤��", e);
		} catch (BadPaddingException e) {
			LOG.error("RSA���ܷ����쳣���������䷽��", e);
		} 
		return null;
	}
	
	/**
	 * RSA��ǩ-����
	 * @param dataAry	����ǩ����-����
	 * @param priKeyAry	RSA��ǩ˽Կ-����
	 * @param algorithm	�㷨
	 * @return
	 */
	public static byte[] signByRSA(byte[] dataAry, byte[] priKeyAry, String algorithm) {
		try {
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(priKeyAry);		// 1.ת��˽Կ����
			KeyFactory factory = KeyFactory.getInstance(KEY_ALGORITHM_RSA);			// 2.ʵ������Կ����
			PrivateKey priKey = factory.generatePrivate(keySpec);					// 3.ȡ˽Կ����
			Signature st = Signature.getInstance(algorithm);						// 4.ʵ����Signature
			st.initSign(priKey);													// 5.��ʼ��Signature
			st.update(dataAry);														// 6.����
			return st.sign();														// 7.ǩ��
		} catch (InvalidKeySpecException e) {
			LOG.error("RSA��ǩ�����쳣����Ч����Կ�淶", e);
		} catch (InvalidKeyException e) {
			LOG.error("RSA��ǩ�����쳣����Ч����Կ", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("RSA��ǩ�����쳣��û��[" + algorithm + "]�㷨", e);
		} catch (SignatureException e) {
			LOG.error("RSA��ǩ�����쳣��ǩ���쳣", e);
		} 
		return null;
	}

	/**
	 * RSA��ǩ-����
	 * @param dataAry	����ǩ����-����
	 * @param pubKeyAry	��������Կ-����
	 * @param signAry	ǩ��-����
	 * @param algorithm	�㷨
	 * @return
	 */
	public static boolean verifyByRSA(byte[] dataAry, byte[] pubKeyAry, byte[] signAry, String algorithm) {
		try {
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(pubKeyAry);				// 1.ת����Կ
			KeyFactory factory = KeyFactory.getInstance(KEY_ALGORITHM_RSA);				// 2.ʵ������Կ����
			PublicKey pubKey = factory.generatePublic(keySpec);							// 3.���ɹ�Կ
			Signature st = Signature.getInstance(algorithm);							// 4.ʵ����Signature
			st.initVerify(pubKey);														// 5.��ʼ��Signature
			st.update(dataAry);															// 6.����
			return st.verify(signAry);													// 7.��ǩ
		} catch (InvalidKeySpecException e) {
			LOG.error("RSA��ǩ�����쳣����Ч����Կ�淶", e);
		} catch (InvalidKeyException e) {
			LOG.error("RSA��ǩ�����쳣����Ч����Կ", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("RSA��ǩ�����쳣��û��[" + algorithm + "]�㷨", e);
		} catch (SignatureException e) {
			LOG.error("RSA��ǩ�����쳣��ǩ���쳣", e);
		} 
		return false;
	}
	
	public static String getSignnatrue(Map<String, String> formparams,String signKey){
		StringBuilder sb=new StringBuilder();
		//String signKey="86950ECA654C1D6757FDCD4B89DDF775";
		sb.append(formparams.get("insCode")).append("|")
				.append(formparams.get("insMerchantCode")).append("|")
				.append(formparams.get("hpMerCode")).append("|")
				.append(formparams.get("orderNo")).append("|")
				.append(formparams.get("orderTime")).append("|")
				.append(formparams.get("orderAmount")).append("|")
				.append(formparams.get("name")).append("|")
				.append(formparams.get("idNumber")).append("|")
				.append(formparams.get("accNo")).append("|")
				.append(formparams.get("telNo")).append("|")
				.append(formparams.get("productType")).append("|")
				.append(formparams.get("paymentType")).append("|")
				.append(formparams.get("nonceStr")).append("|")
				.append(signKey);
		
		MD5 md5=new MD5();
//		System.out.println(md5.getMD5ofStr(sb.toString()));
		return md5.getMD5ofStr(sb.toString());
	}
	
	
	//hpMerCode|orderNo|transDate|transStatus|transAmount|actualAmount|transSeq|statusCode|statusMsg|signKey
	public static String ResultSignnatrue(Map<String, String> formparams,String signKey){
		StringBuilder sb=new StringBuilder();
		sb.append(formparams.get("hpMerCode")).append("|")
				.append(formparams.get("orderNo")).append("|")
				.append(formparams.get("transDate")).append("|")
				.append(formparams.get("transStatus")).append("|")
				.append(formparams.get("transAmount")).append("|")
				.append(formparams.get("actualAmount")).append("|")
				.append(formparams.get("transSeq")).append("|")
				.append(formparams.get("statusCode")).append("|")
				.append(formparams.get("statusMsg")).append("|")
				.append(signKey);
		
		MD5 md5=new MD5();
		return md5.getMD5ofStr(sb.toString());
	}
	
	//insCode|insMerchantCode|hpMerCode|orderNo|transDate|transSeq|productType|paymentType|nonceStr|signKey
	public static String querySignnatrue(Map<String, String> formparams,String signKey){
		StringBuilder sb=new StringBuilder();
		sb.append(formparams.get("insCode")).append("|")
				.append(formparams.get("insMerchantCode")).append("|")
				.append(formparams.get("hpMerCode")).append("|")
				.append(formparams.get("orderNo")).append("|")
				.append(formparams.get("transDate")).append("|")
				.append(formparams.get("transSeq")).append("|")
				.append(formparams.get("productType")).append("|")
				.append(formparams.get("paymentType")).append("|")
				.append(formparams.get("nonceStr")).append("|")
				.append(signKey);
		
		MD5 md5=new MD5();
		return md5.getMD5ofStr(sb.toString());
	}
}
