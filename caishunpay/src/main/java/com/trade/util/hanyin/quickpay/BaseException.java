/*
 * @(#)BaseException.java        1.0 2015��8��20��
 *
 * Copyright (c) 2007-2014 Shanghai Handpay IT, Co., Ltd.
 * No. 80 Xinchang Rd, Huangpu District, Shanghai, China
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * Shanghai Handpay IT Co., Ltd. ("Confidential Information").  
 * You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you 
 * entered into with Handpay.
 */
package com.trade.util.hanyin.quickpay;

import org.apache.commons.lang3.StringUtils;

/**
 * @Description �쳣���࣬����ϵͳ���쳣������̳�
 * @version 1.0
 * @author yhe
 * @since 2015��8��20��
 * @history ʱ�� �汾 ���� �޸�����
 */
public class BaseException extends Exception {

	/** serialVersionUID */
	private static final long serialVersionUID = 5368369642659157789L;
	
	public static final String SYMBOL_DELIMITER = "#";

	public BaseException() {}

	public BaseException(String errorCode) {
		super(errorCode);
	}

	public BaseException(String errorCode, String errorMsg) {
		super(errorCode + SYMBOL_DELIMITER + errorMsg);
	}

	public BaseException(String errorCode, Throwable caused) {
		super(errorCode,caused);
	}

	public BaseException(String errorCode, String errorMsg, Throwable caused) {
		super(errorCode + SYMBOL_DELIMITER + errorMsg, caused);
	}

	/**
	 * ����쳣�Ĵ������
	 * 
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return resultInfo(getMessage(), 0);
	}

	/**
	 * ����쳣�Ĵ�����Ϣ
	 * 
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return resultInfo(getMessage(), 1);
	}

	private String resultInfo(String errorMessage, Integer index) {
		String result = "";
		if (StringUtils.isNotBlank(getMessage())) {
			String[] ary = errorMessage.split(SYMBOL_DELIMITER);
			if (ary != null && ary.length > index) {
				result  = ary[index];
			} else {
				result = errorMessage;
			}
		}
		return result;		
	}
	
}
