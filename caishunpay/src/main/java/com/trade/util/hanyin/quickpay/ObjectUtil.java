/*
 * @(#)ObjectUtil.java        1.0 2009-8-11
 *
 * Copyright (c) 2007-2009 Shanghai Handpay IT, Co., Ltd.
 * 16/F, 889 YanAn Road. W., Shanghai, China
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * Shanghai Handpay IT Co., Ltd. ("Confidential Information").  
 * You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you 
 * entered into with Handpay.
 */

package com.trade.util.hanyin.quickpay;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.CRC32;

import org.apache.commons.beanutils.PropertyUtilsBean;





/**
 * ����Ĺ�����.
 * 
 * @version 1.0 2009-8-11
 * @author yzhu
 * @author sliu modify
 * @since 2013-8-13
 * @��Ȩ���� ����Ƽ�
 */
public class ObjectUtil {
	

	


	/**
	 * �ж��ַ����Ƿ�Ϊ��
	 * 
	 * @param str
	 *            �ַ���
	 * @return �Ƿ�Ϊ��
	 */
	public static boolean isNull(String str) {
		return (str == null || "".equals(str.trim()));
	}



	/**
	 * ��ȡ�ַ���
	 * 
	 * @param str
	 *            �ַ���
	 * @param length
	 *            ��ȡ����
	 * @return
	 */
	public static String subString(String str, int length) {
		if (!ObjectUtil.isObjNull(str) && str.length() > length) {
			str = str.substring(0, length);
		}

		return str;
	}






	/**
	 * �ж϶����Ƿ�Ϊ��
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isObjNull(Object obj) {
		if (obj == null) {
			return true;
		}
		if (obj instanceof java.lang.String) {
			String strObj = (String) obj;
			return isNull(strObj);
		}
		return false;
	}



	/**
	 * 
	 * @param crc
	 * @param bytes
	 * @return
	 */
	public static String getCRC(CRC32 crc, byte[] bytes) {
		if (null == crc) {
			return "";
		}
		crc.reset();
		crc.update(bytes);
		return String.valueOf(crc.getValue());
	}

	/**
	 * �ж��ַ����Ƿ������������ʽ��
	 * 
	 * @param str
	 * @param regEx
	 * @return
	 */
	public static boolean isContains(String str, String regEx) {
		Pattern pat = Pattern.compile(regEx);
		Matcher matcher = pat.matcher(str);
		boolean flg = false;
		if (matcher.find()) {
			flg = true;
		}
		return flg;
	}

	public static String convertToPercent(BigDecimal molecular,
			BigDecimal denominator) {
		if (denominator.compareTo(BigDecimal.ZERO) == 0) {
			return BigDecimal.ZERO.toString();
		}
		String result = "";
		BigDecimal percent = molecular.divide(denominator, 4,
				BigDecimal.ROUND_HALF_EVEN);
		// ��ȡ��ʽ������
		NumberFormat nt = NumberFormat.getPercentInstance();
		// ���ðٷ�����ȷ��2��������λС��
		nt.setMinimumFractionDigits(2);
		result = nt.format(percent);
		return result;
	}

	/**
	 * �ж��ַ����Ƿ���Double����
	 * 
	 * @param str
	 * @param regEx
	 * @return
	 */
	public static boolean isDoubleType(String s) {
		try {
			Double.parseDouble(s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static String splitIt(String splitStr, int start, int bytes) {
		int cutLength = 0;
		int byteNum = bytes;
		byte bt[] = splitStr.getBytes();
		if (bytes > 1) {
			for (int i = 0; i < byteNum; i++) {
				if (bt[i] < 0) {
					cutLength++;
				}
			}
			if (cutLength % 2 == 0) {
				cutLength /= 2;
			} else {
				cutLength = 0;
			}
		}
		int result = cutLength + --byteNum;
		if (result > bytes) {
			result = bytes;
		}
		if (bytes == 1) {
			if (bt[0] < 0) {
				result += 2;
			} else {
				result += 1;
			}
		}
		return new String(bt, start, result);
	}



	/**
	 * �ж�Ϊ�ջ�Ϊ0
	 * 
	 * @author lfjiang 2014��7��30��
	 * @param num
	 * @return
	 */
	public static boolean isZeroInt(Integer num) {
		return num == null || num == 0;
	}

	/**
	 * ��֤������λС��
	 * 
	 * @author zcheng 2014��8��15��
	 * @param ����֤���ַ���
	 * @return ����Ƿ��ϸ�ʽ���ַ���,���� <b>true </b>,����Ϊ <b>false </b>
	 */
	public static boolean IsDecimal(String str) {
		String regex = "^([1-9][\\d]{0,16}|0)(\\.[\\d]{1,2})?$";
		return match(regex, str);
	}

	/**
	 * @author zcheng 2014��8��15��
	 * @param regex
	 *            ������ʽ�ַ���
	 * @param str
	 *            Ҫƥ����ַ���
	 * @return ���str ���� regex��������ʽ��ʽ,����true, ���򷵻� false;
	 */
	public static boolean match(String regex, String str) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}

	public static String matcherString(String str) {
		Pattern p = Pattern.compile("\\s*|\t|\r|\n");
		Matcher m = p.matcher(str);
		return m.replaceAll("");
	}


	/**
	 * �ж��ַ����Ƿ��Ǵ��������
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		Pattern p = Pattern.compile("[0-9]*");
		return str == null ? false : p.matcher(str).matches();
	}
	
	/**
	 * ��[ԭʼ����]������ [ָ������]�� [ָ���ַ���]
	 * @param oldStr 	ԭʼ����
	 * @param count 	ָ������
	 * @param fillStr 	ָ���ַ���
	 * @return
	 */
	public static String fillLeft(String oldStr, int count, String fillStr) {
		if (count <= 0) {
			return oldStr;
		}
		StringBuffer sbf = new StringBuffer();
		for (int i = 0; i < count; i++) {
			sbf.append(fillStr);
		}
		sbf.append(oldStr);
		return sbf.toString();
	}
	
	/**
	 * ��[ԭʼ����]�Ҳ���� [ָ������]�� [ָ���ַ���]
	 * @param oldStr 	ԭʼ����
	 * @param count 	ָ������
	 * @param fillStr 	ָ���ַ���
	 * @return
	 */
	public static String fillRight(String oldStr, int count, String fillStr){
		if(count <= 0){
			return oldStr;
		}
		StringBuffer sbf = new StringBuffer(oldStr);
		for (int i = 0; i < count; i++) {
			sbf.append(fillStr);
		}
		return sbf.toString();
	}
	
	/**
	 * �ϲ�����
	 * @param btArys
	 * @return
	 */
	public static byte[] mergeByteAry(byte[]... btArys){
		if(btArys == null || btArys.length == 0){
			return new byte[0];
		}
		
		int nSize = 0;
		for(byte[] btAry : btArys){
			if(btAry == null){
				continue;
			}
			nSize += btAry.length;
		}
		
		if(nSize == 0){
			return new byte[0];
		}
		
		byte[] newBtAry = new byte[nSize];
		int size = 0, destPos = 0;
		
		for(byte[] b : btArys){
			if(b == null || b.length == 0){
				continue;
			}
			size = b.length;
			System.arraycopy(b, 0, newBtAry, destPos, size);
			destPos += size;
		}
		
		return newBtAry;
	}
	
	/**
	 * ��ȡ����
	 * @param b
	 * @param i
	 * @param to
	 * @return
	 */
	public static byte[] subAry(byte[] b, int i, int to) {
		return Arrays.copyOfRange(b, i, to);
	}

	/**
	 * �������Կ��� ������null������
	 * 
	 * @param dest  Ŀ�����
	 * @param src 	Դ����
	 */
	@SuppressWarnings("rawtypes")
	public static void copyProperties(Object dest, Object src) {
		PropertyUtilsBean util = new PropertyUtilsBean();
		PropertyDescriptor[] srcProps = util.getPropertyDescriptors(src
				.getClass());
		for (int i = 0; i < srcProps.length; i++) {
			Method srcMethod = srcProps[i].getReadMethod();
			Object srcValue = null;
			try {
				srcValue = srcMethod.invoke(src);
			} catch (Exception ex) {
				continue;
			}
			if (srcValue == null) {
				continue;
			}
			Class type = srcProps[i].getPropertyType();
			String merthodName = "set"
					+ srcProps[i].getName().substring(0, 1).toUpperCase()
					+ srcProps[i].getName().substring(1);
			Method destMethod = null;
			try {
				destMethod = dest.getClass().getMethod(merthodName, type);
			} catch (Exception ex) {
				continue;
			}
			if (destMethod == null) {
				continue;
			}
			try {
				destMethod.invoke(dest, srcValue);
			} catch (Exception ex) {
				continue;
			}
		}
	}
	
	/**
	 * ��ȡuuid
	 * @return
	 */
	public static String getUuid(){
		return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
	}
	

}
