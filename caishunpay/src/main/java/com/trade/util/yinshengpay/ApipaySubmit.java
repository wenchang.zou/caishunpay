package com.trade.util.yinshengpay;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.SystemUtils;

import com.gy.system.SysParamUtil;


/**
 * API主入口根据bean传值,取值,遍历签名,验签 最终生成返回
 * @author chang
 *
 */
public class ApipaySubmit {

	/**
	 * API验证签名工具，把签名值，请求字符编码，返回结果(json body)传递过来进行验证签名 公钥验证签名
	 * 用于验证银盛同步响应回来的参数
	 * @param request
	 * @param sign
	 * @param responseBody
	 * @param charset
	 * @return
	 * @see
	 */
	public static boolean verifyJsonSign(HttpServletRequest request, String sign, String responseBody, String charset) {
		ServletContext servletContext = request.getSession().getServletContext();
		InputStream publicCertFileInputStream = servletContext
				.getResourceAsStream(SysParamUtil.getParam("PATH_YSEPAY_PUBLIC_CERT"));
		
		
		boolean isSign = false;
		try {
			isSign = SignUtils.rsaCheckContent(publicCertFileInputStream, responseBody, sign, charset);
		} catch (Exception e) {
			throw new RuntimeException("验证签名失败，请检查银盛公钥证书文件是否存在");
		}
		return isSign;
	}

	
//	/**
//	 * 拼接请求网关参数
//	 * @param request
//	 * @param sParaTemp
//	 * @param strMethod
//	 * @param strButtonName
//	 * @return
//	 */
//	public static String buildRequest(HttpServletRequest request, Object obj) {
//		Map<String, String> sPara = buildRequestPara(request, getProperty(obj));
//		System.out.println("--打印所有参数--"+sPara.toString());
//		List<String> keys = new ArrayList<String>(sPara.keySet());
//
//		StringBuffer sbHtml = new StringBuffer();
//
//		sbHtml.append("正在跳转。。。<br/>" + "<form id=\"ysepaysubmit\" name=\"ysepaysubmit\" action=\""
//				+ SysParamUtil.getParam("YSEPAY_GATEWAY_URL") + "\" method = \"" + "post" + "\">");
//
//		for (int i = 0; i < keys.size(); i++) {
//			String name = keys.get(i);
//			String value = sPara.get(name);
//
//			sbHtml.append("<input type=\"text\" name=\"" + name + "\" value=\"" + StringEscapeUtils.escapeHtml(value)
//					+ "\"/><br/>");
//		}
//
//		sbHtml.append("<input type=\"submit\" value=\"" + "确定" + "\" style=\"display;\"></form>");
//		sbHtml.append("<script>document.forms['ysepaysubmit'].submit();</script>");
//
//		return sbHtml.toString();
//	}

//	/**
//	 * 拼接请求代付参数
//	 * @param request
//	 * @param sParaTemp
//	 * @return
//	 */
//	public static String buildRequestdf(HttpServletRequest request, Object obj) {
//
//		Map<String, String> sPara = buildRequestPara(request, getProperty(obj));
//		List<String> keys = new ArrayList<String>(sPara.keySet());
//		System.out.println("--打印所有参数--"+sPara.toString());
//		StringBuffer sbHtml = new StringBuffer();
//		sbHtml.append("正在跳转。。。<br/>" + "<form id=\"ysepaysubmit\" name=\"ysepaysubmit\" action=\""
//				+ SysParamUtil.getParam("YSEPAY_GATEWAY_URL_DF") + "\" method = \"" + "post" + "\">");
//
//		for (int i = 0; i < keys.size(); i++) {
//			String name = (String) keys.get(i);
//			String value = (String) sPara.get(name);
//
//			sbHtml.append("<input type=\"text\" name=\"" + name + "\" value=\"" + StringEscapeUtils.escapeHtml(value)
//					+ "\"/><br/>");
//		}
//
//		sbHtml.append("<input type=\"submit\" value=\"" + "确定" + "\" style=\"display;\"></form>");
//		sbHtml.append("<script>document.forms['ysepaysubmit'].submit();</script>");
//		
//		return sbHtml.toString();
//		
//	}
	
	
//	/**
//	 * 拼接请求代收参数
//	 * @param request
//	 * @param sParaTemp
//	 * @return
//	 */
//	public static String buildRequestds(HttpServletRequest request, Object obj) {
//
//		Map<String, String> sPara = buildRequestPara(request, getProperty(obj));
//		List<String> keys = new ArrayList<String>(sPara.keySet());
//		System.out.println("--打印所有参数--"+sPara.toString());
//		StringBuffer sbHtml = new StringBuffer();
//
//		sbHtml.append("正在跳转。。。<br/>" + "<form id=\"ysepaysubmit\" name=\"ysepaysubmit\" action=\""
//				+ SysParamUtil.getParam("YSEPAY_GATEWAY_URL_DS") + "\" method = \"" + "post" + "\">");
//
//		for (int i = 0; i < keys.size(); i++) {
//			String name = (String) keys.get(i);
//			String value = (String) sPara.get(name);
//
//			sbHtml.append("<input type=\"text\" name=\"" + name + "\" value=\"" + StringEscapeUtils.escapeHtml(value)
//					+ "\"/><br/>");
//		}
//
//		sbHtml.append("<input type=\"submit\" value=\"" + "确定" + "\" style=\"display;\"></form>");
//		sbHtml.append("<script>document.forms['ysepaysubmit'].submit();</script>");
//
//		return sbHtml.toString();
//	}
	
	
	/**
	 * 获取证书路径并且签名
	 * @param request
	 * @param sParaTemp
	 * @return
	 */
	private static Map<String, String> buildRequestPara(Map<String, String> sParaTemp,String sercetKey) {
		
		//除去数组中的空值和签名参数
		Map<String, String> sPara = SignUtils.paraFilter(sParaTemp);

		//私钥证书路径
		String partnerCert = SysParamUtil.getParam("PATH_PARTER_PKCS12");
		
		//读取证书
		//InputStream pfxCertFileInputStream = servletContext.getResourceAsStream(partnerCert);

		//通过file读取证书
		File file = new File(pathUtil.getRootPath()+partnerCert);
		
		String mysign = "";
		try {
			InputStream pfxCertFileInputStream = new FileInputStream(file);
			//遍历以及根据重新排序
			String signContent = SignUtils.getSignContent(sPara);
			mysign = SignUtils.rsaSign(signContent, sParaTemp.get("charset"), pfxCertFileInputStream, sercetKey);
			
		} catch (Exception e) {
			throw new RuntimeException("签名失败，请检查证书文件是否存在，密码是否正确");
		}

		sPara.put("sign", mysign);

		return sPara;
	}

	/**
	 * 异步验证签名
	 * 
	 * @param request
	 * @param params
	 * @return
	 */
	public static boolean verifySign(Map<String,String> params) {
		//除去数组中的空值和签名参数
		Map<String, String> sPara = SignUtils.paraFilter(params);
		//私钥证书路径
		String publicCert = SysParamUtil.getParam("PATH_YSEPAY_PUBLIC_CERT");
		//通过file读取证书
		File file = new File(pathUtil.getRootPath()+publicCert);
		String sign = "";
		if (params.get("sign") != null) {
			sign = params.get("sign");
		}
		boolean isSign = false;
		try {
			InputStream publicCertFileInputStream = new FileInputStream(file);
			isSign = SignUtils.rsaCheckContent(publicCertFileInputStream, sPara, sign,"utf-8");
		} catch (Exception e) {
			throw new RuntimeException("验证签名失败，请检查银盛公钥证书文件是否存在");
		}

		return isSign;
	}
	
	
	/**
	 * send发送
	 * @param request
	 * @param obj
	 * @return
	 */
	public static String backgroundURL(String url,Map<String, String> map, String sercetKey) {

		Map<String, String> sPara = buildRequestPara(map, sercetKey);

		try {
			String resText = Https.httpsSend(url, sPara);
			
			return resText;
		} catch (Exception e) {
			throw new RuntimeException("支付发送网络异常" + e.getMessage());
		}
	}
	
	/**
	 * 取对应的key和value
	 * @param obj
	 * @return
	 */
	public static Map<String, String> getProperty(Object obj) {

		Map<String, String> map = null;
		try {
			Field fields[] = obj.getClass().getDeclaredFields();
			String[] name = new String[fields.length];
			Object[] value = new Object[fields.length];
			Field.setAccessible(fields, true);
			map = new HashMap<String, String>();
			for (int i = 0; i < name.length; i++) {
				name[i] = fields[i].getName();
				value[i] = fields[i].get(obj);

				if (value[i] == null ||name[i] == null || "serialVersionUID".equals(name[i])) {
					continue;
				}
                System.out.println("key="+name[i]+",value="+String.valueOf(value[i]));
				map.put(name[i], String.valueOf(value[i]));
			}
		} catch (Exception e) {
			throw new RuntimeException("获取bean属性值异常:" + e.getMessage());
		}
		return map;
	}
}
