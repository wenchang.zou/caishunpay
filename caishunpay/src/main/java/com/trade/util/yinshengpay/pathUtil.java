package com.trade.util.yinshengpay;

import com.gy.system.SysParamUtil;

public class pathUtil {
	
	public static void main(String[] args) {
		System.out.println(getRootPath()+SysParamUtil.getParam("PATH_YSEPAY_PUBLIC_CERT"));;
	}
	
	public static String getRootPath() {
		String path=pathUtil.class.getResource("/").getPath();
		if(path.indexOf(":") != -1) {
			path=path.substring(1, path.length());
		}
		path=path.replace("/WEB-INF/classes/", "");
		return path;
		
	}

}
