package com.trade.util.caishun;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;

import com.gy.util.HttpUtility;
import com.gy.util.JsonUtils;
import com.gy.util.UUIDGenerator;

public class Test {

	private final static String URL = "https://fast.onepaypass.com/mps/cloudplatform/api/trade.html";
//	private final static String URL = "https://mempay.onepaypass.com/mps/cloudplatform/api/trade.html";

	private static String key = "2cdcecddc29e43209231c8bb1f04e7af";

	public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String tradeNo = UUIDGenerator.getOrderIdByUUId((int) 20);
		Map<String, String> param = new HashMap<String, String>();
		param.put("tradeType", "cs.pay.submit");
		param.put("version", "1.3");
		param.put("channel", "unionpayQR");
		param.put("mchId", "800000001");
		param.put("subMchId", "800000001000000001");
		param.put("body", "ceshi");
		param.put("outTradeNo", tradeNo);
		param.put("amount", "0.01");
		param.put("currency", "CNY");
//		param.put("timePaid", DateUtil.dateToString(new Date()).replaceAll("_", "").replaceAll(" ", ""));
		param.put("subject", "ceshi");
//		param.put("notifyUrl", "http://www.baidu.com");

		//拼接
		String toSign = SignUtil.createLinkString(param)+"&key="+key;
		System.out.println("签名串："+toSign);
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        byte[] signed = md5.digest(toSign.getBytes("UTF-8"));
        // 3.生成签名:
        String sign = Hex.encodeHexString(signed).toUpperCase();
        param.put("sign", sign);
        String reqStr= JsonUtils.toJson(param);
		// 发送请求
        String jsonResult = HttpUtility.urlPostforJson(URL, reqStr);//进行post请求
        System.out.println(jsonResult);

	}

}
