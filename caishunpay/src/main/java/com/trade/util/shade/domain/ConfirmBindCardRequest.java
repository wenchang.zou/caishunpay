package com.trade.util.shade.domain;

public class ConfirmBindCardRequest extends PubRequest {

    private String userId;
    private String sdMsgNo;    // 申请绑定流水号
    private String phoneNo;
    private String smsCode;    // 短信验证码
    private String notifyUrl;    // 异步通知地址：需要向杉德报备该地址信息
    private String frontUrl;    // 前端通知地址
    private String extend;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSdMsgNo() {
        return sdMsgNo;
    }

    public void setSdMsgNo(String sdMsgNo) {
        this.sdMsgNo = sdMsgNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getSmsCode() {
        return smsCode;
    }

    public void setSmsCode(String smsCode) {
        this.smsCode = smsCode;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getFrontUrl() {
        return frontUrl;
    }

    public void setFrontUrl(String frontUrl) {
        this.frontUrl = frontUrl;
    }

    public String getExtend() {
        return extend;
    }

    public void setExtend(String extend) {
        this.extend = extend;
    }

}
