package com.trade.util.shade.constant; /**
 * Copyright : http://www.sandpay.com.cn , 2011-2014
 * Project : paychannel-cmsb-sdk
 * $Id$
 * $Revision$
 * Last Changed by pxl at 2016-10-19 上午11:26:32
 * $URL$
 * <p>
 * Change Log
 * Author      Change Date    Comments
 * -------------------------------------------------------------
 * pxl         2016-10-19        Initailized
 */


/**
 * @author : pxl
 * @version 2.0.0
 * @ClassName ：Constants
 * @Date : 2016-10-19 上午11:26:32
 */
public class SandpayConstants {
    public static final String SUCCESS_RESP_CODE = "000000";
    public static String QUICKPAY_PRODUCT = "00000018";// 快捷api接入
    public static String APPLY_BIND_CARD = "sandPay.fastPay.apiPay.applyBindCard";//	统一下单并支付
}
