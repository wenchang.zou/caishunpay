/*
 * Decompiled with CFR 0_124.
 */
package com.account.service;

import com.account.bean.TradeMchtAccountDetail;
import com.trade.bean.own.DfParam;
import com.trade.bean.own.MerchantInf;
import com.trade.enums.ResponseEnum;

public interface YiDaifuService {
    public ResponseEnum singlePay(TradeMchtAccountDetail var1, MerchantInf var2);

    public ResponseEnum doSinglePay(TradeMchtAccountDetail var1, MerchantInf var2);

    public TradeMchtAccountDetail querySinglePay(DfParam var1);
}
