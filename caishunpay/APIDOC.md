## 1 瑙勮寖璇存槑

### 1.1 閫氫俊鍗忚

HTTP鍗忚

### 1.2 璇锋眰鏂规硶
鎵�鏈夋帴鍙ｅ彧鏀寔POST鏂规硶鍙戣捣璇锋眰銆�

### 1.3 瀛楃缂栫爜
HTTP閫氳鍙婃姤鏂囧潎閲囩敤UTF-8瀛楃闆嗙紪鐮佹牸寮忋��

### 1.4 鎶ユ枃瑙勮寖璇存槑

1. 鎶ユ枃瑙勮寖浠呴拡瀵逛氦鏄撹姹傛暟鎹繘琛屾弿杩帮紱  

2. 鎶ユ枃瑙勮寖鍒嗕负璇锋眰鎶ユ枃鍜屽搷搴旀姤鏂囥�傝姹傛姤鏂囨弿杩扮敱鍙戣捣鏂癸紝鍝嶅簲鎶ユ枃鐢辨姤鏂囨帴鏀舵柟鍝嶅簲銆�

## 2 鎺ュ彛瀹氫箟

### 2.1 鍙戣捣鏀粯

浣跨敤姝ょ粺涓�鏀粯鎺ュ彛鏉ュ彂璧锋敮浠樸��

- **鎺ュ彛璇存槑锛�** 鍙戣捣鏀粯
- **璇锋眰鏂瑰紡锛�**POST 
- **鎺ュ彛鍦板潃锛�** http://gateway.ykbpay.cn:8080/caishunpay-web/thirdParty/createOrder

#### 2.1.1 璇锋眰鍙傛暟

| 鍙傛暟鍚嶇О        | 绫诲瀷     | 鎻忚堪                                       |
| ----------- | :----- | ---------------------------------------- |
| merchantId  | string | 鍟嗘埛鍙�                                      |
| tradeSn     | string | 鍟嗘埛绯荤粺璁㈠崟鍙�                                  |
| orderAmount | int    | 閲戦锛堝崟浣嶅垎锛�                                  |
| goodsName   | string | 鍟嗗搧鍚嶇О                                     |
| notifyUrl   | string | 鍚庣鍥炶皟URL                                  |
| sign        | string |  绛惧悕瀛楃涓� |
| tradeSource | int    | 浜ゆ槗绫诲瀷, 1:鏀粯瀹� 12:鏀粯瀹滺5 2: 寰俊鎵爜 22:寰俊H5  4:QQ閽卞寘  22 : 寰俊H5  7:蹇嵎鏀粯 71:閾惰仈鎵爜 |
| nonce       | String | 闅忔満瀛楃涓�                                      |

璇锋眰绀轰緥锛�
```
   "merchantid":"shunca0002",
  "tradeSn":"test14",
  "orderAmount":2,
  "goodsName":"娴嬭瘯",
  "notifyUrl":"http://www.baidu.com",
  "sign":"CF1D05DA8CA4095...",
  "tradeSource":22,
  "nonce":1234,
```

#### 2.1.2 鍝嶅簲鍙傛暟

| 鍙傛暟鍚嶇О       | 绫诲瀷     | 鎻忚堪   |
| ---------- | :----- | ---- |
| merchantId | string | 鍟嗘埛鍙�  |
| resultCode | string |  鍝嶅簲鐮侊紝濡傛灉鏄�00000鍒欎负鎴愬姛    |
| message    | string |  鍝嶅簲鐮佹弿杩�   |
| nonce      | string |  闅忔満瀛楃涓�   |
| pay_url    | string |  寮曞鐢ㄦ埛璺宠浆URL    |
| img_url    | string |  寮曞鐢ㄦ埛鎵爜鍥剧墖URL    |
| sign       | string |   绛惧悕   |

鍝嶅簲绀轰緥锛�
```
{
  "resultCode":"000000",
  "message":"璇锋眰鎴愬姛",
  "sign":"29DAQWEFSA...",
  "nonce":"QWEWQEDFSAVCBCOI...",
  "pay_url":"http://gateway.ykbpay.cn/pay/",
  "img_url":"http://gateway.ykbpay.cn/img/",
  "merchantId":"caishun0002"
}
```

### 2.2 绗笁鏂规煡璇㈣鍗曟敮浠�

- **鎺ュ彛璇存槑锛�** 绗笁鏂规煡璇㈣鍗曟敮浠�
- **璇锋眰鏂瑰紡锛�**POST 
- **鎺ュ彛鍦板潃锛�** http://gateway.ykbpay.cn:8080/caishunpay-web/thirdParty/queryOrder

#### 2.2.1 璇锋眰鍙傛暟

| 鍙傛暟鍚嶇О       | 绫诲瀷     | 鎻忚堪                                       |
| ---------- | :----- | ---------------------------------------- |
| merchantId | string | 鍟嗘埛鍙�                                      |
| tradeSn    | string | 鍟嗘埛绯荤粺璁㈠崟鍙�                                  |
| sign       | string | 绛惧悕瀛楃涓� |
| nonce      | int    | 闅忔満鏁�                                      |

璇锋眰绀轰緥锛�
```
  "merchantid":"shunca0002",
  "tradeSn":"test14",
  "sign":"CF1D05DA8CA4095...",
  "nonce":1234,
```

#### 2.2.2 鍝嶅簲鍙傛暟

| 鍙傛暟鍚嶇О        | 绫诲瀷     | 鎻忚堪      |
| ----------- | :----- | ------- |
| sign        | string |    绛惧悕瀛楃涓�     |
| nonce       | string |    闅忔満瀛楃涓�     |
| merchantId  | string | 鍟嗘埛鍙�     |
| tradeSn     | string | 鍟嗘埛绯荤粺璁㈠崟鍙� |
| orderAmount | int    | 閲戦锛堝崟浣嶅垎锛� |
| tradeState  | string |    浜ゆ槗鐘舵��  NOTPAY 鏈敮浠� SUCCESS 鏀粯鎴愬姛    |

鍝嶅簲绀轰緥锛�
```
{
  "sign":"29DAQWEFSA...",
  "nonce":"QWEWQEDFSAVCBCOI...",
  "merchantId":"caishun0002",
  "tradeSn":"test12",
  "orderAmount":22,
  "tradeState":"NOTPAY"
}
```

### 2.3 缃戦摱鍙戣捣璁㈠崟鏀粯

- **鎺ュ彛璇存槑锛�** 缃戦摱鍙戣捣璁㈠崟鏀粯
- **璇锋眰鏂瑰紡锛�**POST 
- **鎺ュ彛鍦板潃锛�** http://gateway.ykbpay.cn:8080/caishunpay-web/bankCardPay/queryOrder

#### 2.3.1 璇锋眰鍙傛暟

| 鍙傛暟鍚嶇О        | 绫诲瀷     | 鎻忚堪                                       |
| ----------- | :----- | ---------------------------------------- |
| merchantId  | string | 鍟嗘埛鍙�                                      |
| tradeSn     | string | 鍟嗘埛绯荤粺璁㈠崟鍙�                                  |
| orderAmount | int    | 閲戦锛堝崟浣嶅垎锛�                                  |
| goodsName   | string | 鍟嗗搧鍚嶇О                                     |
| bankSegment | int    |                                          |
| notifyUrl   | string | 鍚庣鍥炶皟URL                                  |
| tradSource  | int    | 浜ゆ槗绫诲瀷                                     |
| cardType    | int    |   00鍊熻鍗� 01璐疯鍗�                                       |
| channelType | int    |      1 PC绔� 2绉诲姩绔�                                    |
| callbackUrl | string |       浜ゆ槗鎴愬姛鍚庡墠绔烦杞湴鍧�     |
| sign        | string | 鍔犲瘑鍓嶏細goodsName=娴嬭瘯&merchantid=shancai00... |
| nonce       | string | 闅忔満鏁�        |

璇锋眰绀轰緥锛�
```
  "merchantid":"shunca0002",
  "tradeSn":"test14",
  "orderAmount":2,
  "goodsName":"娴嬭瘯",
  "bankSegment":1002,
  "notifyUrl":"http://www.baidu.com",
  "cardType":01,
  "channelType":1,
  "callbackUrl":"http://www.baidu.com",
  "sign":"CF1D05DA8CA4095...",
  "tradeSource":22,
  "nonce":1234
```

#### 2.3.2 鍝嶅簲鍙傛暟

| 鍙傛暟鍚嶇О           | 绫诲瀷     | 鎻忚堪   |
| -------------- | :----- | ---- |
| resultCode     | string |    鍝嶅簲鐮�  |
| message        | string | 杩斿洖娑堟伅 |
| sign           | string |    绛惧悕  |
| nonce          | string |   闅忔満瀛楃涓�   |
| merchantId     | string |   鍟嗘埛鍙�   |
| payUrl         | string |   寮曞鐢ㄦ埛鏀粯URL   |

鍝嶅簲绀轰緥锛�
```
{
  "resultCode":"000000",
  "message":"璇锋眰鎴愬姛",
  "sign":"29DAQWEFSA...",
  "nonce":"QWEWQEDFSAVCBCOI...",
  "merchantId":"shunca0002",
  "payUrl":"http://gateway.ykbpay.cn/pay/",
  "transaction_id":"qwe084524312"
}
```

### 2.4 缃戦摱鏌ヨ璁㈠崟鏀粯

- **鎺ュ彛璇存槑锛�** 閾惰鏌ヨ璁㈠崟鏀粯
- **璇锋眰鏂瑰紡锛�**POST 
- **鎺ュ彛鍦板潃锛�** http://gateway.ykbpay.cn:8080/caishunpay-web/bankCardPay/queryOrder

#### 2.4.1 璇锋眰鍙傛暟

| 鍙傛暟鍚嶇О       | 绫诲瀷     | 鎻忚堪                                       |
| ---------- | :----- | ---------------------------------------- |
| merchantId | string | 鍟嗘埛鍙�                                      |
| tradeSn    | string | 鍟嗘埛绯荤粺璁㈠崟鍙�                                  |
| sign       | string | 绛惧悕涓� |
| nonce      | int    | 闅忔満鏁�                                      |

璇锋眰绀轰緥锛�
```
{
  "merchantid":"shunca0002",
  "tradeSn":"test14",
  "sign":"CF1D05DA8CA4095...",
  "nonce":1234,
}
```

#### 2.4.2 鍝嶅簲鍙傛暟

| 鍙傛暟鍚嶇О        | 绫诲瀷     | 鎻忚堪      |
| ----------- | :----- | ------- |
| sign        | string |     绛惧悕涓�    |
| nonce       | string |    闅忔満瀛楃涓�     |
| merchantId  | string | 鍟嗘埛鍙�     |
| tradeSn     | string | 鍟嗘埛绯荤粺璁㈠崟鍙� |
| orderAmount | int    | 閲戦锛堝崟浣嶅垎锛� |
| tradeState  | string |   浜ゆ槗鐘舵��      |
| carType     | string | 鍗＄被鍨�     |
| bankName    | string | 閾惰鍚嶇О    |
| channelType | string |   1 PC绔�  2 绉诲姩绔�      |

鍝嶅簲绀轰緥锛�
```
{
  "sign":"29DAQWEFSA...",
  "nonce":"QWEWQEDFSAVCBCOI...",
  "merchantId":"caishun0002",
  "tradeSn":"test12",
  "orderAmount":22,
  "tradeState":"NOTPAY",
  "carType":"01",
  "bankName":"鎷涘晢閾惰",
  "channelType":"1"
}
```

### 2.5 鏀粯缁撴灉寮傛鍥炶皟

鏂瑰紡锛� HTTP POST璇锋眰
Encoding : application/json

绀轰緥:
```
{
  "sign":"29DAQWEFSA...",
  "nonce":"QWEWQEDFSAVCBCOI...",
  "merchantId":"caishun0002",
  "tradeSn":"test12",
  "orderAmount":22,
  "tradeState":"NOTPAY"
}
```

| 鍙傛暟鍚嶇О        | 绫诲瀷     | 鎻忚堪      |
| ----------- | :----- | ------- |
| sign        | string |    绛惧悕瀛楃涓�     |
| nonce       | string |    闅忔満瀛楃涓�     |
| merchantId  | string | 鍟嗘埛鍙�     |
| tradeSn     | string | 鍟嗘埛绯荤粺璁㈠崟鍙� |
| orderAmount | int    | 閲戦锛堝崟浣嶅垎锛� |
| tradeState  | string |    浜ゆ槗鐘舵��  NOTPAY 鏈敮浠� SUCCESS 鏀粯鎴愬姛    |

搴旇繑鍥� *success* 瀛楃涓叉潵鍝嶅簲鍥炶皟



## 3 鍔犲瘑鏂瑰紡

涓轰簡淇濊瘉鏁版嵁浼犺緭杩囩▼涓殑鏁版嵁鐪熷疄鎬у拰瀹屾暣鎬э紝鎴戜滑闇�瑕佸鏁版嵁杩涜鏁板瓧绛惧悕锛屽湪鎺ユ敹绛惧悕鏁版嵁涔嬪悗蹇呴』杩涜绛惧悕鏍￠獙銆�
鏁板瓧绛惧悕鏈変袱涓楠わ紝鍏堟寜涓�瀹氳鍒欐嫾鎺ヨ绛惧悕鐨勫師濮嬩覆锛屽啀閫夋嫨鍏蜂綋鐨勭畻娉曞拰瀵嗛挜璁＄畻鍑虹鍚嶇粨鏋溿��
涓�鑸け璐ョ殑缁撴灉涓嶇鍚嶃��

### 3.1 绛惧悕鍘熷涓�

鏃犺鏄姹傝繕鏄簲绛旓紝绛惧悕鍘熷涓叉寜浠ヤ笅鏂瑰紡缁勮鎴愬瓧绗︿覆:

1銆侀櫎sign 瀛楁澶栵紝鎵�鏈夊弬鏁版寜鐓у瓧娈靛悕鐨刟scii鐮佷粠灏忓埌澶ф帓搴忓悗浣跨敤QueryString 鐨勬牸寮�(鍗� key1=value1&key2=value2...)鎷兼帴鑰屾垚锛岀┖鍊间笉浼犻�掞紝涓嶅弬涓庣鍚嶇粍涓层��
2銆� 绛惧悕鍘熷涓蹭腑锛屽瓧娈靛悕鍜屽瓧娈靛�奸兘閲囩敤鍘熷鍊硷紝涓嶈繘琛� URLEncode銆�
3銆� 骞冲彴杩斿洖鐨勫簲绛旀垨閫氱煡娑堟伅鍙兘浼氱敱浜庡崌绾у鍔犲弬鏁帮紝璇烽獙璇佸簲绛旂鍚嶆椂娉ㄦ剰鍏佽杩欑鎯呭喌銆�

### 3.2 绛惧悕绠楁硶
MD5 绛惧悕
MD5 鏄竴绉嶆憳瑕佺敓鎴愮畻娉曪紝閫氳繃鍦ㄧ鍚嶅師濮嬩覆鍚庡姞涓婂晢鎴烽�氫俊瀵嗛挜鐨勫唴瀹癸紝杩涜 MD5 杩愮畻锛� 褰㈡垚鐨勬憳瑕佸瓧绗︿覆鍗充负绛惧悕缁撴灉銆備负浜嗘柟渚挎瘮杈冿紝绛惧悕缁撴灉缁熶竴杞崲涓哄ぇ鍐欏瓧绗︺��
娉ㄦ剰:绛惧悕鏃跺皢瀛楃涓茶浆鍖栨垚瀛楄妭娴佹椂鎸囧畾鐨勭紪鐮佸瓧绗﹂泦搴斾笌鍙傛暟 charset 涓�鑷淬��
MD5 绛惧悕璁＄畻鍏紡:

sign Md5(鍘熷瓧绗︿覆&key=鍟嗘埛瀵嗛挜).toUpperCase 

鍋囪鍟嗘埛瀵嗛挜涓�:sLbAsG00RWs1eF13juevu5WfEFLDSe0c锛屽晢鎴稩D涓�1000001221


i:缁忚繃 a 杩囩▼ URL 閿�煎瀛楀吀搴忔帓搴忓悗鐨勫瓧绗︿覆 string1 涓�: 
```
merchantId=1000001221&message=璇锋眰鎴愬姛&orderAmount=2&resultCode=00000&tradeSn=222227&tradeState=SUCCESS&transaction_id=5GQjTH1y4xNaii64PMkApg
```

ii:缁忚繃 b 杩囩▼鍚庡緱鍒� sign 涓�: 
sign=md5(string1&key=sLbAsG00RWs1eF13juevu5WfEFLDSe0c).toUpperCase
= 2274DE08CAB2FB0ABB44A28D8DA45561
